#!/bin/bash

CLONE_FOLDER_NAME=elevenpanda
GIT_CLONE_DIR=https://gitlab.com/is-eleven/elevenpanda.git
PRODUCTION_NAME=elevenpandatest1

##############################
# REMOVER DIRECTORIO SI EXISTE
##############################
[ -d "./$CLONE_FOLDER_NAME" ] && rm -rf ./$CLONE_FOLDER_NAME

########################
# CREAR NUEVO DIRECTORIO
########################
mkdir $CLONE_FOLDER_NAME

####################
# CLONAR REPOSITORIO
####################
git clone $GIT_CLONE_DIR ./$CLONE_FOLDER_NAME

#######################
# CAMBIAR DEBUG A FALSE
#######################
sed -i "s/DEBUG = True/DEBUG = False/gi" ./$CLONE_FOLDER_NAME/elevenpandaApp/elevenpandaApp/settings.py

################
# INICIAR HEROKU
################
heroku login

#######################################
# CREAR y CONFIGURAR PROYECTO EN HEROKU
#######################################
cd $CLONE_FOLDER_NAME/elevenpandaApp
heroku create $PRODUCTION_NAME
git init
git add .
git commit -m "commit produccion"
heroku git:remote -a $PRODUCTION_NAME
heroku addons:create heroku-postgresql:hobby-dev
git push heroku master

##################################
# MIGRAR TABLAS A LA BASE DE DATOS
##################################
heroku run python manage.py makemigrations
heroku run python manage.py migrate

#########################
# POBLAR LA BASE DE DATOS
#########################
ls
cd ..
cd ..
ls
cat sqlScript.sql | heroku pg:psql --app $PRODUCTION_NAME



