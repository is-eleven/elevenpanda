
-- TOC entry 3517 (class 0 OID 171244)
-- Dependencies: 207
-- Data for Name: Usuarios_usuario; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."Usuarios_usuario" (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined, aprobado, cedula, fecha_nacimiento) FROM stdin;
2	!6haUcaEoHPW2pZkQBITmZXzcRRqAoLPzfzQZPJ9f	2020-11-16 15:56:12.290276-03	f	alfredo	Alfredo	Franco	dmc.luisalfredo@fpuna.edu.py	f	t	2020-11-16 15:56:12.290276-03	t	\N	\N
3	!Bi6TuieYpPMPq5a8VfVZzugcOC5P8yvS2tTufpgk	2020-11-16 15:56:12.605719-03	f	gredo	Gredo	Frame	dmc.luisalgredo@gmail.com	f	t	2020-11-16 15:56:12.605719-03	t	\N	\N
4	!AehkUyGlf55A8amMPQMi4dh2LqFb7chQf6Bpjfi3	2020-11-16 15:56:12.909882-03	t	carlos	Carlos	Ruiz Diaz	carlrda96@gmail.com	f	t	2020-11-16 15:56:12.909882-03	t	\N	\N
5	!QefkRX59djVeUi68E70KuTZ0acVnwNfF8N6GViCq	2020-11-16 15:56:13.217395-03	t	monica	Mónica	Lampert	monicalampert465@gmail.com	f	t	2020-11-16 15:56:13.217395-03	t	\N	\N
6	!wN7PB3CCrt0m5DjjHaWQHyap7hbMhYslqfHLXiXN	2020-11-16 15:56:13.527947-03	t	guillermo	Guillermo	Alvarenga	guillealvarenga@gmail.com	f	t	2020-11-16 15:56:13.527947-03	t	\N	\N
1	!YueoQpQG7ETeDLYdJuQylKusxaPK5Y57G2WGmlC2	2020-11-16 15:56:30.966601-03	t	luis	Luis	Meza	dmc.luisalfredo@gmail.com	f	t	2020-11-16 15:56:11.981098-03	t	\N	\N
\.


--
-- TOC entry 3584 (class 0 OID 172097)
-- Dependencies: 274
-- Data for Name: django_site; Type: TABLE DATA; Schema: public; Owner: postgres
--

--COPY public.django_site (id, domain, name) FROM stdin;
--1	example.com	example.com
--\.

--
-- TOC entry 3586 (class 0 OID 172108)
-- Dependencies: 276
-- Data for Name: socialaccount_socialaccount; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.socialaccount_socialaccount (id, provider, uid, last_login, date_joined, extra_data, user_id) FROM stdin;
2	google	107644820152171739481	2020-11-16 15:56:12.446096-03	2020-11-16 15:56:12.446096-03	{"id": "107644820152171739481", "email": "dmc.luisalfredo@fpuna.edu.py", "verified_email": true, "name": "Luis Alfredo Meza Franco", "given_name": "Luis Alfredo", "family_name": "Meza Franco", "picture": "https://lh6.googleusercontent.com/-BUszdtbXtAY/AAAAAAAAAAI/AAAAAAAAAAA/AMZuucmRxcTtO5znmNdWmbKLrxecWCh-UQ/s96-c/photo.jpg", "locale": "es", "hd": "fpuna.edu.py"}	2
3	google	111213345095493427959	2020-11-16 15:56:12.756643-03	2020-11-16 15:56:12.756643-03	{"id": "111213345095493427959", "email": "dmc.luisalgredo@gmail.com", "verified_email": true, "name": "Luis Alfredo Meza Franco", "given_name": "Luis Alfredo", "family_name": "Meza Franco", "picture": "https://lh4.googleusercontent.com/-ho91zsvVlzg/AAAAAAAAAAI/AAAAAAAAAAA/AMZuuckyYehvdy5Q7Xuo0TTYPiBBy3tXtw/s96-c/photo.jpg", "locale": "es"}	3
4	google	104110230462960140522	2020-11-16 15:56:13.063654-03	2020-11-16 15:56:13.063654-03	{"id": "104110230462960140522", "email": "carlrda96@gmail.com", "verified_email": true, "name": "Carlos Ruiz Diaz", "given_name": "Carlos", "family_name": "Ruiz Diaz", "picture": "https://lh6.googleusercontent.com/-Lse0Eopdp-c/AAAAAAAAAAI/AAAAAAAAAAA/AMZuuclGDLLnCo92DtYWpK7HrfP5UuFD9w/photo.jpg", "locale": "es"}	4
5	google	106924945557037316963	2020-11-16 15:56:13.373512-03	2020-11-16 15:56:13.373512-03	{"id": "106924945557037316963", "email": "monicalampert465@gmail.com", "verified_email": true, "name": "Mu00f3nica Lampert", "given_name": "Mu00f3nica", "family_name": "Lampert", "picture": "https://lh3.googleusercontent.com/a-/AOh14GgOpc4qBaT4shWyayIHIebFF3Tt4EXNE7an0sNZyw=s96-c", "locale": "es-419"}	5
6	google	112790295329099438589	2020-11-16 15:56:13.681542-03	2020-11-16 15:56:13.681542-03	{"id": "112790295329099438589", "email": "guillealvarenga@gmail.com", "verified_email": true, "name": "guillermo alvarenga", "given_name": "guillermo", "family_name": "alvarenga", "picture": "https://lh6.googleusercontent.com/-iZLS1Xatv60/AAAAAAAAAAI/AAAAAAAAAAA/AMZuuckVc5aSngJ5r5l-WXcHRrKrr6xNJA/s96-c/photo.jpg", "locale": "es"}	6
1	google	101078650703823259730	2020-11-16 15:56:30.921572-03	2020-11-16 15:56:12.137193-03	{"id": "101078650703823259730", "email": "dmc.luisalfredo@gmail.com", "verified_email": true, "name": "Luis Meza", "given_name": "Luis", "family_name": "Meza", "picture": "https://lh3.googleusercontent.com/-a2Lmzl4KSl4/AAAAAAAAAAI/AAAAAAAAAAA/AMZuuclXC3KJTnfLUF3nBQJ99DZwFGFksw/s96-c/photo.jpg", "locale": "es-419"}	1
\.


--
-- TOC entry 3588 (class 0 OID 172119)
-- Dependencies: 278
-- Data for Name: socialaccount_socialapp; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.socialaccount_socialapp (id, provider, name, client_id, secret, key) FROM stdin;
1	google	GoogleLogin	1071847219654-tojlqan1rurs5maaep2m6suln6tk6pf9.apps.googleusercontent.com	YKuHb8YcQwvzHbRxmgom8V1G	
\.


--
-- TOC entry 3590 (class 0 OID 172127)
-- Dependencies: 280
-- Data for Name: socialaccount_socialapp_sites; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.socialaccount_socialapp_sites (id, socialapp_id, site_id) FROM stdin;
1	1	1
\.


--
-- TOC entry 3529 (class 0 OID 171350)
-- Dependencies: 219
-- Data for Name: roles_permiso; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.roles_permiso (id, nombre, ambito, descripcion) FROM stdin;
1	ver_lista_usuarios	si	Ver lista de usuarios
2	ver_todos_los_perfiles	si	Ver todos los perfiles
3	ver_perfil_propio	si	Ver perfil propio
4	modificar_todos_los_perfiles	si	Modificar todos los perfiles
5	modificar_perfil_propio	si	Modificar perfil propio
6	deshabilitar_usuario	si	Dehabilitar usuarios
7	aprobar_usuario	si	Aprobar usuarios
8	ver_lista_roles_de_sistema	si	Ver lista de roles del sistema
9	modificar_roles_sistema	si	Modificar roles del sistema
10	crear_proyecto	si	Crear proyecto
11	ver_todos_los_proyectos	si	Ver todos los proyectos
12	ver_proyectos_propios	si	Ver proyectos propios
13	ver_lista_proyectos	si	Ver lista de proyectos
14	modificar_roles_de_usuarios	si	Modificar roles de usuarios
15	ver_configuracion_de_proyecto	pr	Ver configuraciṕn de proyecto
16	ver_fases_de_proyecto	pr	Ver fases de proyecto
17	ver_fase	fa	Ver fase
18	crear_nuevo_item	fa	Crear nuevo item
19	modificar_item	fa	Modificar item
20	modificar_relaciones	fa	Modificar relaciones
21	versionar_item	fa	Versionar ítem
22	ver_trazo	fa	Ver trazo de ítem
23	aprobar_pendiente	fa	Aprobar ítem pendiente
24	aprobar_en_revision	fa	Aprobar ítem en revisión
25	aprobar_comprometido	fa	Aprobar ítem comprometido
26	crear_linea_base	fa	Crear línea base
27	ver_historial_items	fa	Ver historial de ítems
\.


--
-- TOC entry 3523 (class 0 OID 171302)
-- Dependencies: 213
-- Data for Name: roles_rol; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.roles_rol (id, nombre, ambiente) FROM stdin;
1	Visualizador	fa
2	Aprobador	fa
3	Modificador	fa
4	Versionador	fa
5	Creador	fa
6	Full master op	fa
\.


--
-- TOC entry 3527 (class 0 OID 171318)
-- Dependencies: 217
-- Data for Name: roles_rolpermiso; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.roles_rolpermiso (id, permiso_id, rol_id) FROM stdin;
1	17	1
2	22	1
3	27	1
4	17	2
5	23	2
6	24	2
7	25	2
8	17	3
9	20	3
10	19	3
11	17	4
12	21	4
13	17	5
14	18	5
15	26	5
16	17	6
17	18	6
18	19	6
19	20	6
20	21	6
21	22	6
22	23	6
23	24	6
24	25	6
25	26	6
26	27	6
\.


--
-- TOC entry 3541 (class 0 OID 171466)
-- Dependencies: 231
-- Data for Name: proyecto_proyecto; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.proyecto_proyecto (id, nombre, estado, descripcion, fecha_creacion, fecha_finalizacion, color_icono, gerente_id, ultimo_rf, ultimo_rnf) FROM stdin;
1	Proyecto sin iniciar	pe		2020-11-16 15:56:51.482102-03	2020-11-16 15:56:51.482138-03	#4C7	\N	1	1
2	Proyecto iniciado sin ítems	ac		2020-11-16 15:57:27.037802-03	2020-11-16 15:57:27.037845-03	#7A9	1	1	1
4	Proyecto finalizado	fi		2020-11-16 16:11:27.878203-03	2020-11-16 16:11:27.878237-03	#656	1	11	1
3	Proyecto iniciado con ítems	ac		2020-11-16 16:02:28.373279-03	2020-11-16 16:02:28.37332-03	#484	1	12	1
\.


--
-- TOC entry 3553 (class 0 OID 171517)
-- Dependencies: 243
-- Data for Name: proyecto_proyectousuario; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.proyecto_proyectousuario (id, es_valido, fecha_union, proyecto_id, usuario_id) FROM stdin;
1	t	2020-11-16 15:57:39.058938-03	2	1
2	t	2020-11-16 15:57:49.834921-03	2	2
3	t	2020-11-16 15:57:49.86713-03	2	4
4	t	2020-11-16 16:02:34.119983-03	3	1
5	t	2020-11-16 16:03:12.545677-03	3	2
6	t	2020-11-16 16:03:12.577589-03	3	4
7	t	2020-11-16 16:03:12.607117-03	3	3
8	t	2020-11-16 16:03:12.634126-03	3	6
9	t	2020-11-16 16:03:12.666295-03	3	5
10	t	2020-11-16 16:11:35.727778-03	4	1
11	t	2020-11-16 16:11:45.376129-03	4	2
12	t	2020-11-16 16:11:45.415378-03	4	4
\.

--
-- TOC entry 3539 (class 0 OID 171458)
-- Dependencies: 229
-- Data for Name: proyecto_fase; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.proyecto_fase (id, nombre, numero_fase, proyecto_id, color, icono, fecha_creacion, fecha_finalizacion, finalizado) FROM stdin;
1	Configuración	1	2	#424242	fas fa-cogs	2020-11-16 16:00:38.336936-03	\N	f
2	Adecuación	2	2	#F1992E	fas fa-balance-scale	2020-11-16 16:00:57.289187-03	\N	f
3	Ejecución	3	2	#62AE31	fas fa-chart-line	2020-11-16 16:01:11.821681-03	\N	f
4	Preparación	1	3	#8456B7	fas fa-tasks	2020-11-16 16:06:12.58308-03	\N	f
5	Desarrollo	2	3	#3875D7	fas fa-laptop-code	2020-11-16 16:06:32.620327-03	\N	f
6	Corrección	3	3	#E74432	fas fa-bug	2020-11-16 16:07:01.418022-03	\N	f
7	Análisis	1	4	#58BFD7	fas fa-magic	2020-11-16 16:13:59.789889-03	\N	t
8	Desarrollo	2	4	#424242	fas fa-code	2020-11-16 16:14:59.024288-03	\N	t
\.


--
-- TOC entry 3549 (class 0 OID 171501)
-- Dependencies: 239
-- Data for Name: proyecto_tipodeitem; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.proyecto_tipodeitem (id, nombre, fase_id, proyecto_id) FROM stdin;
1	Pronostico	1	2
2	Descriptivo	2	2
3	Inesperado	3	2
4	Inicialización	4	3
5	Descriptivo	5	3
6	Validatorio	6	3
8	Inicialización	7	4
9	Validatorio	7	4
7	Descriptivo	8	4
\.

--
-- TOC entry 3543 (class 0 OID 171477)
-- Dependencies: 233
-- Data for Name: proyecto_proyectorol; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.proyecto_proyectorol (id, proyecto_id, rol_id) FROM stdin;
1	3	1
2	3	2
3	3	3
4	3	4
5	3	5
6	4	6
\.


--
-- TOC entry 3551 (class 0 OID 171509)
-- Dependencies: 241
-- Data for Name: proyecto_rolusuariofase; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.proyecto_rolusuariofase (id, fase_id, rol_id, usuario_id) FROM stdin;
1	7	6	1
2	8	6	1
3	4	1	1
4	4	2	1
5	4	3	1
6	4	4	1
7	4	5	1
8	5	1	1
9	5	2	1
10	5	3	1
11	5	4	1
12	5	5	1
13	6	1	1
14	6	2	1
15	6	3	1
16	6	4	1
17	6	5	1
19	4	1	4
20	4	2	4
21	4	3	4
22	4	4	4
23	4	5	4
24	4	1	5
25	4	2	5
26	4	3	5
27	4	4	5
28	4	5	5
29	4	1	6
30	4	2	6
31	4	3	6
32	4	4	6
33	4	5	6
34	5	1	5
35	5	2	5
36	5	3	5
37	5	4	5
38	5	5	5
39	5	1	6
40	5	2	6
41	5	3	6
42	5	4	6
43	5	1	4
44	5	5	6
45	5	2	4
46	5	3	4
47	5	4	4
48	5	5	4
49	6	1	4
50	6	2	4
51	6	3	4
52	6	4	4
53	6	5	4
54	6	1	5
55	6	2	5
56	6	3	5
57	6	4	5
58	6	5	5
59	6	1	6
60	6	2	6
61	6	3	6
62	6	4	6
63	6	5	6
\.

--
-- TOC entry 3557 (class 0 OID 171538)
-- Dependencies: 247
-- Data for Name: proyecto_atributodinamico; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.proyecto_atributodinamico (id, titulo, es_obligatorio, tipo, min, max, tipo_de_item_id, defecto) FROM stdin;
1	Tiempo	f	str	0	100	1	
2	Día	f	str	0	100	1	
3	Descripción	f	str	0	100	2	
4	Cantidad	f	num	0	100	2	
5	Total	f	num	0	100	2	
6	Fecha	f	str	0	100	3	
7	Importancia	f	num	0	100	3	
8	Es necesario	f	bool	0	100	4	
9	Características	f	str	0	100	4	
10	Descripción	f	str	0	100	5	
11	Es válido	f	bool	0	100	5	
12	Puede validarse	f	bool	0	100	6	
13	Validaciones	f	str	0	100	6	
14	Descripción	f	str	0	100	7	
15	Cantidad	f	num	0	100	7	
16	Total	f	num	0	100	7	
17	Es necesario	f	bool	0	100	8	
18	Características	f	str	0	100	8	
19	Puede validarse	f	bool	0	100	9	
20	Validaciones	f	str	0	100	9	
\.


--
-- TOC entry 3555 (class 0 OID 171525)
-- Dependencies: 245
-- Data for Name: proyecto_item; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.proyecto_item (id, estado, fase_id, tipo_de_item_id, codigo, costo, descripcion, nombre, prefijo, nombre_adjunto) FROM stdin;
7	ap	7	8	7	5	Realizar operaciones CRUD a las cosas	Operaciones CRUD	rf	\N
4	ap	7	9	4	10	Eliminar cosas	Elimiar	rf	\N
2	ap	7	8	2	10	Editar cosas	Editar	rf	\N
11	ap	4	4	1	20	Definir servicios del sistema	Definición de servicios	rf	\N
12	pe	4	4	2	22	Implementar servicio de clientes	Servicio de clientes	rf	\N
13	pe	4	4	3	18	Implementar servicio de empleados	Servicio de empleados	rf	\N
14	pe	4	4	4	10	Agregar nuevo cliente al sistema	Agregar cliente	rf	\N
5	ap	7	8	5	18	Eliminar cosas definitivamente	Eliminar por siempre	rf	\N
15	pe	4	4	5	20	Eliminar cliente del sistema	Eliminar cliente	rf	\N
3	ap	7	8	3	15	Guardar los cambios de las cosas	Guardar cambios	rf	\N
1	ap	7	8	1	10	Crear cosas	Crear	rf	\N
6	ap	7	8	6	10	Visualizar cosas	Visualizar	rf	\N
9	ap	8	7	9	20	Enviar señal a cosas	Enviar señal	rf	\N
16	ap	5	5	6	40	Crear base de datos para los datos de la base ¿...?	Base de datos	rf	\N
8	ap	8	7	8	20	Envía mensajes	Enviar mensaje	rf	\N
10	ap	8	7	10	18	Enviar texto a cosas	Enviar texto	rf	\N
17	ap	5	5	7	10	SELECT * FROM Table	Select	rf	\N
18	pe	5	5	8	33	INSERT INTO Table VALUES (default, 1, 2, 3)	Insert	rf	\N
19	pe	5	5	9	23	UPDATE Table SET attr=1 WHERE attr=2	Update	rf	\N
20	pe	6	6	10	30	Cosas que validan cosas	Validar cosas	rf	\N
21	pe	6	6	11	12	Cosas ramificadas que ramifican ramas	Ramificar ramas	rf	\N
\.

--
-- TOC entry 3575 (class 0 OID 171970)
-- Dependencies: 265
-- Data for Name: proyecto_itemversion; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.proyecto_itemversion (id, nombre, descripcion, costo, item_id, version, fecha_version, estado_anterior) FROM stdin;
1	Crear	Crear cosas	10	1	1.0	2020-11-16 16:18:27.460544-03	\N
2	Editar	Editar cosas	10	2	1.0	2020-11-16 16:18:45.041149-03	\N
3	Guardar cambios	Guardar los cambios de las cosas	15	3	1.0	2020-11-16 16:19:08.594013-03	\N
4	Elimiar	Eliminar cosas	10	4	1.0	2020-11-16 16:19:36.357778-03	\N
5	Eliminar por siempre	Eliminar cosas definitivamente	18	5	1.0	2020-11-16 16:20:02.092271-03	\N
6	Visualizar	Visualizar cosas	10	6	1.0	2020-11-16 16:20:42.227996-03	\N
7	Operaciones CRUD	Realizar operaciones CRUD a las cosas	5	7	1.0	2020-11-16 16:21:05.568452-03	\N
8	Operaciones CRUD	Realizar operaciones CRUD a las cosas	5	7	v2	2020-11-16 16:22:36.913792-03	\N
9	Crear	Crear cosas	10	1	v2	2020-11-16 16:22:43.491896-03	\N
10	Editar	Editar cosas	10	2	v2	2020-11-16 16:22:50.019867-03	\N
11	Elimiar	Eliminar cosas	10	4	v2	2020-11-16 16:22:56.29735-03	\N
12	Visualizar	Visualizar cosas	10	6	v2	2020-11-16 16:23:01.717743-03	\N
13	Guardar cambios	Guardar los cambios de las cosas	15	3	v2	2020-11-16 16:23:07.36374-03	\N
14	Eliminar por siempre	Eliminar cosas definitivamente	18	5	v2	2020-11-16 16:23:17.002537-03	\N
15	Enviar mensaje	Envía mensajes	20	8	1.0	2020-11-16 16:31:06.350422-03	\N
16	Visualizar	Visualizar cosas	10	6	v3	2020-11-16 16:44:14.532106-03	\N
17	Crear	Crear cosas	10	1	v3	2020-11-16 16:45:20.746526-03	\N
18	Editar	Editar cosas	10	2	v3	2020-11-16 16:45:51.311037-03	\N
19	Elimiar	Eliminar cosas	10	4	v3	2020-11-16 16:45:57.276953-03	\N
20	Enviar señal	Enviar señal a cosas	20	9	1.0	2020-11-16 16:46:55.370356-03	\N
21	Enviar texto	Enviar texto a cosas	18	10	1.0	2020-11-16 16:47:41.376154-03	\N
22	Eliminar por siempre	Eliminar cosas definitivamente	18	5	v3	2020-11-16 16:48:26.143995-03	\N
23	Guardar cambios	Guardar los cambios de las cosas	15	3	v3	2020-11-16 16:48:49.423264-03	\N
24	Crear	Crear cosas	10	1	v4	2020-11-16 16:49:26.806355-03	\N
25	Visualizar	Visualizar cosas	10	6	v4	2020-11-16 16:49:47.470505-03	\N
26	Enviar mensaje	Envía mensajes	20	8	v2	2020-11-16 16:51:20.926165-03	\N
27	Enviar señal	Enviar señal a cosas	20	9	v2	2020-11-16 16:51:26.398612-03	\N
28	Enviar texto	Enviar texto a cosas	18	10	v2	2020-11-16 16:51:35.60187-03	\N
29	Definición de servicios	Definir servicios del sistema	20	11	1.0	2020-11-16 16:55:11.710825-03	\N
30	Servicio de clientes	Implementar servicio de clientes	22	12	1.0	2020-11-16 16:55:44.74121-03	\N
31	Servicio de empleados	Implementar servicio de empleados	18	13	1.0	2020-11-16 16:56:06.62006-03	\N
32	Agregar cliente	Agregar nuevo cliente al sistema	10	14	1.0	2020-11-16 16:56:24.514954-03	\N
33	Eliminar cliente	Eliminar cliente del sistema	20	15	1.0	2020-11-16 16:56:48.77629-03	\N
34	Definición de servicios	Definir servicios del sistema	20	11	v2	2020-11-16 16:57:25.694685-03	\N
35	Servicio de clientes	Implementar servicio de clientes	22	12	v2	2020-11-16 16:57:36.256765-03	\N
36	Servicio de empleados	Implementar servicio de empleados	18	13	v2	2020-11-16 16:57:43.598183-03	\N
37	Agregar cliente	Agregar nuevo cliente al sistema	10	14	v2	2020-11-16 16:57:49.92597-03	\N
38	Eliminar cliente	Eliminar cliente del sistema	20	15	v2	2020-11-16 16:57:56.219781-03	\N
39	Base de datos	Crear base de datos para los datos de la base ¿...?	40	16	1.0	2020-11-16 16:59:16.604462-03	\N
40	Select	SELECT * FROM Table	10	17	1.0	2020-11-16 16:59:50.671505-03	\N
41	Insert	INSERT INTO Table VALUES (default, 1, 2, 3)	33	18	1.0	2020-11-16 17:00:25.77701-03	\N
42	Update	UPDATE Table SET attr=1 WHERE attr=2	23	19	1.0	2020-11-16 17:00:52.490002-03	\N
43	Base de datos	Crear base de datos para los datos de la base ¿...?	40	16	v2	2020-11-16 17:02:46.925973-03	\N
44	Select	SELECT * FROM Table	10	17	v2	2020-11-16 17:02:58.852504-03	\N
45	Insert	INSERT INTO Table VALUES (default, 1, 2, 3)	33	18	v2	2020-11-16 17:03:08.293396-03	\N
46	Update	UPDATE Table SET attr=1 WHERE attr=2	23	19	v2	2020-11-16 17:03:13.675-03	\N
47	Validar cosas	Cosas que validan cosas	30	20	1.0	2020-11-16 17:03:59.936775-03	\N
48	Ramificar ramas	Cosas ramificadas que ramifican ramas	12	21	1.0	2020-11-16 17:04:19.226706-03	\N
\.

--
-- TOC entry 3563 (class 0 OID 171792)
-- Dependencies: 253
-- Data for Name: proyecto_itematributo; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.proyecto_itematributo (id, valor, atributo_id, item_id) FROM stdin;
1	true	17	1
2		18	1
3	false	17	2
4		18	2
5	true	17	3
6		18	3
7	true	19	4
8		20	4
9	false	17	5
10		18	5
11	true	17	6
12		18	6
13	true	17	7
14		18	7
15		14	8
16		15	8
17		16	8
18		14	9
19		15	9
20		16	9
21		14	10
22		15	10
23		16	10
24	true	8	11
25		9	11
26	true	8	12
27		9	12
28	true	8	13
29		9	13
30	true	8	14
31		9	14
32	false	8	15
33		9	15
34	Sí	10	16
35	true	11	16
36		10	17
37	false	11	17
38		10	18
39	false	11	18
40		10	19
41	false	11	19
42	true	12	20
43		13	20
44	false	12	21
45		13	21
\.

--
-- TOC entry 3579 (class 0 OID 171989)
-- Dependencies: 269
-- Data for Name: proyecto_itematributoversion; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.proyecto_itematributoversion (id, valor, item_atributo_id, item_version_id) FROM stdin;
1	true	1	1
2		2	1
3	false	3	2
4		4	2
5	true	5	3
6		6	3
7	true	7	4
8		8	4
9	false	9	5
10		10	5
11	true	11	6
12		12	6
13	true	13	7
14		14	7
15	true	13	8
16		14	8
17	true	1	9
18		2	9
19	false	3	10
20		4	10
21	true	7	11
22		8	11
23	true	11	12
24		12	12
25	true	5	13
26		6	13
27	false	9	14
28		10	14
29		15	15
30		16	15
31		17	15
32	true	11	16
33		12	16
34	true	1	17
35		2	17
36	false	3	18
37		4	18
38	true	7	19
39		8	19
40		18	20
41		19	20
42		20	20
43		21	21
44		22	21
45		23	21
46	false	9	22
47		10	22
48	true	5	23
49		6	23
50	true	1	24
51		2	24
52	true	11	25
53		12	25
54		15	26
55		16	26
56		17	26
57		18	27
58		19	27
59		20	27
60		21	28
61		22	28
62		23	28
63	true	24	29
64		25	29
65	true	26	30
66		27	30
67	true	28	31
68		29	31
69	true	30	32
70		31	32
71	false	32	33
72		33	33
73	true	24	34
74		25	34
75	true	26	35
76		27	35
77	true	28	36
78		29	36
79	true	30	37
80		31	37
81	false	32	38
82		33	38
83	Sí	34	39
84	true	35	39
85		36	40
86	false	37	40
87		38	41
88	false	39	41
89		40	42
90	false	41	42
91	Sí	34	43
92	true	35	43
93		36	44
94	false	37	44
95		38	45
96	false	39	45
97		40	46
98	false	41	46
99	true	42	47
100		43	47
101	false	44	48
102		45	48
\.

--
-- TOC entry 3561 (class 0 OID 171784)
-- Dependencies: 251
-- Data for Name: proyecto_itemrelacion; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.proyecto_itemrelacion (id, relacion_tipo, item_id, item_relacion_id) FROM stdin;
5	p	3	2
6	p	5	4
8	p	1	7
9	p	2	7
10	p	4	7
11	p	6	7
13	p	10	8
14	s	5	9
15	s	3	10
16	s	1	8
17	s	6	9
18	p	9	8
19	p	12	11
20	p	13	11
21	p	14	12
22	p	15	12
23	p	17	16
24	p	18	16
25	p	19	16
\.

--
-- TOC entry 3577 (class 0 OID 171981)
-- Dependencies: 267
-- Data for Name: proyecto_itemrelacionversion; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.proyecto_itemrelacionversion (id, relacion_tipo, item_relacion_id, item_version_id, item_id) FROM stdin;
1	p	7	8	1
2	p	7	8	2
3	p	7	8	4
4	p	7	8	6
5	p	7	9	1
6	p	2	10	3
7	p	7	10	2
8	p	4	11	5
9	p	7	11	4
10	p	7	12	6
11	p	2	13	3
12	p	4	14	5
13	p	7	16	6
14	p	7	17	1
15	p	2	18	3
16	p	7	18	2
17	p	4	19	5
18	p	7	19	4
19	p	4	22	5
20	s	9	22	5
21	p	2	23	3
22	s	10	23	3
23	p	7	24	1
24	s	8	24	1
25	p	7	25	6
26	s	9	25	6
27	p	8	26	10
28	p	8	26	9
29	s	8	26	1
30	p	8	27	9
31	s	9	27	5
32	s	9	27	6
33	p	8	28	10
34	s	10	28	3
35	p	11	34	12
36	p	11	34	13
37	p	12	35	14
38	p	12	35	15
39	p	11	35	12
40	p	11	36	13
41	p	12	37	14
42	p	12	38	15
43	p	16	43	17
44	p	16	43	18
45	p	16	43	19
46	p	16	44	17
47	p	16	45	18
48	p	16	46	19
\.

--
-- TOC entry 3573 (class 0 OID 171944)
-- Dependencies: 263
-- Data for Name: proyecto_usuariodecomitedecambios; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.proyecto_usuariodecomitedecambios (id, proyecto_id, usuario_id) FROM stdin;
1	3	1
2	3	4
3	3	6
\.

--
-- TOC entry 3565 (class 0 OID 171832)
-- Dependencies: 255
-- Data for Name: lineaBase_lineabase; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."lineaBase_lineabase" (id, nombre, version, fecha_creacion, fase_id, usuario_creador_id, estado) FROM stdin;
1	LB-1-1	1.0	2020-11-16 16:24:12.805699-03	7	1	ac
2	LB-1-2	1.0	2020-11-16 16:46:17.272506-03	7	1	ac
3	LB-1-3	1.0	2020-11-16 16:46:23.117723-03	7	1	ac
4	LB-2-1	1.0	2020-11-16 16:51:55.752773-03	8	1	ac
5	LB-2-2	1.0	2020-11-16 16:52:00.312414-03	8	1	ac
\.


--
-- TOC entry 3567 (class 0 OID 171840)
-- Dependencies: 257
-- Data for Name: lineaBase_detallelbitems; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."lineaBase_detallelbitems" (id, item_id, linea_base_id) FROM stdin;
1	7	1
2	6	1
3	1	1
4	2	2
5	3	2
6	4	3
7	5	3
8	9	4
9	10	4
10	8	5
\.





--
-- TOC entry 3644 (class 0 OID 0)
-- Dependencies: 197
-- Name: Usuarios_contacto_contacto_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Usuarios_contacto_contacto_id_seq"', 1, false);


--
-- TOC entry 3645 (class 0 OID 0)
-- Dependencies: 200
-- Name: Usuarios_usuario_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Usuarios_usuario_groups_id_seq"', 1, false);


--
-- TOC entry 3646 (class 0 OID 0)
-- Dependencies: 201
-- Name: Usuarios_usuario_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Usuarios_usuario_id_seq"', 6, true);


--
-- TOC entry 3647 (class 0 OID 0)
-- Dependencies: 203
-- Name: Usuarios_usuario_user_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."Usuarios_usuario_user_permissions_id_seq"', 1, false);


--
-- TOC entry 3648 (class 0 OID 0)
-- Dependencies: 205
-- Name: account_emailaddress_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.account_emailaddress_id_seq', 6, true);


--
-- TOC entry 3649 (class 0 OID 0)
-- Dependencies: 207
-- Name: account_emailconfirmation_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.account_emailconfirmation_id_seq', 1, false);


--
-- TOC entry 3650 (class 0 OID 0)
-- Dependencies: 209
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.auth_group_id_seq', 1, false);


--
-- TOC entry 3651 (class 0 OID 0)
-- Dependencies: 211
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.auth_group_permissions_id_seq', 1, false);


--
-- TOC entry 3652 (class 0 OID 0)
-- Dependencies: 213
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.auth_permission_id_seq', 156, true);


--
-- TOC entry 3653 (class 0 OID 0)
-- Dependencies: 215
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.django_admin_log_id_seq', 1, false);


--
-- TOC entry 3654 (class 0 OID 0)
-- Dependencies: 217
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.django_content_type_id_seq', 39, true);


--
-- TOC entry 3655 (class 0 OID 0)
-- Dependencies: 219
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.django_migrations_id_seq', 58, true);


--
-- TOC entry 3656 (class 0 OID 0)
-- Dependencies: 222
-- Name: django_site_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.django_site_id_seq', 1, true);


--
-- TOC entry 3657 (class 0 OID 0)
-- Dependencies: 224
-- Name: fases_proyectorolusuariofase_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.fases_proyectorolusuariofase_id_seq', 1, false);


--
-- TOC entry 3658 (class 0 OID 0)
-- Dependencies: 226
-- Name: lineaBase_detallelbitems_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."lineaBase_detallelbitems_id_seq"', 10, true);


--
-- TOC entry 3659 (class 0 OID 0)
-- Dependencies: 228
-- Name: lineaBase_lineabase_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."lineaBase_lineabase_id_seq"', 5, true);


--
-- TOC entry 3660 (class 0 OID 0)
-- Dependencies: 230
-- Name: lineaBase_lineabaserompersolicitudes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."lineaBase_lineabaserompersolicitudes_id_seq"', 1, false);


--
-- TOC entry 3661 (class 0 OID 0)
-- Dependencies: 232
-- Name: lineaBase_romperlineabasevotos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public."lineaBase_romperlineabasevotos_id_seq"', 1, false);


--
-- TOC entry 3662 (class 0 OID 0)
-- Dependencies: 234
-- Name: proyecto_atributodinamico_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.proyecto_atributodinamico_id_seq', 20, true);


--
-- TOC entry 3663 (class 0 OID 0)
-- Dependencies: 236
-- Name: proyecto_fase_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.proyecto_fase_id_seq', 8, true);


--
-- TOC entry 3664 (class 0 OID 0)
-- Dependencies: 238
-- Name: proyecto_item_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.proyecto_item_id_seq', 21, true);


--
-- TOC entry 3665 (class 0 OID 0)
-- Dependencies: 240
-- Name: proyecto_itematributo_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.proyecto_itematributo_id_seq', 45, true);


--
-- TOC entry 3666 (class 0 OID 0)
-- Dependencies: 242
-- Name: proyecto_itematributoversion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.proyecto_itematributoversion_id_seq', 102, true);


--
-- TOC entry 3667 (class 0 OID 0)
-- Dependencies: 244
-- Name: proyecto_itemhistorial_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.proyecto_itemhistorial_id_seq', 1, false);


--
-- TOC entry 3668 (class 0 OID 0)
-- Dependencies: 246
-- Name: proyecto_itemrelacion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.proyecto_itemrelacion_id_seq', 25, true);


--
-- TOC entry 3669 (class 0 OID 0)
-- Dependencies: 248
-- Name: proyecto_itemrelacionversion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.proyecto_itemrelacionversion_id_seq', 48, true);


--
-- TOC entry 3670 (class 0 OID 0)
-- Dependencies: 250
-- Name: proyecto_itemversion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.proyecto_itemversion_id_seq', 48, true);


--
-- TOC entry 3671 (class 0 OID 0)
-- Dependencies: 252
-- Name: proyecto_proyecto_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.proyecto_proyecto_id_seq', 4, true);


--
-- TOC entry 3672 (class 0 OID 0)
-- Dependencies: 254
-- Name: proyecto_proyectorol_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.proyecto_proyectorol_id_seq', 6, true);


--
-- TOC entry 3673 (class 0 OID 0)
-- Dependencies: 256
-- Name: proyecto_proyectousuario_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.proyecto_proyectousuario_id_seq', 12, true);


--
-- TOC entry 3674 (class 0 OID 0)
-- Dependencies: 258
-- Name: proyecto_rolusuariofase_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.proyecto_rolusuariofase_id_seq', 63, true);


--
-- TOC entry 3675 (class 0 OID 0)
-- Dependencies: 260
-- Name: proyecto_tipodeitem_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.proyecto_tipodeitem_id_seq', 9, true);


--
-- TOC entry 3676 (class 0 OID 0)
-- Dependencies: 262
-- Name: proyecto_usuariodecomitedecambios_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.proyecto_usuariodecomitedecambios_id_seq', 3, true);


--
-- TOC entry 3677 (class 0 OID 0)
-- Dependencies: 264
-- Name: proyecto_usuarioproyecto_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.proyecto_usuarioproyecto_id_seq', 1, false);


--
-- TOC entry 3678 (class 0 OID 0)
-- Dependencies: 266
-- Name: proyecto_usuariorolproyecto_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.proyecto_usuariorolproyecto_id_seq', 1, false);


--
-- TOC entry 3679 (class 0 OID 0)
-- Dependencies: 268
-- Name: roles_permiso_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.roles_permiso_id_seq', 27, true);


--
-- TOC entry 3680 (class 0 OID 0)
-- Dependencies: 270
-- Name: roles_rol_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.roles_rol_id_seq', 6, true);


--
-- TOC entry 3681 (class 0 OID 0)
-- Dependencies: 272
-- Name: roles_rolpermiso_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.roles_rolpermiso_id_seq', 26, true);


--
-- TOC entry 3682 (class 0 OID 0)
-- Dependencies: 274
-- Name: roles_rolusuario_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.roles_rolusuario_id_seq', 1, false);


--
-- TOC entry 3683 (class 0 OID 0)
-- Dependencies: 276
-- Name: socialaccount_socialaccount_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.socialaccount_socialaccount_id_seq', 6, true);


--
-- TOC entry 3684 (class 0 OID 0)
-- Dependencies: 278
-- Name: socialaccount_socialapp_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.socialaccount_socialapp_id_seq', 1, true);


--
-- TOC entry 3685 (class 0 OID 0)
-- Dependencies: 280
-- Name: socialaccount_socialapp_sites_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.socialaccount_socialapp_sites_id_seq', 1, true);


--
-- TOC entry 3686 (class 0 OID 0)
-- Dependencies: 282
-- Name: socialaccount_socialtoken_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.socialaccount_socialtoken_id_seq', 1, true);


