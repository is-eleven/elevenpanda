#!/bin/bash

cd ..
cd elevenpandaApp
. venv/bin/activate

echo ""
echo "##########################"
echo "Iniciando test de Usuarios"
echo "##########################"
echo ""
python3 manage.py test Usuarios.tests
echo ""
echo "#########################################################"
echo "Iniciando test de Proyectos, ítems, tipo de ítems y fases"
echo "#########################################################"
echo ""
python3 manage.py test proyecto.tests
echo ""
echo "#######################"
echo "Iniciando test de Roles"
echo "#######################"
echo ""
python3 manage.py test roles.tests
echo ""
echo "############################"
echo "Iniciando test de Linea base"
echo "############################"
echo ""
python3 manage.py test lineaBase.tests
