#!/bin/bash

CLONE_FOLDER_NAME=elevenpanda
GIT_CLONE_DIR=https://gitlab.com/is-eleven/elevenpanda.git

##############################
# REMOVER DIRECTORIO SI EXISTE
##############################
[ -d "./$CLONE_FOLDER_NAME" ] && rm -rf ./$CLONE_FOLDER_NAME

########################
# CREAR NUEVO DIRECTORIO
########################
mkdir $CLONE_FOLDER_NAME

####################
# CLONAR REPOSITORIO
####################
git clone $GIT_CLONE_DIR ./$CLONE_FOLDER_NAME

######################
# CAMBIAR DEBUG A TRUE
######################
sed -i "s/DEBUG = False/DEBUG = True/gi" ./$CLONE_FOLDER_NAME/elevenpandaApp/elevenpandaApp/settings.py

#######################
# CREAR ENTORNO VIRTUAL
#######################
sudo apt-get install python-pip # Instalar pip
sudo pip install virtualenv # INstalar venv
virtualenv ./$CLONE_FOLDER_NAME/elevenpandaApp/venv # Crear venv
. ./$CLONE_FOLDER_NAME/elevenpandaApp/venv/bin/activate # Activar venv

#########################
# INSTALAR REQUERIMIENTOS
#########################
pip3 install -r ./$CLONE_FOLDER_NAME/elevenpandaApp/requirements.txt

#####################
# CREAR BASE DE DATOS
#####################
createdb -h localhost -p 5432 -U postgres elevenpandaDB

##################################
# MIGRAR TABLAS A LA BASE DE DATOS
##################################
python3 ./$CLONE_FOLDER_NAME/elevenpandaApp/manage.py makemigrations
python3 ./$CLONE_FOLDER_NAME/elevenpandaApp/manage.py migrate

######################
# POBLAR BASE DE DATOS
######################
while read line; do
psql -h localhost -d elevenpandaDB -U postgres -p 5432 -a -q -c "$line"
done < 'sqlScript.sql'

##################
# INICIAR SERVIDOR
##################
python3 ./$CLONE_FOLDER_NAME/elevenpandaApp/manage.py runserver


