# coding=utf-8
from django.http import HttpResponse
from django.shortcuts import render, redirect

from proyecto.models import Item
from proyecto.services import ServicioFases, ServicioProyectos, ServicioTipoDeItem, ServicioItems
from Usuarios.services import ServicioUsuario
from roles.services import ServicioRoles
from django.template.defaultfilters import register
from proyecto.contexts import tdis_context
from lineaBase.services import ServicioLineaBase

servicio_fases = ServicioFases()
servicio_proyectos = ServicioProyectos()
servicio_roles = ServicioRoles()
servicio_items = ServicioItems()
servicio_tdi = ServicioTipoDeItem()
servicio_usuarios = ServicioUsuario()
servicio_linea_base = ServicioLineaBase()

def get_url_prameter_int(request, param):
    """
    Convierte un parámetro pasado por POST a numérico
    :param request: request
    :param param: nombre del paramentro
    :return: parametro en numérico
    """
    paramStr = request.GET.get(param)
    if paramStr != None and paramStr.isdigit():
        return int(request.GET.get(param))
    return None

def fase_view(request, fase_id):
    fase = servicio_fases.get_fase(fase_id)
    proyecto = fase.proyecto
    es_gerente = proyecto.gerente is not None and proyecto.gerente.id == request.user.id
    visible = servicio_fases.usuario_tiene_permiso_en_fase(request.user, fase, 'ver_fase') or es_gerente
    if visible:
        prevFase = servicio_fases.get_fase_previa_a(fase)
        sigFase = servicio_fases.get_fase_siguiente_a(fase)
        tids = servicio_fases.get_tdis(fase)
        items = servicio_fases.get_items(fase_id)
        modItem = get_url_prameter_int(request, 'modItem')
        mostrarTrazo = get_url_prameter_int(request, 'mostrarTrazo')
        showVersion = get_url_prameter_int(request, 'sv')

        base_context = {
            'proyecto': proyecto,
            'fase': fase,
            'tdis': tids,
            'cant_lb': servicio_linea_base.contar_lb_en_fase(fase) + 1,
            'prevId': None if prevFase is None else prevFase.id,
            'sigId': None if sigFase is None else sigFase.id,
            'modificable': not fase.finalizado,
            'items': items,
            'ultimo_rf': servicio_items.get_ultimo_prefijo('RF', proyecto),
            'ultimo_rnf': servicio_items.get_ultimo_prefijo('RNF', proyecto),
            'directorios': [
                {'nombre': 'Proyectos', 'href': '/'},
                {'nombre': proyecto.nombre, 'href': '/proyecto/' + str(proyecto.id) + '/'},
                {'nombre': 'Fase ' + str(fase.numero_fase), 'href': '/fase/' + str(fase.id) + '/'}]
        }
        if modItem is not None:
            item = servicio_items.get_item(modItem)
            extra_context = {
                'modItem': item,
                'showLateral': item is not None,
                'editandoItem': True,
                'din_attrs': servicio_items.get_atributos_dinamicos(item),
                'items_fase': servicio_fases.get_items(fase),
                'lb': servicio_linea_base.get_linea_base_de_item(item),
            }
            base_context = {**base_context, **extra_context}
            # Show version details
            if showVersion is not None:
                item_version = servicio_items.get_item_version(showVersion)
                extra_context = {
                    'versionDeta': item_version,
                    'showVersionDeta': item_version is not None,
                    'din_attrsVersionData': servicio_items.get_atributos_dinamicos_de_version(item_version)
                }
                base_context = {**base_context, **extra_context}
        elif mostrarTrazo is not None:
            item = servicio_items.get_item(mostrarTrazo)
            items = servicio_items.get_trazo(item,fase)
            extra_context = {
                'itemTrazo': item,
                'items_trazo': items,
                'costo_total': servicio_items.get_trazo_costo_total(item),
                'costo_fase': servicio_items.get_trazo_costo_fase(item,fase)
            }
            base_context = {**base_context, **extra_context}
        return render(request, 'fases/menuFase.html', base_context)
    return HttpResponse("La página no existe o no tiene permiso para verla.")

def fase_config_view(request, fase_id):
    fase = servicio_fases.get_fase(fase_id)
    proyecto = fase.proyecto
    visible = True
    if visible:
        base_context = {
            'proyecto': proyecto,
            'fase': fase,
            'modificable': not fase.finalizado,
            'lbs': servicio_linea_base.get_lineas_base_de_fase(fase),
            'directorios': [
                {'nombre': 'Proyectos', 'href': '/'},
                {'nombre': proyecto.nombre, 'href': '/proyecto/' + str(proyecto.id) + '/'},
                {'nombre': 'Fase ' + str(fase.numero_fase), 'href': '/fase/' + str(fase.id) + '/'},
                {'nombre': 'Configuración', 'href': '/fase/' + str(fase.id) + '/config/'}]
        }
        # Agregar tdi
        if request.GET.get('agregar_tdi'):
            extra_context = {
                'agregar_tdi': True,
                'tdis_disponibles': servicio_proyectos.get_tipos_de_item_disponibles(proyecto),
            }
            base_context = {**base_context, **extra_context}
        # Miembros de la fase
        ordenar_por = request.GET.get('ordenarPor')
        filtro = request.GET.get('filtro')
        miembros = servicio_proyectos.get_miembros(proyecto, False, ordenar_por, filtro)
        extra_context = {
            'listaUsuarios': miembros,
            'cantidadUsuariosActivos': len(miembros),
            'mostrarMiembros': True,
            'soloMostrarMiembrosActivos': True,
            'mostrarMiembrosEnFase': True,
        }
        base_context = {**base_context, **extra_context}
        # Tdis de la fase
        base_context = {**base_context, **tdis_context(request, fase, 'fa')}
        # Agregar rol de fase a miembro
        mod_miembro = get_url_prameter_int(request, 'modMiembro')
        if mod_miembro is not None:
            roles_existentes = servicio_fases.get_roles_de_usuario_en_fase(mod_miembro, fase)
            roles_de_usuario_faltantes = servicio_proyectos.get_roles_faltantes(roles_existentes, proyecto, 'fa')
            extra_context = {
                'modMiembro': mod_miembro,
                'rolesFaltantes': roles_de_usuario_faltantes,
            }
            base_context = {**base_context, **extra_context}
        return render(request, 'fases/faseConfig.html', base_context)
    return HttpResponse("La página no existe o no tiene permiso para verla.")

def reporte_de_items_de_fase(request, fase_id):
    fase = servicio_fases.get_fase(fase_id)
    base_context = {
        'fase': fase,
        'items': servicio_fases.get_items(fase),
    }
    return render(request, 'informes/reporteItemsDeFase.html', base_context)

def agregar_rol_de_fase_a_miembro(request):
    """
    View de recepcion para asignar un rol de fase a un miembro del proyecto.

    :param request:
    :return:
    """
    miembro = request.POST.get('miembro')
    rol = request.POST.get('rol')
    fase = request.POST.get('fase')
    href = request.POST.get('href')
    servicio_fases.agregar_rol_de_fase_a_miembro(miembro, rol, fase)
    return redirect(href)

def remover_rol_de_fase_a_miembro(request):
    """
    View de recepcion para remover un rol de fase a un miembro del proyecto
    :param request:
    :return:
    """
    miembro = request.POST.get('miembro')
    rol = request.POST.get('rol')
    fase = request.POST.get('fase')
    href = request.POST.get('href')
    servicio_fases.remover_rol_de_usuario_en_fase(miembro, rol, fase)
    return redirect(href)

def modificar_item(request):
    """
    View de recepcion para modificacion de items
    :param request:
    :return:
    """
    # Modificar ítem
    nombre = request.POST.get('nombre')
    descripcion = request.POST.get('descripcion')
    costo = request.POST.get('costo')
    estado = request.POST.get('estado')
    #usuario = request.POST.get('usuario')
    item = request.POST.get('item')
    atributos_param = request.POST.get('atributos')
    atributos = [pair.split('<$s>') for pair in atributos_param.split('<$n>')] if atributos_param is not None else None
    # item_ref, nombre, descripcion, costo, estado, atributos_pair):
    servicio_items.modificar_item(item, nombre, descripcion, costo, estado, atributos, request.user)
    # Redirigir
    href = request.POST.get('href')
    return redirect(href)

def agregar_item(request, fase_id):
    """
    Views de recepcion para agregar items
    :param request:
    :return:
    """
    # Obtener atributos
    atributos_pair = request.POST.get('atributos').split('<$n>')
    atributos = [pair.split('<$s>') for pair in atributos_pair]
    # Crear ítem
    nombre = request.POST.get('nombre')
    descripcion = request.POST.get('descripcion')
    costo = request.POST.get('costo')
    prefijo = request.POST.get('prefijo')
    codigo = request.POST.get('codigo')
    version = request.POST.get('version')
    tdi = servicio_tdi.get_tipo_de_item(request.POST.get('tdi'))
    servicio_items.agregar_item(nombre, prefijo, codigo, tdi, fase_id, descripcion, costo, atributos, version)
    # Redirect
    href = request.POST.get('href')
    return redirect(href)

def asignar_relacion_item(request):
    """
    View de recepcion para agregar relaciones entre items
    :param request:
    return:
    """
    item = request.POST.get('item')
    item_relacion = request.POST.get('itemRelacion')
    relacion_tipo = request.POST.get('relacionTipo')
    href = request.POST.get('href')
    servicio_items.agregar_relacion(item, item_relacion, relacion_tipo)
    return redirect(href)

def eliminar_relacion_item(request):
    """
    View de recepcion para eliminar relaciones entre items
    :param request:
    return:
    """
    item = request.POST.get('item')
    item_relacion = request.POST.get('itemRelacion')
    href = request.POST.get('href')
    servicio_items.eliminar_relacion(item, item_relacion)
    return redirect(href)

def asignar_atributo(request):
    """
    View de recepcion para asignar atributos a items
    :param request:
    return:
    """
    item = request.POST.get('item')
    atributo = request.POST.get('atributo')
    valor = request.POST.get('valor')
    href = request.POST.get('href')
    servicio_items.agregar_valor_a_atributo(item, atributo, valor)
    return redirect(href)

def finalizar_fase(request, fase_id):
    """
    Finaliza una fase
    :param request: datos de la solicitud
    :param fase_id: fase a finalizar
    :return:
    """
    servicio_fases.finalizar_fase(fase_id)
    href = request.POST.get('href')
    return redirect(href)

def modificar_version(request):
    """
    View de versionado de items
    :param request:
    :return:
    """
    item = request.POST.get('item')
    version = request.POST.get('version')
    usuario = request.POST.get('usuario')
    servicio_items.cambiar_a_version(item, version, usuario)
    href = request.POST.get('href')
    return redirect(href)

def crear_version(request):
    """
    View de creacion de versiones de item
    :param request:
    :return:
    """
    item = request.POST.get('item')
    version = request.POST.get('version')
    servicio_items.crear_nueva_version_de_item(item, version)
    href = request.POST.get('href')
    return redirect(href)

def deshacer_cambios(request):
    """
    Deshace cambios recientes en un item
    :param request:
    :return:
    """
    item = request.POST.get('item')
    servicio_items.deshacer_cambios(item, request.user.id)
    href = request.POST.get('href')
    return redirect(href)

@register.filter
def roles_de_usuario_en_fase(miembro, fase):
    return servicio_fases.get_roles_de_usuario_en_fase(miembro, fase)

@register.filter
def cantidad_de_roles_de_usuario_en_fase_faltantes(usuario, fase):
    roles_existentes = servicio_fases.get_roles_de_usuario_en_fase(usuario, fase)
    roles_de_usuario_faltantes = servicio_proyectos.get_roles_faltantes(roles_existentes, fase.proyecto, 'fa')
    return roles_de_usuario_faltantes.count()

@register.filter
def item_relaciones(item_ref, relacion_tipo):
    return servicio_items.get_relaciones(item_ref, relacion_tipo)

@register.filter
def puede_agregarse_a_lb(item_ref):
    return servicio_linea_base.item_puede_agregarse_a_lb(item_ref)

@register.filter
def hijos_posibles(item_ref):
    return servicio_items.get_hijos_posibles(item_ref)

@register.filter
def sucesores_posibles(item_ref):
    return servicio_items.get_sucesores_posibles(item_ref)

@register.filter
def usuario_fase(_usuario, _fase):
    return _usuario, _fase

@register.filter
def tiene_permiso_fase(_usuario_fase, permiso):
    _usuario, _fase = _usuario_fase
    usuario = servicio_usuarios.get_usuario(_usuario)
    fase = servicio_fases.get_fase(_fase)
    # gerente = servicio_proyectos.get_gerente(fase.proyecto)
    return servicio_fases.usuario_tiene_permiso_en_fase(usuario, fase, permiso) # or gerente.id == usuario.id

@register.filter
def puede_finalizar_fase(fase):
    return servicio_fases.puede_finalizar_fase(fase)

@register.filter
def item_relacion_padre_es_en_lista_items(relacion_padre, lista_items):
    for item in lista_items:
        if item.id == relacion_padre.item_relacion.id:
            return True
    return False

@register.filter
def get_historial(item):
    return servicio_items.get_historial(item)

@register.filter
def replace_enter_to_br(text):
    return text.replace('\n', '<br>')
