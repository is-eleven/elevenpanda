from django.urls import path
from fases.views import fase_view, fase_config_view, agregar_rol_de_fase_a_miembro, remover_rol_de_fase_a_miembro, \
    modificar_item, agregar_item, asignar_relacion_item, eliminar_relacion_item, asignar_atributo, finalizar_fase, \
    modificar_version, crear_version, deshacer_cambios, reporte_de_items_de_fase

urlpatterns = [
    path('<int:fase_id>/', fase_view),
    path('<int:fase_id>/config/', fase_config_view),
    path('miembro/rol/agregar/', agregar_rol_de_fase_a_miembro),
    path('miembro/rol/remover/', remover_rol_de_fase_a_miembro),
    path('item/modificar/', modificar_item),
    path('<int:fase_id>/item/agregar/', agregar_item),
    path('item/relacion/agregar/', asignar_relacion_item),
    path('item/relacion/eliminar/', eliminar_relacion_item),
    path('item/atributo/modificar/', asignar_atributo),
    path('<int:fase_id>/finalizar/', finalizar_fase),
    path('item/version/modificar/', modificar_version),
    path('item/version/crear/', crear_version),
    path('item/version/deshacer/', deshacer_cambios),
    path('<int:fase_id>/reporte/items/', reporte_de_items_de_fase),
]
