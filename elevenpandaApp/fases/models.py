from django.db import models
from proyecto.models import UsuarioRolProyecto, Fase


class ProyectoRolUsuarioFase(models.Model):
    usuario_rol_proyecto = models.ForeignKey(UsuarioRolProyecto, on_delete=models.CASCADE)
    fase = models.ForeignKey(Fase, on_delete=models.CASCADE)
