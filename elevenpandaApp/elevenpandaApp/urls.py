from django.conf.urls import url
from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from Usuarios.decorators import cuenta_activa_redireccion
from .views import redirect_to_login, checking_validation, redirect_to_checking_account, home
from allauth.account.views import account_inactive


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', home),
    path('accounts/signup/', redirect_to_login),
    path('accounts/checking-account/', checking_validation),
    path('accounts/confirm-email/', redirect_to_checking_account),
    path('usuario/', include('Usuarios.urls')),
    path('rol/', include('roles.urls')),
    path('proyecto/', include('proyecto.urls')),
    path('fase/', include('fases.urls')),
    path('lineaBase/', include('lineaBase.urls')),
    path('accounts/inactive/', cuenta_activa_redireccion(account_inactive)),
    url(r'^accounts/', include('allauth.urls')),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

"""
    path("signup/", views.signup, name="account_signup"),
    path("login/", views.login, name="account_login"),
    path("logout/", views.logout, name="account_logout"),
    path("password/change/", views.password_change,
         name="account_change_password"),
    path("password/set/", views.password_set, name="account_set_password"),
    path("inactive/", views.account_inactive, name="account_inactive"),

    # E-mail
    path("email/", views.email, name="account_email"),
    path("confirm-email/", views.email_verification_sent,
         name="account_email_verification_sent"),
    re_path(r"^confirm-email/(?P<key>[-:\w]+)/$", views.confirm_email,
            name="account_confirm_email"),

    # password reset
    path("password/reset/", views.password_reset,
         name="account_reset_password"),
    path("password/reset/done/", views.password_reset_done,
         name="account_reset_password_done"),
    re_path(r"^password/reset/key/(?P<uidb36>[0-9A-Za-z]+)-(?P<key>.+)/$",
            views.password_reset_from_key,
            name="account_reset_password_from_key"),
    path("password/reset/key/done/", views.password_reset_from_key_done,
         name="account_reset_password_from_key_done"),
"""