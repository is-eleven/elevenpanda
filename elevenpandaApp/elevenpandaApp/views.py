from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.template.defaultfilters import register

from Usuarios.decorators import cuenta_aprobada_requerida
from Usuarios.models import Usuario
from allauth.account import views as allauth_views

from Usuarios.services import ServicioUsuario
from proyecto.services import ServicioProyectos

servicio_proyectos = ServicioProyectos()
servicio_usuarios = ServicioUsuario()

@login_required
@cuenta_aprobada_requerida
def home(request):
    ##### Mostrar todos los proyectos del sistema?
    mostrar_todo = request.GET.get('todos') == 'True'
    lista_proyectos = servicio_proyectos.get_proyectos()
    todos_los_proyectos_personales = servicio_usuarios.get_proyectos_de_usuario(request.user)
    proyectos_personales_como_gerente = servicio_usuarios.get_proyectos_de_usuario_como_gerente(request.user)
    ##### Proyectos personales
    if not mostrar_todo:
        solo_soy_gerente = request.GET.get('soyGerente') == 'True'
        if solo_soy_gerente:
            lista_proyectos = proyectos_personales_como_gerente
        else:
            lista_proyectos = todos_los_proyectos_personales
    ##### Ordenar por
    ordenar_por = request.GET.get('ordenarPor')
    if ordenar_por is not None:
        parametro = 'nombre' if ordenar_por == 'nombre' else '-fecha_creacion' if ordenar_por == 'ultimoCreado' else 'fecha_creacion' if ordenar_por == 'primeroCreado' else None
        if parametro is not None:
            lista_proyectos = lista_proyectos.order_by(parametro)
    ##### filtro
    filtrar_por = request.GET.get('filtro')
    if filtrar_por is not None:
        estado = 'pe' if filtrar_por == 'pendientes' else 'ac' if filtrar_por == 'activos' else 'ca' if filtrar_por == 'cancelados' else 'fi'
        lista_proyectos_filtrada = []
        for proyecto_sin_filtrar in lista_proyectos:
            if proyecto_sin_filtrar.estado == estado:
                lista_proyectos_filtrada.append(proyecto_sin_filtrar)
        lista_proyectos = lista_proyectos_filtrada
    ##### Contexto
    context = {
        'hideLateral': True,
        'proyectos': lista_proyectos,
        'cantidadProyectosPersonales': len(todos_los_proyectos_personales),
        'cantidadProyectosSoyGerente': len(proyectos_personales_como_gerente),
    }
    return render(request, 'home.html', context)


@login_required(login_url='')
def checking_validation(request):
    usuario = Usuario.objects.filter(username=request.user.username).first()
    if usuario.aprobado:
        return redirect('/')
    return allauth_views.email_verification_sent(request)

@login_required(login_url='')
def redirect_to_login(request):
    return redirect('/accounts/login/')


@login_required(login_url='')
def redirect_to_checking_account(request):
    return redirect('/accounts/checking-account/')

@register.filter
def primera_letra(nombre):
    return nombre[0:1]

