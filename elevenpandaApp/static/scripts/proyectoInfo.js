
$(document).ready(function() {
    const urlParams = new URLSearchParams(window.location.search);
    if (urlParams.has('estadoImportacion')){
        const state = urlParams.get('estadoImportacion');
        if (state === 'correcto') {
            $.confirm({
                title: "Tipo de ítem importado correctamente",
                type: 'green',
                typeAnimated: true,
                columnClass: 'small',
                boxWidth: '30%',
                useBootstrap: false,
                icon: 'fas fa-check-circle',
                content: '',
                buttons: {
                    formSubmit: {
                        text: 'Ok',
                        action: function () {
                            removeUrlParam('estadoImportacion')
                        }
                    }
                },
                onContentReady: function () {
                    // bind to events
                    var jc = this;
                    this.$content.find('form').on('submit', function (e) {
                        // if the user submits the form by pressing enter in the field.
                        e.preventDefault();
                        jc.$$formSubmit.trigger('click'); // reference the button and click it
                    });
                }
            });
        }
        else if (state === 'error') {
            $.confirm({
                title: "Ocurrió un error durante la importación",
                type: 'orange',
                typeAnimated: true,
                columnClass: 'small',
                boxWidth: '30%',
                useBootstrap: false,
                icon: 'fas fa-exclamation-triangle',
                content: 'Es probable que un tipo de ítem con el mismo nombre ya exista en el proyecto donde desea ' +
                'importarlo. Verifique que esto no ocurra y vuelva a intentarlo.',
                buttons: {
                    formSubmit: {
                        text: 'Ok',
                        action: function () {
                            removeUrlParam('estadoImportacion')
                        }
                    }
                },
                onContentReady: function () {
                    // bind to events
                    var jc = this;
                    this.$content.find('form').on('submit', function (e) {
                        // if the user submits the form by pressing enter in the field.
                        e.preventDefault();
                        jc.$$formSubmit.trigger('click'); // reference the button and click it
                    });
                }
            });
        }
    }
});

//////////////////////////////////// TIPO DE ÍTEM DEL PROYECYO

function addItemType(proyectId) {
    $.confirm({
        title: "Agregar tipo de item",
        type: 'green',
        typeAnimated: true,
        columnClass: 'small',
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fas fa-puzzle-piece',
        content: '<strong>Ingrese el nombre del tipo de item</strong>' +
            '<p>Los atributos dinámicos pueden ser agregados luego seleccionando la opción de modificar el típo de item.</p>' +
        '<form action="" class="formName">' +
            '<div class="form-group">' +
            '<input id="tdi-name" placeholder="" class="confirm-value-input text-input" value="" required />' +
            '</div>' +
        '</form>',
        buttons: {
            formSubmit: {
                text: 'Guardar',
                btnClass: 'btn-blue',
                action: function () {
                    const name = this.$content.find('.confirm-value-input').val();
                    if(!name){
                        fastAlert('Error al intentar guardar.',
                            'red', 'fas fa-bomb');
                        return false;
                    }

                    /*const urlParams = new URLSearchParams(window.location.search);
                    urlParams.append('tdi', tdiId + "");
                    const href = window.location.href.split('?')[0] + '?' + urlParams;*/

                    $("#tdi-name").attr("value", name);
                    $("#tdi-project-id").attr("value", proyectId);
                    $("#href").attr("value", window.location.href);
                    $("#form-sender").attr('action', '/proyecto/tdi/agregar/');
                    $("#form-sender").submit();
                }
            },
            cancelar: function () {
                //close
            },
        },
        onContentReady: function () {
            // bind to events
            var jc = this;
            this.$content.find('form').on('submit', function (e) {
                // if the user submits the form by pressing enter in the field.
                e.preventDefault();
                jc.$$formSubmit.trigger('click'); // reference the button and click it
            });
        }
    });
}
function removeTdi(tdi) {
    $.confirm({
        title: "Remover tipo de item",
        type: 'red',
        typeAnimated: true,
        columnClass: 'small',
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fas fa-eraser',
        content: '<p>Confirmar la opración tendrá un efecto inmediato.</p><br><strong>Advertencia: </strong>' +
            '<p>Si el tipo de ítem ya ha sido asignado a un ítem, esta operación no tendrá efecto incluso si la confirma.</p>',
        buttons: {
            formSubmit: {
                text: 'Confirmar',
                btnClass: 'btn-red',
                action: function () {
                    /*const urlParams = new URLSearchParams(window.location.search);
                    urlParams.append('tdi', tdiId + "");
                    const href = window.location.href.split('?')[0] + '?' + urlParams;*/
                    $("#tdi-name").attr("value", tdi);
                    $("#href").attr("value", window.location.href);
                    $("#form-sender").attr('action', '/proyecto/tdi/remover/');
                    $("#form-sender").submit();
                }
            },
            cancelar: function () {
                //close
            },
        },
        onContentReady: function () {
            // bind to events
            var jc = this;
            this.$content.find('form').on('submit', function (e) {
                // if the user submits the form by pressing enter in the field.
                e.preventDefault();
                jc.$$formSubmit.trigger('click'); // reference the button and click it
            });
        }
    });
}
function closeTdiList() {
    const urlParams = new URLSearchParams(window.location.search);
    urlParams.delete('mostrarTdis');
    window.location.href = window.location.href.split('?')[0] + '?' + urlParams;
}
function editTdi(tdiId) {
    const urlParams = new URLSearchParams(window.location.search);
    if (urlParams.has('tdi')) urlParams.delete('tdi');
    urlParams.append('tdi', tdiId + "");
    window.location.href = window.location.href.split('?')[0] + '?' + urlParams;
}
function showImportTdi(){
    const urlParams = new URLSearchParams(window.location.search);
    if (urlParams.has('importarTdi')) urlParams.delete('importarTdi');
    urlParams.append('importarTdi',  "True");
    window.location.href = window.location.href.split('?')[0] + '?' + urlParams;
}
function closeImportTdi(){
    const urlParams = new URLSearchParams(window.location.search);
    urlParams.delete('importarTdi');
    window.location.href = window.location.href.split('?')[0] + '?' + urlParams;
}


//////////////////////////////////// ATRIBUTOS DE TIPO DE ÍTEM

function addTdiAtt() {
    //var tdi = getTdiById(tdiId);
    const urlParams = new URLSearchParams(window.location.search);
    if (urlParams.has('addTdiAtt')) urlParams.delete('addTdiAtt');
    urlParams.append('addTdiAtt', 'True');
    window.location.href = window.location.href.split('?')[0] + '?' + urlParams;
}
function deleteAtribute(atributeId) {
    $.confirm({
        title: "Remover atributo de tipo de item",
        type: 'red',
        typeAnimated: true,
        columnClass: 'small',
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fas fa-eraser',
        content: '<p>Confirmar la opración tendrá un efecto inmediato.</p><br><strong>Advertencia: </strong>' +
            '<p>Todos los ítems con este tipo de ítem asignado perderán inmediatamente la información relacionada al atributo.</p>',
        buttons: {
            formSubmit: {
                text: 'Confirmar',
                btnClass: 'btn-red',
                action: function () {
                    /*const urlParams = new URLSearchParams(window.location.search);
                    urlParams.append('tdi', tdiId + "");
                    const href = window.location.href.split('?')[0] + '?' + urlParams;*/
                    $("#atributoId").attr("value", atributeId);
                    $("#tdiHref").attr("value", window.location.href);
                    $("#tdi-form").attr('action', '/proyecto/tdi/atributo/remover/');
                    $("#tdi-form").submit();
                }
            },
            cancelar: function () {
                //close
            },
        },
        onContentReady: function () {
            // bind to events
            var jc = this;
            this.$content.find('form').on('submit', function (e) {
                // if the user submits the form by pressing enter in the field.
                e.preventDefault();
                jc.$$formSubmit.trigger('click'); // reference the button and click it
            });
        }
    });
}
function changeTdiName(id, nombre){
    $.confirm({
        title: "Modificar nombre de tipo de ítem",
        type: 'blue',
        typeAnimated: true,
        columnClass: 'small',
        boxWidth: '30%',
        useBootstrap: false,
        icon: 'fas fa-puzzle-piece',
        content: '<p>Ingrese el nuevo nombre para el tipo de ítem. Confirmar esta operación tendrá efecto inmediato sobre el proyecto.</p><br>' +
        '<form action="" class="formName">' +
            '<div class="form-group">' +
            '<input id="tdi-name" placeholder="" class="confirm-value-input text-input" value="'+ nombre +'" required autofocus/>' +
            '</div>' +
        '</form>',
        buttons: {
            formSubmit: {
                text: 'Guardar',
                btnClass: 'btn-blue',
                action: function () {
                    const name = this.$content.find('.confirm-value-input').val();
                    if(!name){
                        fastAlert('Error al intentar guardar.', error,
                            'red', 'fas fa-bomb');
                        return false;
                    }

                    /*const urlParams = new URLSearchParams(window.location.search);
                    urlParams.append('tdi', tdiId + "");
                    const href = window.location.href.split('?')[0] + '?' + urlParams;*/

                    $("#tdiNombreCambiado").attr("value", name);
                    $("#tdiId").attr("value", id);
                    $("#href").attr("value", window.location.href);
                    $("#tdi-form").attr('action', '/proyecto/tdi/cambiarnombre/');
                    $("#tdi-form").submit();
                }
            },
            cancelar: function () {
                //close
            },
        },
        onContentReady: function () {
            // bind to events
            var jc = this;
            this.$content.find('form').on('submit', function (e) {
                // if the user submits the form by pressing enter in the field.
                e.preventDefault();
                jc.$$formSubmit.trigger('click'); // reference the button and click it
            });
        }
    });
}
// CREAR NUEVO ATRIBUTO DE TIPO DE ITEM
$("#add-tdi-att-form").submit(function(e){

    //e.preventDefault();
    var form = this;
    const urlParams = new URLSearchParams(window.location.search);
    urlParams.delete('addTdiAtt');
    const href = window.location.href.split('?')[0] + '?' + urlParams;
    //$("#tdiId").attr("value", atributeId);
    $("#tdiAttId").attr("value", urlParams.get('tdi') + '');
    $("#atrHref").attr("value", href);
    //$(form).submit();
    return true;
});
// GUARDAR NOMBRE DE TIPO DE ITEM - CREO QUE FUNCIONA
$("#tdi-form").submit(function(e){
    $("#tdiHref").attr("value", window.location.href);
    return true;
});
function closeAtt() {
    const urlParams = new URLSearchParams(window.location.search);
    urlParams.delete('tdi');
    window.location.href = window.location.href.split('?')[0] + '?' + urlParams;
}
function closeAddAtt() {
    const urlParams = new URLSearchParams(window.location.search);
    urlParams.delete('addTdiAtt');
    window.location.href = window.location.href.split('?')[0] + '?' + urlParams;
}
