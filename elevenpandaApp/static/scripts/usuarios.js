contactsToRemove = [];
addedContacts = [];
addedRoles = [];
removedRoles = [];

function removeContact(buttonId, contactId) {
    const button = $('#' + buttonId)[0];
    if (contactsToRemove.includes(contactId)) {
        const index = contactsToRemove.indexOf(contactId);
        contactsToRemove.splice(index, 1);
        $(button).addClass('unselected-trash');
        $(button).removeClass('selected-trash');
    }
    else {
        $(button).removeClass('unselected-trash');
        $(button).addClass('selected-trash');
        contactsToRemove.push(contactId);
        fastAlert('Eliminación de contacto',
            "El contacto seleccionado será eliminado de la información de la cuenta una vez que guarde el formulario.",
            'red', 'fas fa-dumpster');
    }
}

function addNumberContact() {
    addContact('Agrega un número telefónico', 'blue', 'fas fa-phone',
        'Ingresa la información de contacto.', 'ej. 0989 123 456', 'te', 'test',
        'Rellene el campo y verifique que el número de teléfono o celular ingresado sea correcto. Utilice' +
        ' solo números y espacios.');
}
function addEmailContact() {
    addContact('Agrega una dirección de correo electrónico', 'blue', 'fas fa-at',
        'Ingresa la información de contacto.',
        'ej. miincreiblecorreo@pandamail.com', 'co', 'email',
        'Rellene el campo y verifique que la dirección de correo sea correcta.');
}
function addUrlContact() {
    addContact('Agrega una dirección web o URL', 'blue', 'fas fa-paper-plane',
        'Ingresa la información de contacto.',
        'ej. miinteresanteblog.com/elevanpanda', 'ur', 'url',
        'Rellene el campo y verifique que la dirección web o URL sea correcta.');
}
function addDirectionContact() {
    addContact('Agrega una dirección física', 'blue', 'fas fa-map-marker-alt',
        'Ingresa la información de contacto.',
        'ej. mi casa esq. la casa de mi vecino', 'di', 'text',
        'Rellene el campo antes de enviar el formulario..');
}

function addContact(title, color, icon, content1, placeholder, contactType, inputType='text',
                    error='Rellene todos los campos antes de guardar el formulario.') {
    $.confirm({
        title: title,
        type: color,
        typeAnimated: true,
        columnClass: 'small',
        boxWidth: '30%',
        useBootstrap: false,
        icon: icon,
        content: '' +
        '<form action="" class="formName">' +
        '<div class="form-group">' +
        '<label>'+ content1 +'</label><br><br>' +
        '<input type="' + inputType + '" placeholder="' + placeholder + '" class="confirm-value-input text-input" required />' +
        '</div>' +
        '</form>',
        buttons: {
            formSubmit: {
                text: 'Guardar',
                btnClass: 'btn-blue',
                action: function () {
                    const name = this.$content.find('.confirm-value-input').val();
                    if(!name || (contactType==='co' && !validateEmail(name)) || (contactType==='te' && !validatePhone(name))
                    || (contactType==='ur' && !validateURL(name))){
                        fastAlert('Error al intentar guardar.', error,
                            'red', 'fas fa-bomb');
                        return false;
                    }
                    addedContacts.push([name, contactType]);
                    fastAlert("Contacto registrado exitosamente.",
                        "Los nuevos datos serán mostrados luego de que guarde el formulario.",
                        'green',
                        'fas fa-save')
                }
            },
            cancelar: function () {
                //close
            },
        },
        onContentReady: function () {
            // bind to events
            var jc = this;
            this.$content.find('form').on('submit', function (e) {
                // if the user submits the form by pressing enter in the field.
                e.preventDefault();
                jc.$$formSubmit.trigger('click'); // reference the button and click it
            });
        }
    });
}

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}
function validatePhone(val) {
    return /^(\s*[0-9]+\s*)+$/.test(val);
}
function validateURL(str) {
  var pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
  '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.?)+[a-z]{2,}|'+ // domain name
  '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
  '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
  '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
  '(\\#[-a-z\\d_]*)?$','i'); // fragment locator
  return pattern.test(str);
}