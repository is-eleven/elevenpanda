function fastAlert(title, content, color, fontawesome) {
    $.alert({
            title: title,
            content: content,
            boxWidth: '35%',
            useBootstrap: false,
            icon: fontawesome,
        })
}

function stringIsNullOrWhitespace(input) {
    if (typeof input === 'undefined' || input == null) return true;
    return input.replace(/\s/g, '').length < 1;
}
function goto(href) {
    window.location.href = href;
}
function appendToUrl(line, removes) {
    var href = window.location.href + line;
    if (removes != null) {
        for (let i = 0; i < removes.length; i++) {
            href = href.replace(removes[i], "");
        }
    }
    goto(href);
}
function addModItemBlockPosition(name, urlParams) {
    let block = $('.group-block');
    if (name === 'modItem' && block) {
        let pos = $(block).position();
        if (urlParams.has('x')) urlParams.delete('x');
        urlParams.append('x', pos.left);
        if (urlParams.has('y')) urlParams.delete('y');
        urlParams.append('y', pos.top);
    }
}
//
// ADD AND REMOVE URL PARAMS
//
function hasUrlParam(param) {
    const urlParams = new URLSearchParams(window.location.search);
    return urlParams.has(param);
}
function getUrlParameterValue(param) {
    const urlParams = new URLSearchParams(window.location.search);
    if (urlParams.has(param)) return urlParams.get(param);
    return null;
}
function addUrlParam(name, value, removes = null, redirect=true) {
    const urlParams = new URLSearchParams(window.location.search);
    addModItemBlockPosition(name, urlParams);
    if (removes) removes.forEach(remove => {
        if (urlParams.has(remove)) urlParams.delete(remove)
    });
    if (urlParams.has(name)) urlParams.delete(name);
    urlParams.append(name, value);
    let url = window.location.href.split('?')[0] + '?' + urlParams;
    if (redirect) window.location.href = url;
    else return url;
}
function addUrlParamList(pairs, removes) {
    const urlParams = new URLSearchParams(window.location.search);
    addModItemBlockPosition(name, urlParams);
    if (removes)removes.forEach(remove=>{if(urlParams.has(remove))urlParams.delete(remove)});
    pairs.forEach(pair=>{
        if (urlParams.has(pair.name)) urlParams.delete(pair.name);
        urlParams.append(pair.name, pair.value);
    });
    window.location.href = window.location.href.split('?')[0] + '?' + urlParams;
}
function removeUrlParam(name, redirect=true) {
    const urlParams = new URLSearchParams(window.location.search);
    addModItemBlockPosition(name, urlParams);
    if (urlParams.has(name)) urlParams.delete(name);
    if (urlParams.has('estadoImportacion')) urlParams.delete('estadoImportacion');
    if (redirect)
        window.location.href = window.location.href.split('?')[0] + '?' + urlParams;
    else return window.location.href.split('?')[0] + '?' + urlParams;
}
function removeUrlParamList(names) {
    const urlParams = new URLSearchParams(window.location.search);
    addModItemBlockPosition(name, urlParams);
    for (let i = 0; i < names.length; i++){
        if (urlParams.has(names[i])) urlParams.delete(names[i]);
    }
    if (urlParams.has('estadoImportacion')) urlParams.delete('estadoImportacion');
    window.location.href = window.location.href.split('?')[0] + '?' + urlParams;
}

$('#input-buscar-enter').keypress(function(event){
	var keycode = (event.keyCode ? event.keyCode : event.which);
	if(keycode === 13){
	    addUrlParam('buscar', $(this).val());
	}
});
