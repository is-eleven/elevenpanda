function aprobarRegistroDeUsuario(username) {
    $.confirm({
        title: 'Confirmar aprobación de usuario',
        content: 'El registro del usuario será aprobado y podrá acceder al sistema.  No podrá deshacer esta acción una vez hecha.',
        icon: 'fas fa-user-cleck',
        boxWidth: '30%',
        useBootstrap: false,
        buttons: {
            confirmar: function () {
                sendRequest(
                    '/usuario/aprobar/',
                    {
                        'usuario': username,
                        'href': window.location.href
                    }
                );
            },
            cancelar: function () { }
        }
    });
}

function desactivarUsuario(username) {
    $.confirm({
        title: 'Confirmar desactivación de usuario',
        content: 'La cuenta del usuario seleccionado quedará desactivada y no podrá iniciar sesión en el sistema.',
        icon: 'fas fa-user-slash',
        boxWidth: '30%',
        useBootstrap: false,
        buttons: {
            confirmar: function () {
                sendRequest(
                    '/usuario/desactivar/',
                    {
                        'usuario': username,
                        'href': window.location.href
                    }
                );
            },
            cancelar: function () { }
        }
    });
}

function activarUsuario(username) {
    $.confirm({
        title: 'Confirmar la reactivación de usuario',
        content: 'La cuenta del usuario seleccionado quedará habilitada y podrá iniciar sesión en el sistema.',
        icon: 'fas fa-user-check',
        boxWidth: '30%',
        useBootstrap: false,
        buttons: {
            confirmar: function () {
                sendRequest(
                    '/usuario/activar/',
                    {
                        'usuario': username,
                        'href': window.location.href
                    }
                );
            },
            cancelar: function () { }
        }
    });
}
