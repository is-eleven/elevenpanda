function sendRequest(path, parameters, method='POST') {
    const form = document.createElement("form");
    form.setAttribute("method", method);
    form.setAttribute("action", path);
    const csrf = document.createElement("input");
    csrf.setAttribute("type", "hidden");
    csrf.setAttribute("name", "csrfmiddlewaretoken");
    csrf.setAttribute("value", csrfToken);
    form.appendChild(csrf);

    for (let key in parameters) {
        if (parameters.hasOwnProperty(key)) {
            const field = document.createElement("input");
            field.setAttribute("type", "hidden");
            field.setAttribute("name", key);
            field.setAttribute("value", parameters[key]);
            form.appendChild(field);
        }
    }
    document.body.appendChild(form);
    form.submit();
}