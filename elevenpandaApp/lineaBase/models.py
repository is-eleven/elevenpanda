from django.db import models
from proyecto.models import Item
from fases.models import Fase
from Usuarios.models import Usuario


class LineaBase(models.Model):
    """
    Clase que modela la idea de lineas base
    """
    nombre = models.CharField(max_length=50)
    version = models.CharField(max_length=50, default="1.0")
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fase = models.ForeignKey(Fase, on_delete=models.CASCADE)
    estado = models.CharField(max_length=5, default="ac")
    usuario_creador = models.ForeignKey(Usuario, on_delete=models.CASCADE)


class DetalleLbItems(models.Model):
    """
    Clase intermedia que relaciona una linea base con un item
    """
    linea_base = models.ForeignKey(LineaBase, on_delete=models.CASCADE)
    item = models.ForeignKey(Item, on_delete=models.CASCADE)


class LineaBaseRomperSolicitudes(models.Model):
    """
    Clase que modela las solicitudes de ruptura de lineas base
    """
    linea_base = models.ForeignKey(LineaBase, on_delete=models.CASCADE)
    usuario_solicitante = models.ForeignKey(Usuario, on_delete=models.CASCADE)
    motivo = models.TextField()
    fecha_solicitud = models.DateTimeField(auto_now_add=True)
    resuelto = models.BooleanField(default=False)


class RomperLineaBaseVotos(models.Model):
    """
    Clase que modela los votos de las solicitudes de ruptura de lineas base
    """
    solicitud = models.ForeignKey(LineaBaseRomperSolicitudes, on_delete=models.CASCADE)
    usuario = models.ForeignKey(Usuario, on_delete=models.CASCADE)
    voto = models.BooleanField(null=True, blank=True, default=None)
