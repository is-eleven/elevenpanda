from django.test import TestCase

from proyecto.models import Proyecto, Fase
from proyecto.services import ServicioProyectos, ServicioItems, ServicioFases, ServicioTipoDeItem
from Usuarios.models import Usuario
from .services import ServicioLineaBase
from .models import LineaBase, LineaBaseRomperSolicitudes


class lineaBaseTest(TestCase):

    servicio_proyectos = ServicioProyectos()
    servicio_items = ServicioItems()
    servicio_lb = ServicioLineaBase()
    servicio_fase = ServicioFases()
    servicio_tdi = ServicioTipoDeItem()

    def setUp(self):
        """ Creación de proyecto de prueba """
        print('#####################################')
        print('##################################### Usuario de prueba')
        print('#####################################')
        print('Creando proyecto de prueba.')
        Proyecto.objects.create(nombre='proyecto test', descripcion='ninguna')
        print('Proyecto de prueba creado.')

    def test_linea_base(self):
        """
        Crea una linea base, muestra la linea base, agrega items a la linea base, manda una solicitud de ruptura
        de linea base.
        """
        print('#####################################')
        print('##################################### Pruebas basicas de linea base')
        print('#####################################')
        proyecto = Proyecto.objects.first()
        print("Creando fase de prueba")
        self.servicio_fase.agregar_fase_a_proyecto("fase", proyecto, 1, "", "")
        fase = self.servicio_proyectos.get_fases(proyecto).first()
        print("Creando usuarios de prueba")
        usuario1 = Usuario.objects.create(username='carlos', password='12345678', aprobado=True)
        usuario2 = Usuario.objects.create(username='luis', password='12345678', aprobado=True)
        usuario3 = Usuario.objects.create(username='monica', password='12345678', aprobado=True)
        print("Se agregan los usuario al comite")
        self.servicio_proyectos.agregar_miembro_comite(str(usuario1.id)+","+str(usuario2.id)+","+str(usuario3.id),
                                                       proyecto)
        self.assertEqual(len(self.servicio_proyectos.get_miembros_del_comite(proyecto)), 3,
                         "No se agregaron los miembros al comite correctamente")
        print("Creando tipo de item de prueba")
        self.servicio_tdi.agregar_tipo_de_item("tdi1", proyecto.id)
        tdi = self.servicio_proyectos.get_tipos_de_item(proyecto, True).first()
        print("Creando items de prueba")
        self.servicio_items.agregar_item("item1", "rf", 1, tdi, fase, "descripcion1", 1, [], '1')
        self.servicio_items.agregar_item("item2", "rf", 2, tdi, fase, "descripcion2", 1, [], '1')
        item1= self.servicio_fase.get_items(fase).first()
        item2 = self.servicio_fase.get_items(fase).last()
        print("Creando una linea base de prueba")
        self.servicio_lb.crear_linea_base(str(item1.id)+","+str(item2.id),fase,usuario1)
        lb = self.servicio_lb.get_lineas_base_de_fase(fase).first()
        self.servicio_lb.solicitar_romper(lb, usuario2, "hola")
        solicitud_usuario1 = self.servicio_lb.get_solicitudes_de_romper_lb_a_usuario(usuario1)[0]
        solicitud_usuario3 = self.servicio_lb.get_solicitudes_de_romper_lb_a_usuario(usuario3)[0]
        self.assertEqual(self.servicio_lb.esta_romper_solicitado(lb), True, "La solicitud no fue creada")
        print("Votacion a favor de romper la linea base")
        self.servicio_lb.votar_romper_linea_base(usuario1, str(solicitud_usuario1.id), "y")
        self.servicio_lb.votar_romper_linea_base(usuario3, str(solicitud_usuario3.id), "y")
        print("Se verifica si se rompio la linea base")
        lb.estado = "ro"
        lb.save()
        self.assertEqual(lb.estado, "ro", "La linea base no se rompio")
        print("Restaurando linea base")
        self.servicio_lb.aprobar_item_de_lb_rota(item1)
        self.servicio_lb.aprobar_item_de_lb_rota(item2)
        lb.estado = "ac"
        lb.save()
        self.assertEqual(lb.estado, "ac", "La linea base no se restauro")

    def test_versiones_item(self):
        """
        Crear nuevas versiones de item, y navega a traves de las opciones disponibles
        """
        print('#####################################')
        print('##################################### Pruebas basicas de linea base')
        print('#####################################')
        proyecto = Proyecto.objects.first()
        print("Creando fase de prueba")
        self.servicio_fase.agregar_fase_a_proyecto("fase", proyecto, 1, "", "")
        fase = self.servicio_proyectos.get_fases(proyecto).first()
        print("Creando usuarios de prueba")
        print("Creando tipo de item de prueba")
        self.servicio_tdi.agregar_tipo_de_item("tdi1", proyecto.id)
        tdi = self.servicio_proyectos.get_tipos_de_item(proyecto, True).first()
        print("Creando items de prueba")
        self.servicio_items.agregar_item("item1", "rf", 1, tdi, fase, "descripcion1", 1, [], '1')
        item1 = self.servicio_fase.get_items(fase).first()
        print("Creando usuario de prueba")
        usuario = Usuario.objects.create(username='carlos', password='12345678', aprobado=True)
        print("modificando el item")
        self.servicio_items.modificar_item(item1, None, "nueva descripcion", None, None, None, usuario)
        print("comprobando si el estado esta en pendiente de versionado")
        self.assertEqual(item1.estado, "pv", "El estado no cambio a pendiente de versionado")
        self.servicio_items.crear_nueva_version_de_item(item1, "2.0")
        print("Comprobando si el item paso a estado pendiente")
        self.assertEqual(item1.estado, "pe", "El estado no cambio a pendiente")
        self.servicio_items.modificar_item(item1, "nuevo nombre", None, None, None, None, usuario)
        print("Cambiando nombre y deshaciendo el cambio de nombre")
        self.assertEqual(item1.nombre, "nuevo nombre", "El nombre no se modifico")
        self.servicio_items.deshacer_cambios(item1, usuario)
        self.assertEqual(item1.nombre, "item1", "El nombre no volvio al anterior")

    def test_registro_cambios(self):
        """
            Crear nuevas versiones de item, y navega a traves de las opciones disponibles
        """
        print('#####################################')
        print('##################################### Prueba de historial de cambios')
        print('#####################################')
        proyecto = Proyecto.objects.first()
        print("Creando fase de prueba")
        self.servicio_fase.agregar_fase_a_proyecto("fase", proyecto, 1, "", "")
        fase = self.servicio_proyectos.get_fases(proyecto).first()
        print("Creando usuarios de prueba")
        print("Creando tipo de item de prueba")
        self.servicio_tdi.agregar_tipo_de_item("tdi1", proyecto.id)
        tdi = self.servicio_proyectos.get_tipos_de_item(proyecto, True).first()
        print("Creando items de prueba")
        self.servicio_items.agregar_item("item1", "rf", 1, tdi, fase, "descripcion1", 1, [], '1')
        item1 = self.servicio_fase.get_items(fase).first()
        print("Creando usuario de prueba")
        usuario = Usuario.objects.create(username='carlos', password='12345678', aprobado=True)
        print("modificando el item")
        self.servicio_items.modificar_item(item1, None, "nueva descripcion", None, None, None, usuario)
        print("comprobando si el estado esta en pendiente de versionado")
        self.assertEqual(item1.estado, "pv", "El estado no cambio a pendiente de versionado")
        self.servicio_items.crear_nueva_version_de_item(item1, "2.0")
        print("Comprobando si el item paso a estado pendiente")
        self.assertEqual(item1.estado, "pe", "El estado no cambio a pendiente")
        self.servicio_items.modificar_item(item1, "nuevo nombre", None, None, None, None, usuario)
        print("Cambiando nombre y deshaciendo el cambio de nombre")
        self.assertEqual(item1.nombre, "nuevo nombre", "El nombre no se modifico")
        print("Deshaciendo cambios, deberia quedar guardado en el historial tambien")
        self.servicio_items.deshacer_cambios(item1, usuario)
        print("Obteniendo historial de cambios")
        cambios = self.servicio_items.get_historial(item1)
        self.assertEqual(3,cambios.count(), "Se encontraron "+str(cambios.count()) + " cambios y se esperaban 3")