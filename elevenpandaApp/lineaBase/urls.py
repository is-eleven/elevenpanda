from django.urls import path
from .views import crear_linea_base, aprobar_item_de_lb_rota

urlpatterns = [
    # path('<int:fase_id>/', fase_view),
    path('crear/', crear_linea_base),
    path('rota/aprobar/item/', aprobar_item_de_lb_rota),
]