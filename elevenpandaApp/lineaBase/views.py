from django.shortcuts import redirect
from django.template.defaultfilters import register
from .services import ServicioLineaBase

servicio_linea_base = ServicioLineaBase()

def crear_linea_base(request):
    """
    View de creacion de linea base
    :param request:
    :return:
    """
    items = request.POST.get('items')
    fase = request.POST.get('fase')
    usuario = request.POST.get('creador')
    servicio_linea_base.crear_linea_base(items, fase, usuario)
    href = request.POST.get('href')
    return redirect(href)

def aprobar_item_de_lb_rota(request):
    """
    View de aprobacion de items de lineas bases rotas
    :param request:
    :return:
    """
    item = request.POST.get('item')
    servicio_linea_base.aprobar_item_de_lb_rota(item)
    href = request.POST.get('href')
    return redirect(href)


@register.filter
def items_lb(lb):
    return servicio_linea_base.get_items_de_linea_base(lb)

@register.filter
def item_tiene_lb(item):
    return servicio_linea_base.item_tiene_linea_base(item)

@register.filter
def item_lb_nombre(item):
    return servicio_linea_base.get_linea_base_de_item(item).nombre

@register.filter
def esta_romper_solicitado(lb):
    return servicio_linea_base.esta_romper_solicitado(lb)

@register.filter
def romper_lb_notificaciones(usuario):
    return servicio_linea_base.get_solicitudes_de_romper_lb_a_usuario(usuario)

@register.filter
def voto_en_solicitud(solicitud_id, usuario_id):
    return servicio_linea_base.get_voto_en_romper_lb_solicitud(solicitud_id, usuario_id).voto

@register.filter
def es_lb_rota(lb):
    return servicio_linea_base.get_lb_estado(lb) == 'ro'
