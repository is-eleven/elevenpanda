# coding=utf-8
from proyecto.services import ServicioFases, ServicioItems, ServicioProyectos
from Usuarios.services import ServicioUsuario
from .models import LineaBase, DetalleLbItems, LineaBaseRomperSolicitudes, RomperLineaBaseVotos
import math

servicio_fases = ServicioFases()
servicio_usuario = ServicioUsuario()
servicio_item = ServicioItems()
servicio_proyectos = ServicioProyectos()



class ServicioLineaBase:

    def get_lb(self, lb_ref):
        """
        Obtiene el objeto línea base dada una referencia la misma

        :param lb_ref: referencia la línea base
        :return: objeto en sí
        """
        if isinstance(lb_ref, str) and lb_ref.isdigit():
            return LineaBase.objects.filter(id=int(lb_ref)).first()
        if isinstance(lb_ref, int):
            return LineaBase.objects.filter(id=lb_ref).first()
        return lb_ref

    def crear_linea_base(self, items_id, fase_ref, user_ref):
        """
        Crea una nueva línea base.

        :param items_id: cadena de los identificadores (id) de los ítems que se agregarán a la línea base, separados por coma
        :param fase_ref: fase donde se crea la línea base
        :param user_ref: usuario que crea la linea base
        """
        fase = servicio_fases.get_fase(fase_ref)
        usuario = servicio_usuario.get_usuario(user_ref)
        # Crear linea base
        name = "LB-" + str(fase.numero_fase) + "-" + str(self.contar_lb_en_fase(fase) + 1)
        lb = LineaBase.objects.create(nombre=name, fase=fase, usuario_creador=usuario)
        lb.save()
        # Asignar items a linea base
        for item_id in items_id.split(','):
            item = servicio_item.get_item(item_id)
            DetalleLbItems.objects.create(linea_base=lb, item=item)

    def aprobar_item_de_lb_rota(self, item_ref):
        """
        Funcion para aprobar modificaciones de items cuyas lineas bases se rompieron.

        :param item_ref: referencia al item en cuestion
        """
        item = servicio_item.get_item(item_ref)
        lb = self.get_linea_base_de_item(item)
        # aprobar item
        item.estado = 'ap'
        item.save()
        # verificar linea base
        items_lb = self.get_items_de_linea_base(lb)
        todos_aprobados = True
        for item_lb in items_lb:
            if item_lb.estado != 'ap':
                todos_aprobados = False
                break
        if todos_aprobados:
            # restaurar linea base
            lb.estado = 'ac'
            lb.save()

    def get_items_de_linea_base(self, lb_ref):
        """
        Obtiene todos los items de una linea base.

        :param lb_ref: referencia a la linea base en cuestion
        :return: Una lista de items, en caso de que la linea base no tenga items relacionados, una lista vacia
        """
        lb = self.get_lb(lb_ref)
        return [detalle.item for detalle in DetalleLbItems.objects.filter(linea_base=lb)]

    def get_linea_base_de_item(self, item_ref):
        """
        Obtiene la linea base de un item.
        :param item_ref: Referencia al item en cuestion
        :return: Un objeto linea base, o en caso de que el item no tenga linea base, None
        """
        item = servicio_item.get_item(item_ref)
        result = [detalle.linea_base for detalle in DetalleLbItems.objects.filter(item=item)]
        return result[0] if len(result) > 0 else None

    def get_lineas_base_de_fase(self, fase):
        """
        Funcion para obtener todas las lineas bases de una fase.

        :param fase: fase de donde se obtendrán las lineas base
        :return: lista de líneas base de la fase dada
        """
        return LineaBase.objects.filter(fase=fase)

    def item_tiene_linea_base(self, item_ref):
        """
        Funcion para comprobar si un item dado esta relacionado a una linea base.

        :param item_ref: Referencia al item en cuestion
        :return: True si el item esta en una fase, False en caso contrario
        """
        item = servicio_item.get_item(item_ref)
        return DetalleLbItems.objects.filter(item=item).count() > 0

    def contar_lb_en_fase(self, fase_ref):
        """
        Funcion para obtener la cantidad de lineas base de una fase especifica.

        :param fase_ref: Referencia a la fase en cuestion
        :return: La cantidad de lineas base de la fase
        """
        fase = servicio_fases.get_fase(fase_ref)
        return LineaBase.objects.filter(fase=fase).count()

    def solicitar_romper(self, linea_base_ref, usuario_ref, motivo):
        """
        Funcion para abrir una solicitud de ruptura de linea base.

        :param linea_base_ref: Referencia a la linea base a romper
        :param usuario_ref: Usuario solicitante de la ruptura
        :param motivo: Una explicacion breve sobre la ruptura
        """
        linea_base = self.get_lb(linea_base_ref)
        usuario = servicio_usuario.get_usuario(usuario_ref)
        LineaBaseRomperSolicitudes.objects.create(linea_base=linea_base, usuario_solicitante=usuario, motivo=motivo)

    def esta_romper_solicitado(self, linea_base_ref):
        """
        Funcion que checkea si una linea base esta en proceso de ruptura.

        :param linea_base_ref: Referencia a la linea base en cuestion
        :return: True si existe una solicitud de ruptura no resuelta sobre la linea base, False en caso contrario
        """
        linea_base = self.get_lb(linea_base_ref)
        try:
            solicitud = LineaBaseRomperSolicitudes.objects.filter(linea_base=linea_base).order_by('fecha_solicitud').last()
            return solicitud is not None and not solicitud.resuelto
        except:
            return False

    def get_solicitudes_de_romper_lb_a_usuario(self, usuario_ref):
        """
        Funcion que notifica a un usuario del comite de gestion de cambios sobre una solicitud de ruptura.

        :param usuario_ref: Referencia al usuario en cuestion
        :return: lista de notificaciones de un usuario
        """
        usuario = servicio_usuario.get_usuario(usuario_ref)
        proyectos = servicio_proyectos.get_proyectos_usuario_es_miembro_comite(usuario)
        nots = []
        for proy in proyectos:
            nots.extend(LineaBaseRomperSolicitudes.objects.filter(linea_base__fase__proyecto=proy, resuelto=False))
        return nots

    def votar_romper_linea_base(self, usuario_ref, solicitud_id, voto):
        """
        Funcion para la votacion sobre la ruptura de una linea base.

        :param usuario_ref: Usuario que votara
        :param solicitud_id: Id de la solicitud
        :param voto: El voto del usuario
        """
        usuario = servicio_usuario.get_usuario(usuario_ref)
        RomperLineaBaseVotos.objects.create(usuario=usuario, solicitud_id=solicitud_id, voto=voto == 'y')
        self.verificar_romper_linea_base_votos(solicitud_id)

    def verificar_romper_linea_base_votos(self, solicitud_id):
        """
        Procesa las votos de una solicitud de ruptura de linea base y las resuelve segun mayoria simple.

        :param solicitud_id: Id de la solicitud
        """
        votos_total = self.get_votos_de_solicitud(solicitud_id).count()
        solicitud = LineaBaseRomperSolicitudes.objects.get(id=solicitud_id)
        miembros_comite = servicio_proyectos.get_miembros_del_comite(solicitud.linea_base.fase.proyecto)
        votos_minimo = min(2, math.floor(len(miembros_comite) / 2) + 1)
        if votos_total >= votos_minimo:
            votos_favor = self.get_votos_de_solicitud_a_favor(solicitud_id).count()
            votos_contra = votos_total - votos_favor
            if votos_favor >= votos_minimo:
                # resolver
                solicitud.resuelto = True
                solicitud.save()
                # romper línea base
                self.romper_lb(solicitud.linea_base)
            elif votos_contra >= votos_minimo:
                # resolver
                solicitud.resuelto = True
                solicitud.save()

    def get_voto_en_romper_lb_solicitud(self, solicitud_id, usuario_ref):
        """
        Funcion para guardar los votos de un usuario en una solicitud.

        :param solicitud_id: Id de la solicitud
        :param usuario_ref: Usuario votante
        :return: Retorna un objeto de votacion.
        """
        usuario = servicio_usuario.get_usuario(usuario_ref)
        return RomperLineaBaseVotos.objects.get(usuario=usuario, solicitud_id=solicitud_id)

    def get_votos_de_solicitud(self, solicitud_id):
        """
        Obtiene los votos de una solicitud.

        :param solicitud_id: Id de la solicitud
        :return: Retorna una lista de objetos de votacion, en caso de que no existan votos, una lista vacia.
        """
        solicitud = LineaBaseRomperSolicitudes.objects.get(id=solicitud_id)
        miembros = servicio_proyectos.get_miembros_del_comite(solicitud.linea_base.fase.proyecto)
        miembros_id = [miembro.id for miembro in miembros if miembro.is_active]
        return RomperLineaBaseVotos.objects.filter(solicitud=solicitud, usuario_id__in=miembros_id)

    def get_votos_de_solicitud_a_favor(self, solicitud_id):
        """
        Obtiene los votos a favor de la ruptura de una solicitud.

        :param solicitud_id: Id de la solicitud
        :return: Retorna una lista de objetos de votacion, en caso no existir votos a favor, una lista vacia
        """
        return RomperLineaBaseVotos.objects.filter(solicitud_id=solicitud_id, voto=True)

    def get_lb_estado(self, lb_ref):
        """
        Obtiene el estado de una linea base.

        :param lb_ref: Referencia a la linea base en cuestion
        :return: El estado de linea base
        """
        return self.get_lb(lb_ref).estado

    def romper_lb(self, lb_ref):
        """
        Funcion para romper una linea base.

        :param lb_ref: Referencia a la linea base en cuestion.
        """
        lb = self.get_lb(lb_ref)
        # Cambiar estado de ítems
        items = self.get_items_de_linea_base(lb)
        for item in items:
            item.estado = 'er'
            item.save()
            self.romper_lb_rec(item)
        # Cambiar estado de lb
        lb.estado = "ro"
        lb.save()

    def romper_lb_rec(self, item):
        """
        Función que cambia los estados de los ítems de una línea base una vez que se rompe.

        :param item: Ítem en recursión
        """
        for hijo in servicio_item.get_relaciones(item, 'h'):
            if not hijo.item.estado == 'er' and not hijo.item.estado == 'co':
                hijo.item.estado = 'co'
                hijo.item.save()
                self.romper_lb_rec(hijo.item)
        for sucesor in servicio_item.get_relaciones(item, 's'):
            if not sucesor.item_relacion.estado == 'er' and not sucesor.item_relacion.estado == 'co':
                sucesor.item_relacion.estado = 'co'
                sucesor.item_relacion.save()
                self.romper_lb_rec(sucesor.item_relacion)

    def item_puede_agregarse_a_lb(self, item_ref):
        """
        Funcion que valida si un item puede agregarse a una linea base.

        :param item_ref: Referencia al item en cuestion
        :return: True si el objeto cumple las condiciones para unirse a una linea base, False en caso contrario
        """
        item = servicio_item.get_item(item_ref)
        # Si no está aprobado a si ya está en una lb, no puede agregarse a una lb
        if item.estado != 'ap' or self.item_tiene_linea_base(item):
            return False
        # Si no es un ítem de la última fase
        """
        if item.fase.numero_fase < servicio_proyectos.get_fases(item.fase.proyecto).count():
            # Si no tiene sucesores ni hijos, no puede agregarse a una lb
            if servicio_item.get_relaciones(item,'s').count() == 0 and servicio_item.get_relaciones(item,'h').count() == 0:
                return False
        """
        # Si no es un ítem de la primer fase
        if item.fase.numero_fase > 1:
            # Si no tiene antecesores ni padres
            if servicio_item.get_relaciones(item,'a').count() == 0 and servicio_item.get_relaciones(item,'p').count() == 0:
                return False
        return True

    def get_items_en_lb_en_fase(self, fase_ref):
        """
        Funcion para listar todos los items que estan en linea base de una fase en especifico.

        :param fase_ref: Referencia a lase en cuestion
        :return: Retorna una lista de items, si no existen  items en lineas base, retorna una lista vacia
        """
        fase = servicio_fases.get_fase(fase_ref)
        lbs = LineaBase.objects.filter(fase=fase)
        lbs_id = [lb.id for lb in lbs]
        return [i.item for i in DetalleLbItems.objects.filter(linea_base_id__in=lbs_id)]
