# Generated by Django 3.0.4 on 2020-09-13 23:34

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('lineaBase', '0004_lineabase_romper_solicitado'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='lineabase',
            name='romper_solicitado',
        ),
        migrations.CreateModel(
            name='LineaBaseRomperSolicitudes',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('motivo', models.TextField()),
                ('fecha_solicitud', models.DateTimeField(auto_now_add=True)),
                ('linea_base', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='lineaBase.LineaBase')),
                ('usuario_solicitante', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
