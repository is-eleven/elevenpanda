# Generated by Django 3.0.4 on 2020-09-14 00:07

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('lineaBase', '0005_auto_20200913_1934'),
    ]

    operations = [
        migrations.CreateModel(
            name='RomperLineaBaseVotos',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('voto', models.BooleanField(blank=True, default=None, null=True)),
                ('solicitud', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='lineaBase.LineaBaseRomperSolicitudes')),
                ('usuario', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
