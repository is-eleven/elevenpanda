# -*- coding: utf-8 -*-
import os

# CREAR DIRECTORIOS
os.mkdir('_build')
os.mkdir('_static')
os.mkdir('_templates')

# CREAR ARCHIVO config.py

conf = open("conf.py", "w+")
conf.write("import os\n"
           "import sys\n"
           "import django\n"
           "sys.path.insert(0, os.path.abspath('..'))\n"
           "os.environ['DJANGO_SETTINGS_MODULE'] = 'elevenpandaApp.settings'\n"
           "django.setup()\n"
           "project = u'Eleven Panda'\n"
           "copyright = u'2020, luis'\n"
           "author = u'luis'\n"
           "version = u''\n"
           "release = u''\n"
           "extensions = ['sphinx.ext.autodoc', 'sphinx.ext.doctest', 'sphinx.ext.todo', 'sphinx.ext.coverage', 'sphinx.ext.imgmath']\n"
           "templates_path = ['_templates']\n"
           "source_suffix = '.rst'\n"
           "master_doc = 'index'\n"
           "language = u'es'\n"
           "exclude_patterns = [u'_build', 'Thumbs.db', '.DS_Store']\n"
           "pygments_style = None\n"
           "#html_theme = 'alabaster'\n"
           "html_theme = 'sphinx_rtd_theme'\n"
           "html_static_path = ['_static']\n"
           "htmlhelp_basename = 'ElevenPandadoc'\n"
           "latex_elements = {}\n"
           "latex_documents = [(master_doc, 'ElevenPanda.tex', u'Eleven Panda Documentation', u'luis', 'manual'),]\n"
           "man_pages = [(master_doc, 'elevenpanda', u'Eleven Panda Documentation', [author], 1)]\n"
           "texinfo_documents = [(master_doc, 'ElevenPanda', u'Eleven Panda Documentation',author, 'ElevenPanda', 'One line description of project.','Miscellaneous'),]\n"
           "epub_exclude_files = ['search.html']\n"
           "todo_include_todos = True")
conf.close()

# CREAR ARCHIVO INDEX

index = open("index.rst", "w+")
index.write("Bienvenido a la documentación de Elenpanda!\n"
            "===========================================\n\n"
            ".. toctree::\n"
            "   :maxdepth: 2\n"
            "   :caption: Contents:\n\n\n\n"
            "Indices y tablas\n"
            "==================\n\n"
            "* :ref:`genindex`\n"
            "* :ref:`modindex`\n"
            "* :ref:`search`")
index.close()

# CREAR ARCHIVO MAKEFILE

makefile = open("Makefile", "w+")
makefile.write("SPHINXOPTS    ?=\n"
               "SPHINXBUILD   ?= sphinx-build\n"
               "SOURCEDIR     = .\n"
               "BUILDDIR      = _build\n\n"
               "help:\n"
               "	@$(SPHINXBUILD) -M help '$(SOURCEDIR)' '$(BUILDDIR)' $(SPHINXOPTS) $(O)\n\n"
               ".PHONY: help Makefile\n\n"
               "%: Makefile\n"
               "	@$(SPHINXBUILD) -M $@ '$(SOURCEDIR)' '$(BUILDDIR)' $(SPHINXOPTS) $(O)")
makefile.close()

# CREAR ARCHIVO DE FASES

fases = open("elevenpandaApp.fases.rst", "w+")
fases.write("Fases\n"
            "============================\n\n"
            "----------------------------------\n\n"
            ".. automodule:: fases.models\n"
            "   :members:\n"
            "   :undoc-members:\n"
            "   :show-inheritance:\n")
fases.close()

# CREAR ARCHIVO DE LÍNEA BASE

lb = open("elevenpandaApp.lineaBase.rst", "w+")
lb.write("Linea Base\n"
         "================================\n\n"
         "Modelos de Línea Base\n"
         "--------------------------------------\n\n"
         ".. automodule:: lineaBase.models\n"
         "   :members:\n"
         "   :undoc-members:\n"
         "   :show-inheritance:\n\n"
         "Servicios de Línea Base\n"
         "----------------------------------------\n\n"
         ".. automodule:: lineaBase.services\n"
         "   :members:\n"
         "   :undoc-members:\n"
         "   :show-inheritance:")
lb.close()

# CREAR ARCHIVO DE PROYECTO

proyecto = open("elevenpandaApp.proyecto.rst", "w+")
proyecto.write("Proyecto\n"
               "===============================\n\n"
               "Modelos de Proyecto\n"
               "-------------------------------------\n\n"
               ".. automodule:: proyecto.models\n"
               "   :members:\n"
               "   :undoc-members:\n"
               "   :show-inheritance:\n\n"
               "Servicios de Proyecto\n"
               "---------------------------------------\n\n"
               ".. automodule:: proyecto.services\n"
               "   :members:\n"
               "   :undoc-members:\n"
               "   :show-inheritance:")
proyecto.close()

# CREAR ARCHIVO DE ROLES

roles = open("elevenpandaApp.roles.rst", "w+")
roles.write("Roles\n"
            "============================\n\n"
            "Modelos de Roles\n"
            "----------------------------------\n\n"
            ".. automodule:: roles.models\n"
            "   :members:\n"
            "   :undoc-members:\n"
            "   :show-inheritance:\n\n"
            "Servicios de Roles\n"
            "------------------------------------\n\n"
            ".. automodule:: roles.services\n"
            "   :members:\n"
            "   :undoc-members:\n"
            "   :show-inheritance:")
roles.close()

# CREAR ARCHIVO DE Usuarios

usuarios = open("elevenpandaApp.Usuarios.rst", "w+")
usuarios.write("Usuarios\n"
               "===============================\n\n"
               "Modelos de Usuarios\n"
               "-------------------------------------\n\n"
               ".. automodule:: Usuarios.models\n"
               "   :members:\n"
               "   :undoc-members:\n"
               "   :show-inheritance:\n\n"
               "Servicios de Usuarios\n"
               "---------------------------------------\n\n"
               ".. automodule:: Usuarios.services\n"
               "   :members:\n"
               "   :undoc-members:\n"
               "   :show-inheritance:")
usuarios.close()


# CREAR ARCHIVO DE EVENPANDA APP

epapp = open("elevenpandaApp.rst", "w+")
epapp.write("ElevenPanda App\n"
            "===============\n\n"
            "Módulos\n"
            "-----------\n\n"
            ".. toctree::\n\n"
            "   elevenpandaApp.Usuarios\n"
            "   elevenpandaApp.fases\n"
            "   elevenpandaApp.lineaBase\n"
            "   elevenpandaApp.proyecto\n"
            "   elevenpandaApp.roles")
epapp.close()








# CREAR ARCHIVO DE ROLES

#roles = open("elevenpandaApp.roles.rst", "w+")
#roles.write()
#roles.close()
