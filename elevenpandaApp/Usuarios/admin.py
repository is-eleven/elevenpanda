from django.contrib import admin
from .models import Usuario, Contacto

admin.site.register(Usuario)
admin.site.register(Contacto)