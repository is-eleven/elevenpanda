from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect
from django.urls import path
from .views import informacion_deusuario_view, guardar_cambios_usuario, listar_usuarios, desactivar_usuario, \
    activar_usuario, aprobar_usuario, agregar_admin, editar_informacion_deusuario_view, \
    editar_contrasenia_deusuario_view, cambiar_nombre_de_usuario, cambiar_perfil_de_usuario, \
    editar_roles_deusuario_view, agregar_rol_a_usuario, remover_rol_a_usuario

from .models import Usuario


urlpatterns = [
    path('perfil/', informacion_deusuario_view, name='usuario_info'),
    path('<str:username>/editar/', editar_informacion_deusuario_view),
    path('<str:username>/editar/roles/', editar_roles_deusuario_view),
    path('rol/agregar/', agregar_rol_a_usuario),
    path('rol/remover/', remover_rol_a_usuario),
    #path('<str:username>/editar/password/', editar_contrasenia_deusuario_view),
    path('lista/', listar_usuarios, name='lista_usuarios'),
    path('desactivar/', desactivar_usuario),
    path('activar/', activar_usuario),
    path('aprobar/', aprobar_usuario),
    path('agregar/admin/', agregar_admin),
    path('<str:username>/', informacion_deusuario_view, name='usuario_info'),
    path('cambiardatos/cuenta/', cambiar_nombre_de_usuario),
    path('cambiardatos/perfil/', cambiar_perfil_de_usuario),
    #path('save/changes/', guardar_cambios_usuario),
]

"""
    path("signup/", views.signup, name="account_signup"),
    path("login/", views.login, name="account_login"),
    path("logout/", views.logout, name="account_logout"),
    path("password/change/", views.password_change,
         name="account_change_password"),
    path("password/set/", views.password_set, name="account_set_password"),
    path("inactive/", views.account_inactive, name="account_inactive"),

    # E-mail
    path("email/", views.email, name="account_email"),
    path("confirm-email/", views.email_verification_sent,
         name="account_email_verification_sent"),
    re_path(r"^confirm-email/(?P<key>[-:\w]+)/$", views.confirm_email,
            name="account_confirm_email"),

    # password reset
    path("password/reset/", views.password_reset,
         name="account_reset_password"),
    path("password/reset/done/", views.password_reset_done,
         name="account_reset_password_done"),
    re_path(r"^password/reset/key/(?P<uidb36>[0-9A-Za-z]+)-(?P<key>.+)/$",
            views.password_reset_from_key,
            name="account_reset_password_from_key"),
    path("password/reset/key/done/", views.password_reset_from_key_done,
         name="account_reset_password_from_key_done"),
"""