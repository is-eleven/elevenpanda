from django.contrib.auth.models import AbstractUser
from django.db import models

from roles.models import Rol, RolUsuario


class Usuario(AbstractUser):
    """
    Clase que implementa el modelo de Usuario, relacionado a la clase AbstractUser
    aprobado: es un atributo booleano que indica si el registro del usuario solicitado a sido o no aprobado
    cedula: numero de identidad del usuario
    fecha_nacimiento: fecha de nacimiento del usuario
    """

    aprobado = models.BooleanField(default=False)
    cedula = models.CharField(max_length=60, null=True, blank=True)
    fecha_nacimiento = models.DateField(null=True, blank=True)
    roles = models.ManyToManyField(Rol, through="roles.RolUsuario")

    def __str__(self):
        return self.username

    def save(self, *args, **kwargs):
        # Si es el primer usuario, está aprobado y es superusuario
        if Usuario.objects.count() == 0:
            self.aprobado = True
            self.is_superuser = True
        super(Usuario, self).save(*args, **kwargs)


class Contacto(models.Model):
    """

    """
    TIPO_DE_CONTACTO = [
        ('te', 'Telefono'),
        ('co', 'Correo electrónico'),
        ('ur', 'Dirección URL'),
        ('di', 'Dirección física'),
    ]
    contacto_id = models.AutoField(primary_key=True)
    descripcion = models.CharField(max_length=50)
    tipo_contacto = models.CharField(max_length=2, choices=TIPO_DE_CONTACTO)
    usuario = models.ForeignKey(Usuario, on_delete=models.CASCADE)

    def __str__(self):
        return self.tipo_contacto + ': ' + self.descripcion
