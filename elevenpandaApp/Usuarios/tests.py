from django.test import TestCase
from .models import Usuario
from .services import ServicioUsuario

class UsuariosTest(TestCase):
    servicio_usuarios = ServicioUsuario()

    def setUp(self):
        """ Creación de usuario de prueba """
        print('#####################################')
        print('##################################### Usuario de prueba')
        print('#####################################')
        print('Creando usuarios de prueba.')
        Usuario.objects.create(username='carlosruiz', password='usuario1', first_name='carlos', last_name='ruiz')
        Usuario.objects.create(username='luisluis', password='luisluis123', first_name='luis', last_name='luis')
        print('Usuarios de prueba creados.')

    def test_usuario_validado(self):
        """ Test de comprobacion de que todo usuario recien creado debe estar inicialmente no aprobado. """
        print('#####################################')
        print('##################################### Validando estado de usuario')
        print('#####################################')
        print('Verificando que el primer usuario creado tenga acceso al sistema.')
        usuario = self.servicio_usuarios.get_lista_usuarios().first()
        self.assertEqual(usuario.aprobado, True, 'El primer usuario creado no está aprobado cuando debería estarlo.')
        print('Verificando que los demás usuarios recientemente creados no tengan acceso al sistema.')
        usuario2 = self.servicio_usuarios.get_lista_usuarios().last()
        self.assertEqual(usuario2.aprobado, False, 'Un usuario recién creado está aprobado cuando no debería estarlo.')

    def test_modificar_usuario(self):
        """ Test para verificar que la información de usuario se modifica correctamente. """
        print('#####################################')
        print('##################################### Modificar usuario')
        print('#####################################')
        print('Cambiando información de usuario para verificar que el servicio para modificar datos de usuario funcione.')
        usuario = self.servicio_usuarios.get_lista_usuarios().first()
        usuario = self.servicio_usuarios.modificar_usuario(usuario_ref=usuario, name='NuevoNombre', username='NuevoNombreDeUsuario')
        self.assertEqual(usuario.username, 'NuevoNombreDeUsuario', 'No se modificó correctamente el nombre de usuario.')
        self.assertEqual(usuario.first_name, 'NuevoNombre', 'No se modificó correctamente el nombre.')

    def test_aprobar_usuario(self):
        """ Test para aprobar un usuario del sistema y verificar que el servicio correspondiente funcione. """
        print('#####################################')
        print('##################################### Modificar usuario')
        print('#####################################')
        print('Aprobando usuario.')
        usuario = self.servicio_usuarios.get_lista_usuarios().first()
        self.servicio_usuarios.aprobar_usuario(usuario)
        self.assertEqual(usuario.aprobado, True, 'No se aprobó correctamente al usuario.')

    def test_inhabilitar_usuario(self):
        """ Test para inhabilitar un usuario del sistema y verificar que el servicio correspondiente funcione. """
        print('#####################################')
        print('##################################### Modificar usuario')
        print('#####################################')
        print('Aprobando usuario.')
        usuario = self.servicio_usuarios.get_lista_usuarios().first()
        self.servicio_usuarios.inhabilitar_usuario(usuario)
        self.assertEqual(usuario.is_active, False, 'No se inhabilitó correctamente al usuario.')