from urllib.parse import urlparse
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import redirect

from Usuarios.models import Usuario


def cuenta_aprobada_requerida(function):
    """
    Esta funcion implementa un decorator personalizado que impide a usuarios no aprobados ingresar
    al sistema.
    :param function: es la funcion detallada abajo
    :return: Resultado de la funcion
    """
    def _function(request, *args, **kwargs):
        """
        Esta funcion realiza una validacion sobre una variable de usuario
        :param request: Es un query que contiene el html de una vista
        :param args: valores
        :param kwargs: claves
        :return: Retorna una redireccion Http si el usuario no esta validado
        """

        if not Usuario.objects.filter(username=request.user.username).first().aprobado:
            return HttpResponseRedirect('/accounts/checking-account/')
        return function(request, *args, **kwargs)
    return _function


def cuenta_activa_redireccion(function):
    """
    Este decorador pesonalizado redirige a un usuario hacia el home del sistema cuando anteriormente había
    estado desactivado pero actualmente ya no lo está.
    """
    def _function(request, *args, **kwargs):
        if request.user and request.user.is_active:
            return redirect('/')
        return function(request, *args, **kwargs)
    return _function
