# coding=utf-8
from abc import abstractmethod

from django.contrib.sessions.models import Session
from django.contrib.sites.models import Site
from django.utils import timezone
from pyxtension.Json import Json

from Usuarios.models import Contacto, Usuario
from allauth.socialaccount.models import SocialAccount, SocialApp


class ServicioUsuario:
    """
    Definición de métodos para interactura con los usuarios
    """
    def get_usuario(self, referencia):
        """
        Obtien un objeto usuario dado una referencia al mismo.

        :param referencia: identificación del usuario
        :return: usuario objeto
        """
        if isinstance(referencia, str) and referencia.isdigit():
            return Usuario.objects.filter(id=int(referencia)).first()
        elif isinstance(referencia, int):
            return Usuario.objects.filter(id=referencia).first()
        elif isinstance(referencia, str):
            return Usuario.objects.filter(username=referencia).first()
        return referencia

    def get_existe_usuario(self, username):
        """
        Define si un usuario existe en el sistema o no

        :param username: nombre de usuario a considerar
        :return: si el usuario dado un nombre de usuario existe
        """
        return len(Usuario.objects.filter(username=username)) > 0

    def get_lista_usuarios(self, order_by=None):
        """
        Obtiene una lista con todos los usuarios del sistema.

        :param order_by: parámetro de ordenación
        :return: lista de usuarios del sistema ordenos
        """
        if order_by == 'nombreDeUsuario':
            order_by = 'username'
        elif order_by == 'ultimoRegistrado':
            order_by = '-date_joined'
        elif order_by == 'primeroRegistrado':
            order_by = 'date_joined'
        try:
            return Usuario.objects.all().order_by(order_by)
        except:
            return Usuario.objects.all()

    def es_usuario_propio(self, usuario_de_sesion, nombre_usuario_parametro):
        """
        Define si dado dos referencias de usuarios, ambos son los mismos.

        :param usuario_de_sesion:
        :param nombre_usuario_parametro:
        :return:
        """
        _usuario_de_sesion = self.get_usuario(usuario_de_sesion).username
        return nombre_usuario_parametro is None or nombre_usuario_parametro == 'perfil' or nombre_usuario_parametro == _usuario_de_sesion

    def get_usuario_image_url(self, nombre_usuario):
        """
        Obtiene la URL de la imagen de perfil de un usuario.

        :param nombre_usuario: nombre del usuario a considerar
        :return: URL de la imagen de perfil
        """
        id_usuario = self.get_usuario(nombre_usuario).id
        picture = SocialAccount.objects.filter(user_id=id_usuario).first()
        if picture is not None:
            return Json(picture.extra_data)['picture']
        else:
            return 'https://journal-ranrupt-associations.info/wp-content/uploads/2019/06/how-do-you-draw-a-panda-bear-drawing-of-pandas-easy-pandas-to-draw-panda-face-drawing-panda-face-drawing-panda-realistic-art-hand-draw-panda-bear.jpg'

    def modificar_usuario(self, usuario_ref, username=None, name=None, lastname=None, birthday=None, document=None):
        """
        Modifica los atributos dados de un usuario.

        :param usuario_ref: usuario que será modificado
        :param username: nuevo nombre de usuario
        :param name: nuevo nombre
        :param lastname: nuevo apellido
        :param birthname: nueva fecha de nacimiento
        :param document: nuevo número de documente
        :return: el usuario modificado
        """
        usuario = self.get_usuario(usuario_ref)
        if username is not None:
            usuario.username = username
        if name is not None:
            usuario.first_name = name
        if lastname is not None:
            usuario.lastname = lastname
        if birthday is not None:
            usuario.fecha_nacimient = birthday
        if document is not None:
            usuario.cedula = document
        usuario.save()
        return usuario

    def modificar_username(self, usuario_ref, nuevo_username):
        """
        Modifica el nombre de usuario de un usuario dado.

        :param usuario_ref: usuario a modificar
        :param nuevo_username: nuevo nombre de usuario
        """
        def es_nombre_de_usuario_valido(self):
            if " " in nuevo_username:
                return False, 'El nombre de usuario no debe contener espacios.'
            if nuevo_username.isdigit():
                return False, 'El nombre de usuario no debe ser solo números.'
            if not nuevo_username.replace("_", "").isalnum():
                return False, 'El nombre de usuario debe ser alfanumérico, no puede contener simbolos a excepción de _ (guión bajo).'
            if self.get_usuario(nuevo_username) is not None:
                return False, 'El nombre de usuario ya existe.'
            return True, 'Valido'

        es_valido = es_nombre_de_usuario_valido(self)
        if es_valido[0]:
            # Es valido
            usuario = self.get_usuario(usuario_ref)
            usuario.username = nuevo_username
            usuario.save()
        return es_valido

    def modificar_perfil(self, usuario_id, nombre, apellido, documento, cumple):
        """
        Modifica los datos de perfil de un usuario dado.

        :param usuario_id: usuario que será modificado
        :param nombre: nuevo nombre
        :param apellido: nuevo apellido
        :param documento: nuevo número de documente
        :param cumple: nueva fecha de nacimiento
        """
        usuario = self.get_usuario(usuario_id)
        if usuario.first_name != nombre:
            usuario.first_name = nombre
        if usuario.last_name != apellido:
            usuario.last_name = apellido
        if usuario.cedula != documento:
            usuario.cedula = documento
        if usuario.fecha_nacimiento != cumple and cumple:
            usuario.fecha_nacimiento = cumple
        usuario.save()

    def inhabilitar_usuario(self, usuario_id):
        """
        Cambia el estado de un usuario a deshabilitado.

        :param usuario_id: el usuario que será inhabilitado
        :return: el usuario modificado
        """
        usuario = self.get_usuario(usuario_id)
        if usuario and usuario.is_active:
            usuario.is_active = False
            usuario.save()
            return usuario
        return None

    def habilitar_usuario(self, usuario_id):
        """
        Cambia el estado de un usuario dado a habilitado.

        :param usuario_id: usuario a habilitar
        :return: el usuario habilitado
        """
        usuario = self.get_usuario(usuario_id)
        if usuario and not usuario.is_active:
            usuario.is_active = True
            usuario.save()
            return usuario
        return None

    def aprobar_usuario(self, usuario_referencia):
        """
        Aprueba el registro de un usuario

        :param usuario_referencia: usuario a aprobar
        :return: el usuario aprobado
        """
        usuario = self.get_usuario(usuario_referencia)
        if not usuario.aprobado:
            usuario.aprobado = True
            usuario.save()
            return usuario
        return None

    def hacer_admin(self, usuario_ref):
        """
        Hace que un usuario dado sea administrador del sistema.

        :param usuario_ref: usuario a convertir en administrador
        """
        usuario = self.get_usuario(usuario_ref)
        usuario.is_superuser = True
        usuario.save()

    def esta_aprobado(self, usuario_id):
        """
        Verifica si el estado de aprobación de un usuario dado.

        :param usuario_id: usuario a verificar
        :return: si el usuario está aprobado o no
        """
        return self.get_usuario(usuario_id).aprobado

    def get_usuarios_activos(self, order_by=None):
        """
        Obtiene una lista con todos los usuario aprobados del sistema.

        :param order_by: ordenar por atributo
        :return: lista de usuarios ordenados
        """
        if order_by is not None:
            if order_by == 'nombreDeUsuario':
                order_by = 'username'
            elif order_by == 'ultimoRegistrado':
                order_by = '-date_joined'
            elif order_by == 'primeroRegistrado':
                order_by = 'date_joined'
            return Usuario.objects.filter(aprobado=True, is_active=True).order_by(order_by).all()
        return Usuario.objects.filter(aprobado=True, is_active=True).all()

    def usuarios_id_perfil_imagen(self):
        """
        Obtiene una lista de las URLs de las ímagens de perfil de los usuarios.

        :return: lista de URLs
        """
        usuarios = self.get_lista_usuarios()
        dic_usimg = {}
        for usuario in usuarios:
            dic_usimg[usuario.id] = self.get_usuario_image_url(usuario.id)
        return dic_usimg

    def get_usuarios_logueados(self):
        """
        Obtiene una lista de los usuario que tiene sesión iniciada.

        :return: lista de usuarios.
        """
        # Query all non-expired sessions
        # use timezone.now() instead of datetime.now() in latest versions of Django
        sessions = Session.objects.filter(expire_date__gte=timezone.now())
        uid_list = []

        # Build a list of user ids from that query
        for session in sessions:
            data = session.get_decoded()
            uid_list.append(data.get('_auth_user_id', None))

        # Query all logged in users based on id list
        return Usuario.objects.filter(id__in=uid_list)

    def get_esta_logueado(self, nombre_usuario):
        """
        Define si un usuario tiene la sensión iniciada.

        :param nombre_usuario: nombre de usuario a verificar
        :return: si el usuario está logeado
        """
        usuario = self.get_usuario(nombre_usuario)
        return usuario in self.get_usuarios_logueados()

    def get_proyectos_de_usuario(self, usuario_ref, solo_donde_activo=True):
        """
        Obtiene una lista con todos los proyectos donde el usuario dado está asignado como miembro.

        :param usuario_ref: usuario a considerar
        :param solo_donde_activo: filtrar para mostrar solo donde esté activo
        :return: lista de proyectos del usuario
        """
        from proyecto.services import ServicioProyectos
        servicio_proyectos = ServicioProyectos()
        todos_los_proyectos = servicio_proyectos.get_proyectos()
        proyectos_de_usuario = []
        for proyecto in todos_los_proyectos:
            if servicio_proyectos.tiene_miembro(proyecto, usuario_ref):
                if servicio_proyectos.es_miembro_habilitado(proyecto, usuario_ref) or not solo_donde_activo:
                    proyectos_de_usuario.append(proyecto)
        proyectos_de_usuario_id = [rec.id for rec in proyectos_de_usuario]
        return servicio_proyectos.get_proyectos().filter(id__in=proyectos_de_usuario_id)

    def get_proyectos_de_usuario_como_gerente(self, usuario_ref):
        """
        Obtiene una lista de proyectos de un usuario dado en los cuales es gerente.

        :param : Usuario a considerar
        :return: Lista de proyectos donde es gerente
        """
        from proyecto.services import ServicioProyectos
        servicio_proyectos = ServicioProyectos()
        todos_los_proyectos = servicio_proyectos.get_proyectos()
        proyectos_de_usuario = []
        usuario = self.get_usuario(usuario_ref)
        for proyecto in todos_los_proyectos:
            gerente_proyecto = servicio_proyectos.get_gerente(proyecto)
            if gerente_proyecto is not None and gerente_proyecto.id == usuario.id:
                proyectos_de_usuario.append(proyecto)
        proyectos_de_usuario_id = [rec.id for rec in proyectos_de_usuario]
        return servicio_proyectos.get_proyectos().filter(id__in=proyectos_de_usuario_id)


class ServicioContacto:
    def get_contacto(self, contacto):
        if isinstance(contacto, int):
            return Contacto.objects.filter(contacto_id=contacto).first()
        return contacto

    def get_lista_contactos(self, usuario_id, tipo=''):
        tipo_num = 0 if tipo == 'te' else 1 if tipo == 'co' else 2 if tipo == 'ur' else 3 if tipo == 'di' else -1
        if tipo_num == -1:
            return Contacto.objects.filter(usuario_id=usuario_id)
        else:
            return self.get_lista_contactos(usuario_id).filter(tipo_contacto=Contacto.TIPO_DE_CONTACTO[tipo_num][0])

    def agregar_contacto(self, usuario_id, tipo, desc):
        if Usuario.objects.filter(id=usuario_id).first() is not None:
            contacto = Contacto.objects.create(tipo_contacto=tipo, descripcion=desc, usuario_id=usuario_id)
            contacto.save()
            return contacto

    def modificar_contacto(self, contacto_id, desc):
        contacto = self.get_contacto(contacto_id)
        if contacto:
            contacto.descripcion = desc
            contacto.save()
            return contacto
        return None

    def eliminar_contacto(self, contacto_id):
        contacto = self.get_contacto(contacto_id)
        if contacto:
            contacto.delete()
            return contacto
