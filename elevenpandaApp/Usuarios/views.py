# coding=utf-8
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.template.defaulttags import register

from roles.decorators import permiso_ver_perfil_requerido, permiso_requerido
from .decorators import cuenta_aprobada_requerida
from .services import ServicioUsuario, ServicioContacto
from roles.services import ServicioUsuarioRolSistema, ServicioRoles
from proyecto.services import ServicioProyectos

servicio_usuario = ServicioUsuario()
servicio_contacto = ServicioContacto()
servicio_usuario_rol_sistema = ServicioUsuarioRolSistema()
servicio_roles = ServicioRoles()
servicio_proyectos = ServicioProyectos()

def agregar_parametros_a_url(url, params):
    import urllib.parse as urlparse
    from urllib.parse import urlencode
    url_parts = list(urlparse.urlparse(url))
    query = dict(urlparse.parse_qsl(url_parts[4]))
    query.update(params)
    url_parts[4] = urlencode(query)
    return urlparse.urlunparse(url_parts)

@login_required
@cuenta_aprobada_requerida
@permiso_ver_perfil_requerido
def informacion_deusuario_view(request, username=None):
    """
    Vista con la información de un usuario
    :param request:
    :param username:
    :return:
    """
    # Obtiene el usuario del cual se mostrará el perfil
    if servicio_usuario.es_usuario_propio(request.user, username):
        usuario_perfil = request.user
    else:
        usuario_perfil = servicio_usuario.get_usuario(username)

    # Si la página es visible y el usuario correcto...
    if usuario_perfil is not None:
        #
        # Definir modificabilidad
        #
        # Puede modificar todos los perfiles
        modificable = servicio_usuario_rol_sistema.usuario_tiene_permiso(request.user.username, 'modificar_todos_los_perfiles')
        if not modificable and request.user.username == usuario_perfil.username:
            # Puede modificar su propio perfil
            modificable = servicio_usuario_rol_sistema.usuario_tiene_permiso(request.user.username, 'modificar_perfil_propio')
        #
        # Cargar datos de contexto
        #
        base_context = get_contexto_basico(usuario_perfil)
        extra_context = {
            "modificable": modificable or request.user.is_superuser,
            'sistemaRoles': servicio_roles.roles_a_diccionario(servicio_roles.get_roles()),
            'usuarioRoles': servicio_roles.roles_a_diccionario(servicio_usuario_rol_sistema.get_roles_usuario(usuario_perfil)),
            'numeroscelular': servicio_contacto.get_lista_contactos(usuario_perfil.id, 'te'),
            'correos': servicio_contacto.get_lista_contactos(usuario_perfil.id, 'co'),
            'urls': servicio_contacto.get_lista_contactos(usuario_perfil.id, 'ur'),
            'direcciones': servicio_contacto.get_lista_contactos(usuario_perfil.id, 'di'),
            'proyetosLista': servicio_usuario.get_proyectos_de_usuario(usuario_perfil),
            'usuarioMod': usuario_perfil,
        }
        # return HttpResponse(str(username) + ' - ' + ' - ' + str(request.user.username) + ' - ' +str(base_context))
        return render(request, 'usuario/menu_usuario.html', {**base_context, **extra_context})
    # Nombre de usuario no existe o no tiene permiso para ver
    return HttpResponse("Esta página no existe o no tienes los permisos para verla.")

@login_required
@cuenta_aprobada_requerida
def editar_informacion_deusuario_view(request, username):
    usuario_perfil = servicio_usuario.get_usuario(username)
    #
    # Definir modificabilidad
    #
    # Puede modificar todos los perfiles
    modificable = servicio_usuario_rol_sistema.usuario_tiene_permiso(request.user.username, 'modificar_todos_los_perfiles')
    if not modificable and request.user.username == username:
        # Puede modificar su propio perfil
        modificable = servicio_usuario_rol_sistema.usuario_tiene_permiso(request.user.username, 'modificar_perfil_propio')
    if modificable:
        #
        # Datos del usuario
        #
        roles = servicio_usuario_rol_sistema.get_roles_usuario(usuario_perfil)
        context = {
            "modificable": modificable or request.user.is_superuser,
            'sistemaRoles': servicio_roles.roles_a_diccionario(servicio_roles.get_roles()),
            'usuarioRoles': roles,
            'rolesFaltantes': servicio_roles.get_roles_faltantes_en_ambiente(roles),
            'usuarioMod': usuario_perfil,
            'showLateral': True,
            'editandoUsuario': True,
            'editandoPerfil': True,
            'directorios': [
                {'nombre': usuario_perfil.username, 'href': '/usuario/' + usuario_perfil.username + '/'},
                {'nombre': 'Editar usuario', 'href': '/usuario/' + usuario_perfil.username + '/editar/'},
                {'nombre': 'Perfil', 'href': '/usuario/' + usuario_perfil.username + '/editar/'}]
        }
        return render(request, 'usuario/usuarioPerfilEditar.html', context)
    # Nombre de usuario no existe o no tiene permiso para ver
    return HttpResponse("Esta página no existe o no tienes los permisos para verla.")

@login_required
@cuenta_aprobada_requerida
@permiso_requerido('modificar_roles_de_usuarios')
def editar_roles_deusuario_view(request, username):
    usuario_perfil = servicio_usuario.get_usuario(username)
    #
    # Definir modificabilidad
    #
    # Puede modificar todos los perfiles
    modificable = servicio_usuario_rol_sistema.usuario_tiene_permiso(request.user.username,
                                                                     'modificar_todos_los_perfiles')
    if not modificable and request.user.username == username:
        # Puede modificar su propio perfil
        modificable = servicio_usuario_rol_sistema.usuario_tiene_permiso(request.user.username,
                                                                         'modificar_perfil_propio')
    if modificable:
        #
        # Datos del usuario
        #
        roles = servicio_usuario_rol_sistema.get_roles_usuario(usuario_perfil)
        #### mostrar todos? o solo con permisos?
        mostar_todos = request.GET.get('todos') == 'True'
        roles_no_vacios = servicio_usuario_rol_sistema.get_roles_de_usuario_no_vacios(username)
        if mostar_todos:
            lista_roles = servicio_usuario_rol_sistema.get_roles_usuario(username)
        else:
            lista_roles = roles_no_vacios
        #### Filtrar
        lista_roles = servicio_roles.filtrar_lista_de_roles(lista_roles, request.GET.get('filtro'), mostar_todos)
        #### Ordenar por
        lista_roles = servicio_roles.ordenar_lista_de_roles_por(lista_roles, request.GET.get('ordenarPor'))
        context = {
            "modificable": modificable or request.user.is_superuser,
            #'sistemaRoles': servicio_roles.roles_a_diccionario(servicio_roles.get_roles()),
            'lista_roles': lista_roles,
            'cantidad_roles_no_vacios': len(roles_no_vacios),
            'cantidad_roles_totales': len(roles),
            'rolesFaltantes': servicio_roles.get_roles_faltantes_en_ambiente(roles),
            'usuarioMod': usuario_perfil,
            'showLateral': True,
            'editandoUsuario': True,
            'editandoRoles': True,
            'directorios': [
                {'nombre': usuario_perfil.username, 'href': '/usuario/' + usuario_perfil.username + '/'},
                {'nombre': 'Editar usuario', 'href': '/usuario/' + usuario_perfil.username + '/editar/'},
                {'nombre': 'Roles', 'href': '/usuario/' + usuario_perfil.username + '/editar/roles/'}]
        }
        return render(request, 'usuario/usuarioPerfilEditarPermisos.html', context)
    # Nombre de usuario no existe o no tiene permiso para ver
    return HttpResponse("Esta página no existe o no tienes los permisos para verla.")

@login_required
@cuenta_aprobada_requerida
def editar_contrasenia_deusuario_view(request, username):
    usuario_perfil = servicio_usuario.get_usuario(username)
    #
    # Definir modificabilidad
    #
    # Puede modificar todos los perfiles
    modificable = servicio_usuario_rol_sistema.usuario_tiene_permiso(request.user.username, 'modificar_todos_los_perfiles')
    if not modificable and request.user.username == username:
        # Puede modificar su propio perfil
        modificable = servicio_usuario_rol_sistema.usuario_tiene_permiso(request.user.username, 'modificar_perfil_propio')
    if modificable:
        #
        # Datos del usuario
        #
        roles = servicio_usuario_rol_sistema.get_roles_usuario(usuario_perfil)
        context = {
            "modificable": modificable or request.user.is_superuser,
            'sistemaRoles': servicio_roles.roles_a_diccionario(servicio_roles.get_roles()),
            'usuarioRoles': roles,
            'rolesFaltantes': servicio_roles.get_roles_faltantes_en_ambiente(roles),
            'usuarioMod': usuario_perfil,
            'showLateral': True,
            'editandoUsuario': True,
            'editandoContrasenia': True,
            'directorios': [
                {'nombre': usuario_perfil.username, 'href': '/usuario/' + usuario_perfil.username + '/'},
                {'nombre': 'Editar usuario', 'href': '/usuario/' + usuario_perfil.username + '/editar/'},
                {'nombre': 'Contraseña', 'href': '/usuario/' + usuario_perfil.username + '/editar/password/'}]
        }
        return render(request, 'usuario/usuarioPerfilEditarContrasenia.html', context)
    # Nombre de usuario no existe o no tiene permiso para ver
    return HttpResponse("Esta página no existe o no tienes los permisos para verla.")

@login_required
@cuenta_aprobada_requerida
def guardar_cambios_usuario(request):
    """
    Guarda los datos mofidicados de la información de un usuario.

    :param request:
    :return:
    """
    if request.method != 'POST':
        return HttpResponse('Método incorrecto.')
    changestate = 'success'
    usuario = servicio_usuario.get_usuario(request.POST['originalUsername'])
    # Modificación de nombre de usuario
    if not servicio_usuario.get_existe_usuario(request.POST['username']):
        usuario.username = request.POST['username']
    elif usuario.username != request.POST['username']:
        changestate = 'user_already_exist'
    # Modificación de datos personales
    usuario.first_name = request.POST['name']
    usuario.last_name = request.POST['lastname']
    if request.POST['birthday'] != '':
        usuario.fecha_nacimiento = request.POST['birthday']
    usuario.cedula = request.POST['document']
    usuario.save()

    # Eliminación de contactos
    for c in request.POST['contactsToRemove'].split(','):
        if c.isnumeric():
            servicio_contacto.eliminar_contacto(int(c))

    # Modificación de contactos
    prefix = 'contact'
    for key, contato in request.POST.items():
        if key.startswith(prefix):
            str_id = key[len(prefix):]
            if str_id.isnumeric():  # es un id de contacto
                num_id = int(str_id)
                db_contacto = servicio_contacto.get_contacto(num_id)
                if db_contacto is not None and db_contacto.descripcion != contato:
                    servicio_contacto.modificar_contacto(num_id, contato)

    # Agregación de contactos
    added_contacts = request.POST['addedContacts'].split(',')
    for i in range(0, len(added_contacts), 2):
        if added_contacts[i] and added_contacts[i].strip():
            servicio_contacto.agregar_contacto(usuario.id, added_contacts[i + 1], added_contacts[i])  # Tipo, descripción

    # Agregación de roles
    roles_agregados = request.POST['addedRoles'].split('<!->')
    for rol_agregaro in roles_agregados:
        if rol_agregaro and rol_agregaro.strip():
            servicio_usuario_rol_sistema.agregar_rol_de_sistema(usuario, rol_agregaro)

    # Eliminación de roles
    roles_eliminados = request.POST['removedRoles'].split('<!->')
    for rol_eliminado in roles_eliminados:
        if rol_eliminado and rol_eliminado.strip():
            servicio_usuario_rol_sistema.remover_rol_de_sistema(usuario, rol_eliminado)

    # value = ''
    # for k, v in request.POST.items():
    #   value += '(' + k + ', ' + v + '), '
    # return informacion_deusuario_view(request, usuario.username)
    #return HttpResponse(value)
    return redirect('/usuario/' + usuario.username + '?changestate=' + changestate)

@login_required
@cuenta_aprobada_requerida
@permiso_requerido('ver_lista_usuarios')
def listar_usuarios(request):
    """
    Vista con la información para listar los usuarios del sistema.

    :param request:
    :return:
    """
    usuario = servicio_usuario.get_usuario(referencia=request.user.username)
    visible = servicio_usuario_rol_sistema.usuario_tiene_permiso(request.user.id, 'ver_lista_usuarios')  # PERMISO PARA VER
    mostrar_todos = request.GET.get('todos') == 'True'

    #### Ordenar
    ordenar_por = request.GET.get('ordenarPor')
    todos_los_usuarios = servicio_usuario.get_lista_usuarios(ordenar_por)
    usuarios_activos = servicio_usuario.get_usuarios_activos(ordenar_por)
    lista_usuarios = todos_los_usuarios if mostrar_todos else usuarios_activos

    #### filtros
    filtro = request.GET.get('filtro')
    if filtro is not None:
        if filtro == 'pendientes':
            lista_usuarios = lista_usuarios.filter(aprobado=False)
        elif filtro == 'inactivos':
            lista_usuarios = lista_usuarios.filter(is_active=False)
        elif filtro == 'administradores':
            lista_usuarios = lista_usuarios.filter(is_superuser=True)

    if request.user.is_superuser:
        visible = True

    if usuario is not None and visible:
        base_context = get_contexto_basico(usuario)
        extra_context = {
            # "modificable": modificable,  # TODO: Cambiar por permisos
            'ordenarPor': ordenar_por,
            'listaUsuarios': lista_usuarios,
            'cantidadUsuariosActivos': len(usuarios_activos),
            'cantidadUsuariosTotales': len(todos_los_usuarios),
            # 'usuariosImagenes': servicioUsuario.usuarios_id_perfil_imagen(),
            # 'usuariosLogueados': servicioUsuario.get_usuarios_logueados()
        }
        return render(request, 'usuario/listaUsuarios.html', {**base_context, **extra_context})
    return HttpResponse("Error al ingresar a esta página: Esta página no existe no o tiene permiso para verla.")


def agregar_rol_a_usuario(request):
    """
    :param request:
    :return:
    """
    usuario_id = request.POST.get('usuarioId')
    rol_id = request.POST.get('rolId')
    href = request.POST.get('href')
    servicio_usuario_rol_sistema.agregar_rol_de_sistema(usuario_id, rol_id)
    return redirect(href)

def remover_rol_a_usuario(request):
    usuario_id = request.POST.get('usuario')
    rol_id = request.POST.get('rol')
    href = request.POST.get('href')
    servicio_usuario_rol_sistema.remover_rol_de_ambiente(usuario_id, rol_id)
    return redirect(href)

def cambiar_nombre_de_usuario(request):
    nuevo_username = request.POST.get('username')
    usuario_id = request.POST.get('usuarioId')
    href = request.POST.get('href')
    resultado = servicio_usuario.modificar_username(usuario_id, nuevo_username)
    if resultado[0]:
        return redirect('/usuario/' + nuevo_username + '/editar/')
    else:
        return redirect(agregar_parametros_a_url(href, {'error': resultado[1]}))

def cambiar_perfil_de_usuario(request):
    nombre = request.POST.get('nombre')
    apellido = request.POST.get('apellido')
    documento = request.POST.get('documento')
    cumple = request.POST.get('cumple')
    usuario_id = request.POST.get('usuarioId')
    href = request.POST.get('href')
    servicio_usuario.modificar_perfil(usuario_id, nombre, apellido, documento, cumple)
    return redirect(href)

def aprobar_usuario(request):
    if request.method == 'POST':
        servicio_usuario.aprobar_usuario(request.POST.get('usuario'))
        return redirect(request.POST.get('href'))

def agregar_admin(request):
    usuario_ref = request.POST.get('usuario')
    href = request.POST.get('href')
    servicio_usuario.hacer_admin(usuario_ref)
    return redirect(href)

def desactivar_usuario(request):
    if request.method == 'POST':
        servicio_usuario.inhabilitar_usuario(request.POST.get('usuario'))
        return redirect(request.POST.get('href'))

def activar_usuario(request):
    if request.method == 'POST':
        servicio_usuario.habilitar_usuario(request.POST.get('usuario'))
        return redirect(request.POST.get('href'))

def get_contexto_basico(usuario):
    usuario = servicio_usuario.get_usuario(usuario)
    context = {
            "userid": usuario.id,
            "username": usuario.username,
            "name": usuario.first_name,
            "lastname": usuario.last_name,
            "email": usuario.email,
            "datejoined": usuario.date_joined,
            "birthday": usuario.fecha_nacimiento if usuario.fecha_nacimiento is not None else '',
            "document": usuario.cedula if usuario.cedula is not None else '',
            # 'perfilImagenUrl': servicioUsuario.get_usuario_image_url(usuario.id),
            # 'sesionPerfilImagenUrl': servicioUsuario.get_usuario_image_url(request.user.id),
        }
    return context


@register.filter
def get_usuario_imagen_perfil(nombre_usuario):
    return servicio_usuario.get_usuario_image_url(nombre_usuario)

@register.filter
def es_usuario_logueado(usuario):
    return servicio_usuario.get_esta_logueado(usuario)

@register.filter
def es_usuario_aprobado(usuarios, usuario):
    return usuario in usuarios and usuario.aprobado

@register.filter
def es_admin(usuarios, usuario):
    return usuario in usuarios and usuario.is_superuser

@register.filter
def tiene_permiso_sistema(usuario_ref, permiso):
    usuario = servicio_usuario.get_usuario(usuario_ref)
    return servicio_usuario_rol_sistema.usuario_tiene_permiso(usuario, permiso) or usuario.is_superuser

@register.filter
def activo_en_proyecto(usuario_ref):
    usuario = servicio_usuario.get_usuario(usuario_ref)
    return servicio_usuario.get_proyectos_de_usuario(usuario).count() > 0
