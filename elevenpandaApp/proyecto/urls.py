from django.urls import path
from .views import agregar_atributo_de_tdi, agregar_miembro_a_proyecto, agregar_tdi, \
    deshabilitar_miembro_de_proyecto, agregar_rol_de_proyecto_a_miembro, \
    cambiar_nombre_descripcion_proyecto, cambiar_nombre_tdi, eliminar_atributo_de_tdi, \
    eliminar_rol_de_proyecto_a_miembro, eliminar_tdi, agregar_proyecto, proyecto_view, \
    proyectos_lista_view, agregar_rol_a_proyecto, remover_rol_de_proyecto, \
    agregar_permiso_a_rol, remover_permiso_de_rol, agregar_gerente_a_proyecto, importar_tdi, activar_proyecto, \
    agregar_fase_a_proyecto, eliminar_fase_de_proyecto, proyecto_miembros_view, proyecto_configuracion_general, \
    cancelar_proyecto, proyecto_configuracion_roles, proyecto_configuracion_fases, proyecto_configuracion_tdis, \
    agregar_tdi_a_fase, agregar_rol_de_fases, agregar_fase_rol_permiso, agregar_comite_usuario, \
    remover_comite_usuario, solicitar_romper_lineabase, confirmar_romper_lineabase, finalizar_proyecto, \
    adjuntar_archivo_item, remover_archivo_item, reporte_de_items_de_proyecto, reporte_de_cambios_de_proyecto

urlpatterns = [
    path('lista/', proyectos_lista_view),
    path('<int:proyecto_id>/', proyecto_view),
    path('<int:proyecto_id>/miembros/', proyecto_miembros_view),
    path('<int:proyecto_id>/config/', proyecto_configuracion_general),
    path('<int:proyecto_id>/roles/', proyecto_configuracion_roles),
    path('<int:proyecto_id>/fases/', proyecto_configuracion_fases),
    path('<int:proyecto_id>/tdis/', proyecto_configuracion_tdis),
    path('<int:proyecto_id>/agregar/comite/usuario/', agregar_comite_usuario),
    path('<int:proyecto_id>/remover/comite/usuario/', remover_comite_usuario),
    path('solicitar/romper/lineabase/', solicitar_romper_lineabase),
    path('confirmar/romper/lineabase/', confirmar_romper_lineabase),

    path('<int:proyecto_id>/reporte/items/', reporte_de_items_de_proyecto),
    path('<int:proyecto_id>/reporte/cambios/', reporte_de_cambios_de_proyecto),

    path('cancelar/', cancelar_proyecto),
    path('agregar/', agregar_proyecto),
    path('activar/', activar_proyecto),
    path('finalizar/', finalizar_proyecto),
    path('guardar_nombre_desc/', cambiar_nombre_descripcion_proyecto),
    path('gerente/agregar/', agregar_gerente_a_proyecto),
    path('fase/agregar/', agregar_fase_a_proyecto),
    path('fase/remover/', eliminar_fase_de_proyecto),
    path('fase/tdi/agregar/', agregar_tdi_a_fase),
    path('fase/rol/agregar/', agregar_rol_de_fases),
    path('fase/rol/permiso/agregar/', agregar_fase_rol_permiso),
    path('rol/agregar/', agregar_rol_a_proyecto),
    path('rol/remover/', remover_rol_de_proyecto),
    path('rol/permiso/agregar/', agregar_permiso_a_rol),
    path('rol/permiso/remover/', remover_permiso_de_rol),
    path('tdi/importar/', importar_tdi),
    path('tdi/agregar/', agregar_tdi),
    path('tdi/remover/', eliminar_tdi),
    path('tdi/cambiarnombre/', cambiar_nombre_tdi),
    path('tdi/atributo/agregar/', agregar_atributo_de_tdi),
    path('tdi/atributo/remover/', eliminar_atributo_de_tdi),
    path('miembro/agregar/', agregar_miembro_a_proyecto),
    path('miembro/remover/', deshabilitar_miembro_de_proyecto),
    path('miembro/rol/agregar/', agregar_rol_de_proyecto_a_miembro),
    path('miembro/rol/remover/', eliminar_rol_de_proyecto_a_miembro),
    path('item/adjuntar/', adjuntar_archivo_item),
    path('item/desadjuntar/', remover_archivo_item),
]
