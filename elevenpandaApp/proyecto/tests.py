from django.test import TestCase
from .models import Proyecto, Usuario, TipoDeItem, Item, Fase
from .services import ServicioProyectos, ServicioItems


class UsuariosTest(TestCase):

    servicio_proyectos = ServicioProyectos()
    servicio_items = ServicioItems()

    def setUp(self):
        """ Creación de proyecto de prueba """
        print('#####################################')
        print('##################################### Usuario de prueba')
        print('#####################################')
        print('Creando proyecto de prueba.')
        Proyecto.objects.create(nombre='proyecto test', descripcion='ninguna')
        print('Proyecto de prueba creado.')

    def test_modificar_proyecto(self):
        """
        Modifica datos del proyecto utilizando el servicio de proyectos y verifica si los cambios fueron realizados exitosamente
        """
        print('#####################################')
        print('##################################### Cambiar nombre y descripción de proyecto')
        print('#####################################')
        proyecto = Proyecto.objects.first()
        print('Cambiando nombre del proyecto.')
        self.servicio_proyectos.cambiar_nombre('NuevoNombre', proyecto)
        print('Verificando que el nombre del proyecto haya cambiado correctamente.')
        self.assertEqual(proyecto.nombre, 'NuevoNombre', 'No se cambió correctamente el nombre del proyecto.')
        print('Cambiando descripción del proyecto.')
        self.servicio_proyectos.cambiar_descripcion('Nueva descripción', proyecto)
        print('Verificando que la descripción del proyecto haya cambiado correctamente.')
        self.assertEqual(proyecto.descripcion, 'Nueva descripción', 'No se cambió correctamente la descripción del proyecto.')

    def test_miembros(self):
        """
        Agrega usuarios a un proyecto utilizando el servicio de proyectos y verifica si los usuarios fueron agregados y posteriormente removidos correctamente
        """
        print('#####################################')
        print('##################################### Miembros del proyecto')
        print('#####################################')
        proyecto = Proyecto.objects.first()
        print('Creando usuario de prueba.')
        miembro = Usuario.objects.create(username='luis', password='12345678', aprobado=True)
        print('Agrengando miembro a proyecto.')
        self.servicio_proyectos.agregar_miembro(miembro, proyecto)
        print('Verificando que el usuario haya sido agregado correctamente.')
        cantidad = self.servicio_proyectos.get_miembros(proyecto).count()
        self.assertEqual(cantidad, 1, 'Se esperaba que hubiera un usuario asignado al proyecto, pero se encontraron ' + str(cantidad))
        if cantidad == 1:
            print('Inhabilitando usuario de proyecto.')
            self.servicio_proyectos.deshabilitar_miembro(miembro, proyecto)
            print('Verificando que el usuario haya sido inhabilitado correctamente.')
            cantidad = self.servicio_proyectos.get_miembros(proyecto).count()
            self.assertEqual(cantidad, 0,
                'Se esperaba que no hubiera usuario asignado al proyecto, pero se encontraron ' + str(cantidad))

    def test_tipo_de_items(self):
        """
        Agrega tipos de ítems a un proyecto utilizando el servicio de proyectos y verifica si los usuarios fueron agregados y posteriormente removidos correctamente
        """
        print('#####################################')
        print('##################################### Tipos de ítem del proyecto')
        print('#####################################')
        proyecto = Proyecto.objects.first()
        print('Creando tipo de ítem de prueba con referencia a el proyecto.')
        tdi = TipoDeItem.objects.create(nombre='tdi prueba', proyecto=proyecto)
        print('Verificando que el tipo de item haya sido agregado correctamente.')
        cantidad = self.servicio_proyectos.get_tipos_de_item(proyecto).count()
        self.assertEqual(cantidad, 1, 'Se esperaba que hubiera un tipo de ítem asignado al proyecto, pero se encontraron ' + str(cantidad))

    def test_rol_de_proyecto(self):
        """
        Agrega roles a un proyecto utilizando el servicio de proyectos y verifica si los usuarios fueron agregados y posteriormente removidos correctamente
        :return:
        """
        print('#####################################')
        print('##################################### Roles del proyecto')
        print('#####################################')
        proyecto = Proyecto.objects.first()
        print('Creando y signando rol a proyecto')
        self.servicio_proyectos.agregar_rol_a_proyecto(proyecto, 'rol prueba')
        print('Verificando que el rol haya sido agregado correctamente.')
        cantidad = self.servicio_proyectos.get_roles_proyecto(proyecto).count()
        self.assertEqual(cantidad, 1, 'Se esperaba que hubiera un rol asignado al proyecto, pero se encontraron ' + str(cantidad))
        print('Removiendo el rol del proyecto.')
        self.servicio_proyectos.remover_rol_de_proyecto(self.servicio_proyectos.get_roles_proyecto(proyecto).first())
        print('Verificando que el rol haya sido removido correctamente.')
        cantidad = self.servicio_proyectos.get_roles_proyecto(proyecto).count()
        self.assertEqual(cantidad, 0, 'Se esperaba que no hubiera roles asignados al proyecto, pero se encontraron ' + str(cantidad))


    def test_items(self):
        """
        Para los CU 06.01, 06.02, 06.03 y 06.06. Crea un item, lo muestra, procede a modificar algunos atributos del item a
        traves de un servicio y lo muestra luego de ser modificado.
        :return:
        """
        print('#####################################')
        print('##################################### Creacion y modificacion de items')
        print('#####################################')
        proyecto = Proyecto.objects.first()
        print('Creando tipo de ítem de prueba.')
        tdi = TipoDeItem.objects.create(nombre='tdi prueba', proyecto=proyecto)
        print('Creando una fase de prueba.')
        fase = Fase.objects.create(nombre="fase de prueba", proyecto=proyecto, numero_fase=1)
        print('Creando un item de prueba.')
        item = Item.objects.create(nombre="item de prueba", prefijo="RF", codigo="00", tipo_de_item=tdi, fase=fase, estado="pe", descripcion="descripcion de Item de Prueba", costo=0)
        print('Verificando que el item se haya sido creado correctamente.')
        item_creado = self.servicio_items.get_item(item)
        self.assertEqual(item_creado, item, 'El ítem creado y el ítem obtenido a través del servicio de ítems no son los mismos.')
        print('Mostrando el item de prueba.')
        print(item)
        print('Modificando el item de prueba.')
        usuario = Usuario.objects.create(username="nombre", password="1234", aprobado=True)
        self.servicio_items.modificar_item(item, "item de prueba modificado", "Descripcion modificada", 1, "pa", None, usuario)
        print('Mostrando el item de prueba modificado.')
        print(item)

