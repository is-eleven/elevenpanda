from django.contrib import admin
from .models import Proyecto, Fase, ProyectoRol

admin.site.register(Proyecto)
admin.site.register(Fase)
admin.site.register(ProyectoRol)
