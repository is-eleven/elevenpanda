# coding=utf-8
from lineaBase.models import LineaBaseRomperSolicitudes, RomperLineaBaseVotos
from .models import Proyecto, TipoDeItem, AtributoDinamico, ProyectoUsuario, UsuarioRolProyecto, ProyectoRol, \
	UsuarioProyecto, Fase, Item, ItemVersion, ItemAtributoVersion, ItemRelacionVersion, UsuarioDeComiteDeCambios, \
	ItemHistorial, RolUsuarioFase
from Usuarios.models import Usuario
from Usuarios.services import ServicioUsuario
from roles.services import ServicioRoles, ServicioUsuarioRolSistema, ServicioPermisos
from roles.models import RolUsuario, Rol, RolPermiso
from proyecto.models import ItemAtributo, ItemRelacion
# from .views import servicio_linea_base

import dropbox

servicio_usuario = ServicioUsuario()
servicio_roles = ServicioRoles()
servicio_usuario_rol_sistema = ServicioUsuarioRolSistema()
servicio_permisos = ServicioPermisos()


class ServicioProyectos:
	"""
	Clase que organiza los servicios para proyectos, simplificacion de api.
	"""

	def agregar_proyecto(self, nombre, descripcion, estado='pe'):
		"""
		Agregar un nuevo proyecto a la base de datos dados los parámetros básicos requeridos.

		:param nombre: nombre que se asignará al proyecto.
		:param descripcion: descripción opcional del prolyecto.
		:param estado: estado inicial del proyecto, por defecto es 'pe' (pendiente).
		"""
		Proyecto.objects.create(nombre=nombre, descripcion=descripcion, estado=estado).save()

	def get_proyectos(self):
		"""
		Otener la lista completa de proyectos del sistema.

		:return: la lista de todos los proyectos del sistema.
		"""
		from roles.services import ServicioPermisos
		ServicioPermisos().crear_permisos()
		return Proyecto.objects.all()

	def get_proyecto(self, proyecto):
		"""
		Convierte una refrencia a un proyecto (id, nombre de proyecto, id en texto) a el objeto proyecto que representa.

		:param proyecto: La identificacion referencial del proyecto
		:return: el objeto proyecto que representa
		"""
		if isinstance(proyecto, str) and proyecto.isdigit():
			return Proyecto.objects.filter(id=int(proyecto)).first()
		if isinstance(proyecto, str):
			return Proyecto.objects.filter(nombre=proyecto).first()
		if isinstance(proyecto, int):
			return Proyecto.objects.filter(id=proyecto).first()
		return proyecto

	def cambiar_nombre(self, nombre, proyecto):
		"""
		Modifica el atributo 'nombre' de un proyecto dado.

		:param nombre: nuevo nombre para el proyecto
		:param proyecto: identificador del proyecto a modificar
		"""
		proyecto = self.get_proyecto(proyecto)
		proyecto.nombre = nombre
		proyecto.save()

	def cambiar_descripcion(self, descripcion, proyecto):
		"""
		Modifica el atributo 'descripción' de un proyecto dado

		:param nombre: nueva descripción para el proyecto
		:param proyecto: identificador del proyecto a modificar
		"""
		proyecto = self.get_proyecto(proyecto)
		proyecto.descripcion = descripcion
		proyecto.save()

	def cancelar_proyecto(self, proyecto_id):
		"""
		Cambia el estado de un proyecto a cancelado.

		:param proyecto_id: Id del proyecto en cuestion
		"""
		proyecto = self.get_proyecto(proyecto_id)
		proyecto.estado = 'ca'
		proyecto.save()

	def get_miembros(self, proyecto_ref, mostrar_no_validos=False, ordenar_por=None, filtro=None):
		"""
		Obtiene todos los usuarios asignados a un proyecto

		:param filtro: descripción del filtro para la lista, pueden ser: 'gerente' o 'inactivos'
		:param proyecto: El proyecto en cuestion
		:param mostrar_no_validos: indica si también se listarán los miembros de un proyecto que fueron invalidados, por defeco es Falso
		:param ordenar_por: especifica el atributo por el cual la lista de usuarios devuelto se ordenará, los valores posibles son: nombre, ultimoAgregado, primeroAgregado.
		:return: una lista con los miembros del proyecto, si no tiene ningun miembro, retorna una lista vacia
		"""
		if filtro == 'gerente':
			return Usuario.objects.filter(id=self.get_gerente(proyecto_ref).id).all()

		def get_order(orden):
			usuario_orden = proyecto_usuarios.order_by(orden).all()
			pk_list = [miembro.usuario.id for miembro in usuario_orden]
			clauses = ' '.join(['WHEN id=%s THEN %s' % (pk, i) for i, pk in enumerate(pk_list)])
			ordering = 'CASE %s END' % clauses
			return Usuario.objects.filter(id__in=pk_list).extra(
				select={'ordering': ordering}, order_by=('ordering',))

		proyecto = self.get_proyecto(proyecto_ref)
		if mostrar_no_validos:
			if filtro == 'inactivos':
				proyecto_usuarios = ProyectoUsuario.objects.filter(proyecto=proyecto, es_valido=False).all()
			else:
				proyecto_usuarios = ProyectoUsuario.objects.filter(proyecto=proyecto).all()
		else:
			if filtro == 'inactivos':
				return Usuario.objects.none()
			else:
				proyecto_usuarios = ProyectoUsuario.objects.filter(proyecto=proyecto, es_valido=True).all()
		if ordenar_por is not None:
			if ordenar_por == 'ultimoAgregado':
				return get_order('-fecha_union')
			elif ordenar_por == 'primeroAgregado':
				return get_order('fecha_union')
			elif ordenar_por == 'nombreDeUsuario':
				return Usuario.objects.filter(id__in=[miembro.usuario.id for miembro in proyecto_usuarios]).order_by(
					'username').all()
		return Usuario.objects.filter(id__in=[miembro.usuario.id for miembro in proyecto_usuarios]).all()

	def get_miembros_faltantes(self, proyecto_ref):
		"""
		Funcion que obtiene todos los usaurios que no estan relacionados a un proyecto especifico.

		:param proyecto_ref: Referencia al proyecto en cuestion
		:return: Una lista de usuarios, o una lista vacia
		"""
		miembros_existentes_id = [miembro.id for miembro in self.get_miembros(proyecto_ref)]
		return servicio_usuario.get_usuarios_activos(order_by='nombreDeUsuario').exclude(id__in=miembros_existentes_id)

	def get_miembros_inactivos(self, proyecto_ref):
		"""
		Obtener todos los usuarios inactivos de un proyecto.

		:param proyecto: El proyecto en cuestion
		:return: Retorna una lista con los miembros inactivos del proyecto
		"""
		proyecto = self.get_proyecto(proyecto_ref)
		usuarios_proyecto = ProyectoUsuario.objects.filter(proyecto=proyecto).all()
		usuarios = []
		for usuario_proyecto in usuarios_proyecto:
			if not usuario_proyecto.es_valido:
				usuarios.append(usuario_proyecto.usuario)
		usuarios_id = [usuario.id for usuario in usuarios]
		return Usuario.objects.filter(id__in=usuarios_id)

	def es_miembro_habilitado(self, proyecto_ref, usuario_ref):
		"""
		Define si un usuario es (fue) miembro del proyecto pero su está inahilitado.

		:param proyecto_ref: proyecto donde se buscará al usuario
		:param usuario_ref: usuario a definir estado
		:return: si el miembro está o no deshabilitado
		"""
		usuario = servicio_usuario.get_usuario(usuario_ref)
		proyecto = self.get_proyecto(proyecto_ref)
		proyecto_usuario = ProyectoUsuario.objects.filter(usuario=usuario, proyecto=proyecto).first()
		return proyecto_usuario is not None and proyecto_usuario.es_valido

	def agregar_miembro(self, nombre, proyecto_ref):
		"""
		Asignar un nuevo miembro a un proyecto dado.

		:param nombre: identificador del usuario a agregar
		:param proyecto_ref: proyecto donde el usuario será agregado
		"""
		if servicio_usuario.get_existe_usuario(nombre):
			usuario = servicio_usuario.get_usuario(nombre)
			proyecto = self.get_proyecto(proyecto_ref)
			ya_tiene_miembro = self.tiene_miembro(proyecto, usuario)
			# Si el usuario ya está en el proyecto pero está deshabilitado, es vuelto a habilitar y se reinicia su fecha de incorporación
			if ya_tiene_miembro and not self.es_miembro_habilitado(proyecto, usuario):
				proyecto_usuario = ProyectoUsuario.objects.filter(usuario=usuario, proyecto=proyecto).first()
				proyecto_usuario.es_valido = True
				from django.utils.timezone import now
				proyecto_usuario.fecha_union = now()
				proyecto_usuario.save()
			# Si es la primera vez, se crea un nuevo registro
			elif not ya_tiene_miembro:
				ProyectoUsuario.objects.create(usuario=usuario, proyecto=proyecto).save()

	def get_roles_proyecto_usuario(self, proyecto_ref, usuario_ref, ambiente='pr'):
		"""
		Obtiene la lista de roles de proyecto que le fue asignado a un usuario en un determinado proyecto.

		:param ambiente: ambito del rol en el proyecto, pueden ser: 'pr' (rol de proyecto), 'fa' (rol de fase)
		:param proyecto_ref: proyecto cuyos roles serán considerados
		:param usuario_ref: miembro del proyecto a identificar
		:return: la lista de roles del usuario en el proyecto especificado
		"""
		proyecto = self.get_proyecto(proyecto_ref)
		usuario = servicio_usuario.get_usuario(usuario_ref)
		roles_id = []
		for rol_proyecto in [urp.rol_proyecto for urp in UsuarioRolProyecto.objects.filter(usuario=usuario).all()]:
			if proyecto.id == rol_proyecto.proyecto.id:
				if rol_proyecto.rol.ambiente == ambiente:
					roles_id.append(rol_proyecto.rol.id)
		return Rol.objects.filter(id__in=roles_id)

	def get_miembros_con_rol(self, proyecto_ref, rol_ref):
		"""
		Identifica los roles asignados a un usuario en un proyecto dado.

		:param proyecto_ref: proyecto a considerar
		:param rol_ref: rol del proyecto a evaluar
		:return: la lista de usuarios que tienen el rol dentro del proyecto
		"""
		proyecto = self.get_proyecto(proyecto_ref)
		rol = servicio_roles.get_rol(rol_ref)
		proyecto_rol = ProyectoRol.objects.filter(proyecto=proyecto, rol=rol)
		return proyecto_rol.usuarios.all()

	def miembro_tiene_rol_de_proyecto(self, usuario_ref, rol_ref):
		"""
		Verifica si un miembro tiene roles dentro de un proyecto.

		:param usuario_ref: Referencia al usuario en cuestion
		:param rol_ref: Referencia al proyecto en cuestion
		:return:
		"""
		usuario = servicio_usuario.get_usuario(usuario_ref)
		rol = servicio_roles.get_rol(rol_ref)
		rol_proyecto = ProyectoRol.objects.filter(rol=rol).first()
		return UsuarioRolProyecto.objects.filter(usuario=usuario, rol_proyecto=rol_proyecto).first() is not None

	def get_roles_proyecto(self, proyecto_ref, solo_no_vacios=False, ambiente='pr'):
		"""
		Obtiene la lista de roles de un proyecto.

		:param ambiente:
		:param solo_no_vacios:
		:param proyecto_ref: proyecto a considerar
		:return: lista de roles del proyecto
		"""
		roles_proyecto = ProyectoRol.objects.filter(proyecto=self.get_proyecto(proyecto_ref))
		if solo_no_vacios:
			roles_id = [pr.rol.id for pr in roles_proyecto if pr.rol.ambiente == ambiente
			            and servicio_roles.get_permisos_rol(pr.rol).count() > 0]
		else:
			roles_id = [pr.rol.id for pr in roles_proyecto if pr.rol.ambiente == ambiente]
		return Rol.objects.filter(id__in=roles_id)

	def agregar_rol_a_proyecto(self, proyecto_ref, rol_nombre):
		"""
		Crear un nuevo rol dentro de un proyecto específico.

		:param proyecto_ref: proyecto donde se creará el rol
		:param rol_nombre: nombre del rol a crear
		"""
		proyecto = self.get_proyecto(proyecto_ref)
		rol = Rol.objects.create(nombre=rol_nombre, ambiente='pr')
		rol.save()
		ProyectoRol.objects.create(proyecto=proyecto, rol=rol).save()

	def agregar_rol_proyecto_a_miembro(self, nombre, proyecto_ref, rol_id):
		"""
		Asignar un rol existente a un miembro del proyecto.

		:param nombre: identificador del miembro
		:param proyecto: proyecto a considerar
		:param rol_id: rol que será asignado al miembro dentro del proyecto
		"""
		rol = servicio_roles.get_rol(rol_id)
		proyecto = self.get_proyecto(proyecto_ref)
		rol_proyecto = ProyectoRol.objects.filter(rol=rol, proyecto=proyecto).first()
		if rol_proyecto is None:
			rol_proyecto = ProyectoRol.objects.create(rol=rol, proyecto=proyecto)
			rol_proyecto.save()
		UsuarioRolProyecto.objects.create(usuario=servicio_usuario.get_usuario(nombre),
		                                  rol_proyecto=rol_proyecto).save()

	def eliminar_rol_proyecto_a_miembro(self, nombre, proyecto_ref, rol_id):
		"""
		Remueve un rol de un usuario dentro del un proyecto específico.

		:param nombre: identificador del usuario
		:param proyecto: proyecto a considerar
		:param rol_id: rol que será removido al usuario dentro del proyecto
		"""
		rol = servicio_roles.get_rol(rol_id)
		proyecto = self.get_proyecto(proyecto_ref)
		usuario = servicio_usuario.get_usuario(nombre)
		roles_proyectos = ProyectoRol.objects.filter(rol=rol, proyecto=proyecto)
		usuario_roles_proyectos = UsuarioRolProyecto.objects.filter(usuario=usuario)
		for rol_proyecto in roles_proyectos:
			for usuario_rol_proyecto in usuario_roles_proyectos:
				if rol_proyecto.rol.id == rol.id and rol_proyecto.proyecto.id == proyecto.id and usuario_rol_proyecto.usuario.id == usuario.id:
					UsuarioRolProyecto.objects.filter(usuario=usuario, rol_proyecto=rol_proyecto).delete()

	def deshabilitar_miembro(self, usuario_ref, proyecto_ref):
		"""
		Deja deshabilitado a un usuario dentro de un proyecto.

		:param usuario_ref: usuario a inhabilitar
		:param proyecto_ref: proyecto donde será inhabilitado
		"""
		# remover todos los roles asignados
		roles = self.get_roles_proyecto_usuario(proyecto_ref, usuario_ref)
		for rol in roles:
			self.eliminar_rol_proyecto_a_miembro(usuario_ref, proyecto_ref, rol)
		# invalidar usuario
		pu = ProyectoUsuario.objects.filter(usuario=servicio_usuario.get_usuario(usuario_ref),
		                                    proyecto=self.get_proyecto(proyecto_ref)).first()
		pu.es_valido = False
		pu.save()

	def tiene_tipo_de_item_con_nombre(self, proyecto_ref, tdi_ref):
		"""
		Define si un proyecto tiene un tipo de ítem con un nombre específico

		:param proyecto_ref: proyecto a considerar
		:param tdi_ref: nombre de tipo de ítem a considerar
		:return: si existe un tipo de ítem con cierto nombre dentro de un proyecto específico
		"""
		# proyecto = self.get_proyecto(proyecto_ref)
		servicio_tdi = ServicioTipoDeItem()
		tdis = self.get_tipos_de_item(proyecto_ref)
		tdi = servicio_tdi.get_tipo_de_item(tdi_ref)
		for tdi_iter in tdis:
			if tdi.nombre == tdi_iter.nombre:
				return True
		return False

	def importar_tdi(self, proyecto_ref, tdi_ref):
		"""
		Tomar un típo de ítem de referencia y lo copia exactamente igual dentro de un proyecto destino.

		:param proyecto_ref: proyecto donde se copiará el tipo de ítem
		:param tdi_ref: tipo de ítem a copiar
		"""
		# Obtener datos
		servicio_tdi = ServicioTipoDeItem()
		proyecto = self.get_proyecto(proyecto_ref)
		tdi_orig = servicio_tdi.get_tipo_de_item(tdi_ref)
		# Crear nuevo tipo de ítem
		if self.tiene_tipo_de_item_con_nombre(proyecto, tdi_orig):
			return False
		TipoDeItem.objects.create(nombre=tdi_orig.nombre, proyecto=proyecto).save()
		nuevo_tdi = TipoDeItem.objects.last()
		# Asignar atributos a nuevo tipo de ítem
		tdi_atributos = servicio_tdi.get_atributos_de_tdi(tdi_orig)
		for atributo in tdi_atributos:
			servicio_tdi.agregar_atributo_de_tdi(atributo.titulo, nuevo_tdi, atributo.tipo, atributo.es_obligatorio,
			                                     atributo.min, atributo.max)
		return True

	def tiene_miembro(self, proyecto_ref, usuario_ref):
		"""
		Define si un proyecto tiene incluido a un usuario específico.

		:param proyecto_ref: proyecto a considerar
		:param usuario_ref: usuario a evaluar
		:return: si un usuario existe o no dentro de un proyecto
		"""
		usuario = servicio_usuario.get_usuario(usuario_ref)
		if usuario is not None:
			miembros_proyecto = self.get_miembros(proyecto_ref=proyecto_ref, mostrar_no_validos=True)
			for miembro_proyecto in miembros_proyecto:
				if usuario.id == miembro_proyecto.id:
					return True
		return False

	def tiene_miembro_activo(self, proyecto_ref, usuario_ref):
		"""
		Define si un usuario existe y además está activo dentro de un proyecto.

		:param proyecto_ref: proyecto a considerar
		:param usuario_ref: usuario a evaluar
		:return: si un usuario existe y está activo o no dentro de un proyecto
		"""
		return self.tiene_miembro(proyecto_ref, usuario_ref) and self.es_miembro_habilitado(proyecto_ref, usuario_ref)

	def tiene_rol(self, proyecto_ref, rol_ref):
		"""
		Define si un proyecto tiene asignado un rol específico.

		:param proyecto_ref: Proyecto a considerar
		:param rol_ref: Rol buscado
		:return: Si el proyecto posee el rol epecificado o no
		"""
		roles_proyecto = self.get_roles_proyecto(proyecto_ref)
		rol = servicio_roles.get_rol(rol_ref)
		if rol is not None:
			for rol_proyecto in roles_proyecto:
				if rol.id == rol_proyecto.id:
					return True
		return False

	def get_gerente(self, proyecto_ref):
		"""
		Obtiene el usuario gerente de un proyecto dado.

		:param proyecto_ref: Proyecto a considerar
		:return: El usuario gerente del proyecto en cuestión
		"""
		return self.get_proyecto(proyecto_ref).gerente

	def asignar_gerente(self, proyecto_ref, usuario_ref):
		"""
		Asigna un gerente dentro de un proyecto específico.

		:param proyecto_ref: Proyecto al que se asignará un gerente
		:param usuario_ref: Usuario que será asignado como gerente
		"""
		proyecto = self.get_proyecto(proyecto_ref)
		usuario = servicio_usuario.get_usuario(usuario_ref)
		proyecto.gerente = usuario
		proyecto.save()
		if not self.tiene_miembro(proyecto, usuario):
			self.agregar_miembro(usuario, proyecto)

	def remover_rol_de_proyecto(self, rol_id):
		"""
		Remueve un rol asignado a un proyecto.

		:param rol_id: Rol a eliminar
		"""
		# obtener datos
		rol = servicio_roles.get_rol(rol_id)
		rol_proyecto = ProyectoRol.objects.filter(rol=rol)
		usuarios_rol_proyecto = UsuarioRolProyecto.objects.filter(rol_proyecto=rol_proyecto.first())
		# elimiar referencias del rol
		usuarios_rol_proyecto.delete()  # Elimina todas las relaciones de los usuarios con este rol en este proyecto
		rol_proyecto.delete()  # Elimina el rol del proyecto
		Rol.objects.filter(id=rol.id).delete()  # Elimina el rol

	def get_roles_faltantes(self, roles_existentes, proyecto_ref, ambiente='pr'):
		"""
		Toma una lista de roles y los compara con la lista de roles de un proyecto específico para devolver solo
		aquellos que no estén incluidos dentro de la primera lista.

		:param ambiente: tipo de rol en proyecto, puede ser: 'pr' (proyecto), 'fa' (fase)
		:param roles_existentes: lista de roles que el proyecto ya contiene
		:param proyecto_ref: proyecto a considerar
		:return: lista de proyectos faltantes
		"""
		proyecto = self.get_proyecto(proyecto_ref)
		roles_existentes_id = [re.id for re in roles_existentes]
		roles_proyecto_id = [pr.rol.id for pr in ProyectoRol.objects.filter(proyecto=proyecto) if
		                     pr.rol.ambiente == ambiente]
		return Rol.objects.filter(id__in=roles_proyecto_id).exclude(id__in=roles_existentes_id)

	def puede_ver_proyecto(self, usuario_ref, proyecto_ref):
		"""
		Define si un usuario tiene los permisos para ver un proyecto específico.

		:param usuario_ref: usuario a validar
		:param proyecto_ref: proyecto a considerar
		:return: si tiene permiso de ver el proyecto o no
		"""
		usuario = servicio_usuario.get_usuario(usuario_ref)
		proyecto = self.get_proyecto(proyecto_ref)
		if proyecto.gerente is not None and proyecto.gerente.id == usuario.id:
			return True
		if servicio_usuario_rol_sistema.usuario_tiene_permiso(usuario,
		                                                      'ver_todos_los_proyectos') or usuario.is_superuser:
			return True
		if servicio_usuario_rol_sistema.usuario_tiene_permiso(usuario, 'ver_proyectos_propios') and self.tiene_miembro(
				proyecto, usuario):
			return True
		return False

	def get_tipos_de_item(self, proyecto, mostrar_todos=True):
		"""
		Funcion para obtener los tipos de items de un proyecto especifico.

		:param proyecto: El proyecto en cuestion
		:param mostrar_todos: Define si se retornaran todos los tdis o solo aquellos que tengan al menos un atributo
		:return: Retorna los tipos de items del proyecto
		"""
		todos = TipoDeItem.objects.filter(proyecto=self.get_proyecto(proyecto)).all()
		if mostrar_todos:
			return todos
		tdis_con_atribs = []
		for tdi in todos:
			atribs = AtributoDinamico.objects.filter(tipo_de_item=tdi).all()
			if atribs.count() > 0:
				tdis_con_atribs.append(tdi)
		tdis_id = [tdi.id for tdi in tdis_con_atribs]
		return TipoDeItem.objects.filter(id__in=tdis_id).all()

	def get_tipos_de_item_disponibles(self, proyecto):
		"""
		Obtiene una lista de tipos de ítems que no fueron asignados a ninguna fase.

		:param proyecto: Proyecto del cual serán considerados los típos de ítem
		:return: lista de los tipos de ítem de un proyecto que no han sido asignados
		"""
		return TipoDeItem.objects.filter(proyecto=self.get_proyecto(proyecto), fase=None).all()

	def get_fases(self, proyecto, ordenar_por='numero_fase'):
		"""
		Funcion para obtener las fases de un proyecto especifico.

		:param proyecto: El proyecto en cuestion
		:return: Retorna las fases del proyecto
		"""
		ordenar_por = 'nombre' if ordenar_por == 'nombreDeFase' else 'fecha_creacion' if ordenar_por == '-ultimoAgregado' else 'fecha_creacion' if ordenar_por == 'primeroAgregado' else 'numero_fase'
		return Fase.objects.filter(proyecto=self.get_proyecto(proyecto)).order_by(ordenar_por).all()

	def get_fecha_creacion(self, proyecto):
		"""
		Funcion para obtener la fecha de creaacion de un proyecto especifico.

		:param proyecto: El proyecto en cuestion
		:return: Retorna la fecha de creacion del proyecto
		"""
		return self.get_proyecto(proyecto).fecha_creacion

	def get_roles_de_fases(self, proyecto_ref, solo_no_vacios=False):
		"""
		Obtiene todos los roles de fase en un proyecto dado.

		:param proyecto_ref: El proyecto en cuestion
		:param solo_no_vacios: Filtrar solo aquellos roles que tiene al menos un permiso asignado
		:return: Una lisata con los roles de fase del proyecto
		"""
		return self.get_roles_proyecto(proyecto_ref, solo_no_vacios, 'fa')

	def usuario_tiene_permiso_proyecto(self, usuario_ref, proyecto_ref, permiso_ref):
		"""
		Verifica si un usuario tiene un permiso dado en un proyecto dado

		:params usuario_ref: Referencia al usuario en cuestion
		:params proyecto_ref: Referencia al proyecto en cuestion
		:params permiso_ref: Referencia al permiso en cuestion
		:return: True si el usuario tiene el permiso, False en caso contrario
		"""
		usuario = servicio_usuario.get_usuario(usuario_ref)
		proyecto = self.get_proyecto(proyecto_ref)
		permiso = servicio_permisos.get_permiso(permiso_ref)
		roles = [r for r in self.get_roles_proyecto_usuario(proyecto, usuario)]
		for rol in roles:
			for rol_permiso in servicio_roles.get_permisos_rol(rol):
				if rol_permiso.id == permiso.id:
					return True
		return False

	def get_miembros_del_comite(self, proyecto_ref):
		"""
		Obtiene una lista de usuario con los miembros del comité de controls de cambios asignado al proyecto dado.

		:params proyecto_ref: Proyecto a considerar para obtener la lista de usuarios
		:return: lista de usuarios que pertenecen al comité de cambios del proyecto
		"""
		proyecto = self.get_proyecto(proyecto_ref)
		return [m.usuario for m in UsuarioDeComiteDeCambios.objects.filter(proyecto=proyecto)]

	def agregar_miembro_comite(self, usuarios_id, proyecto_ref):
		"""
		Asigna uno o varios usuarios a la lista de miembros del comité de control de cambios de un proyecto dado.

		:params usuarios_id: lista de los ids de los usuarios a ser asignados como miembros
		:params proyecto_ref: proyecto donde los usuarios serán asignados
		"""
		proyecto = self.get_proyecto(proyecto_ref)
		for miembro_id in usuarios_id.split(','):
			miembro = servicio_usuario.get_usuario(miembro_id)
			UsuarioDeComiteDeCambios.objects.create(proyecto=proyecto, usuario=miembro)

	def remover_miembro_comite(self, usuario_id, proyecto_ref):
		"""
		Remueve un usuario de los miembros del comité de control de cambio de un proyecto dado.

		:params usuario_id: usuario que será removido del comité
		:params proyecto_ref: proyecto de donde se removerá un usuario del comité
		"""
		proyecto = self.get_proyecto(proyecto_ref)
		miembro = servicio_usuario.get_usuario(usuario_id)
		UsuarioDeComiteDeCambios.objects.get(proyecto=proyecto, usuario=miembro).delete()
		# Elimiar todos sus votos
		solicitudes_id = [s.id for s in LineaBaseRomperSolicitudes.objects.filter(linea_base__fase__proyecto=proyecto)]
		RomperLineaBaseVotos.objects.filter(usuario=miembro, solicitud_id__in=solicitudes_id).delete()

	def get_proyectos_usuario_es_miembro_comite(self, usuario_ref):
		"""
		Obtiene una lista de proyectos en los cuales un usuario dado es miembro del comité de control de cambios.

		:params usuario_ref: usuario a considerar
		:return: lista de proyectos
		"""
		usuario = servicio_usuario.get_usuario(usuario_ref)
		result = []
		for proy in self.get_proyectos():
			if self.usuario_es_miembro_comite(usuario, proy):
				result.append(proy)
		return result

	def usuario_es_miembro_comite(self, usuario_ref, proyecto_ref):
		"""
		Define si un usuario dados es miembros del comité de control de cambios de un proyecto dado

		:params usuario_ref: usuario a definir si es miembro
		:params proyecto_ref: proyecto donde se busca identificar al usuario
		:return: si el usuario es miembro del comité del proyecto dado o no
		"""
		proyecto = self.get_proyecto(proyecto_ref)
		usuario = servicio_usuario.get_usuario(usuario_ref)
		return UsuarioDeComiteDeCambios.objects.filter(proyecto=proyecto, usuario=usuario).count() > 0

	def puede_finalizar_proyecto(self, proyecto_ref):
		"""
		Define si un proyecto cumple con todas las condiciones para ser finalizado.
		Esto se da solo cuando todas las fases del proyecto están finalizadas.

		:params proyecto_ref: proyecto a considerar
		:return: si el proyecto en consideración puede ser finalizado o no
		"""
		for fase in self.get_fases(proyecto_ref):
			if not fase.finalizado:
				return False
		return True

	def finalizar_proyecto(self, proyecto_ref):
		"""
		Cambia el estado de un proyecto a finalizado.

		:params proyecto_ref: proyecto que será finalizado
		"""
		proyecto = self.get_proyecto(proyecto_ref)
		proyecto.estado = 'fi'
		proyecto.save()


class ServicioTipoDeItem:
	"""
	Clase que organiza los servicios para Tipos de Item, simplificacion de api.
	"""
	# servicio_items = ServicioItems()
	servicio_proyectos = ServicioProyectos()

	def get_atributo_de_tdi(self, atributo):
		"""
		Funcion que obtiene un atributo especifico.

		:param atributo: El atributo en cuestion
		:return: Retorna el objeto atributo especificado, si no se encuentra retorna None. En caso de error retorna la referencia recibida.
		"""
		if isinstance(atributo, str) and atributo.isdigit():
			return AtributoDinamico.objects.filter(id=int(atributo)).first()
		elif isinstance(atributo, int):
			return AtributoDinamico.objects.filter(id=atributo).first()
		return atributo

	def get_tipos_de_item_de_proyecto(self, proyecto_ref):
		"""
		Funcion que retorna los Tipos de Item de un proyecto en especifico.

		:param proyecto_ref: Referencia al proyecto
		:return: Retorna una lista de proyectos, en caso de que un proyecto no tenga Tipos de Item, retorna una lista vacia
		"""
		proyecto = self.servicio_proyectos.get_proyecto(proyecto_ref)
		return TipoDeItem.objects.filter(proyecto=proyecto).all()

	def get_tipo_de_item(self, tdi):
		"""
		Funcion que retorna un tipo de item en especifico.

		:param tdi: El tipo de item en cuestion
		:return: Retorna el objeto tipo de item especificado, si no se encuentra retorna None. En caso de error retorna la referencia recibida.
		"""
		if isinstance(tdi, str) and tdi.isdigit():
			return TipoDeItem.objects.filter(id=int(tdi)).first()
		if isinstance(tdi, str):
			return TipoDeItem.objects.filter(nombre=tdi).first()
		elif isinstance(tdi, int):
			return TipoDeItem.objects.filter(id=tdi).first()
		return tdi

	def get_tipos_de_item(self):
		"""
		Funcion que obtiene todos los tipos de item del sistema.

		:return: Retorna una lista de todos los tipos de item del sistema, si no existen tipos de item, retorna una lista vacia
		"""
		return TipoDeItem.objects.all()

	def get_atributos_de_tdi(self, tdi_ref):
		"""
		Funcion que obtiene todos los atributos de un tipo de item especificado.

		:param tdi_ref: Referencia al tipo de item
		:return: Una lista de todos de atributos del tipo de item, si este no tiene ninguno, retorna una lista vacia
		"""
		return AtributoDinamico.objects.filter(tipo_de_item=self.get_tipo_de_item(tdi_ref))

	def agregar_tipo_de_item(self, nombre, proyecto_id):
		"""
		Funcion que crea un tipo de item y lo asigna a un proyecto-

		:param nombre: Identificacion del nuevo tipo de item
		:param proyecto_id: Id del proyecto a recibir el nuevo tipo de item
		:return:
		"""
		proyecto = self.servicio_proyectos.get_proyecto(proyecto_id)
		TipoDeItem.objects.create(nombre=nombre, proyecto=proyecto).save()

	def eliminar_tipo_de_item(self, tdi_id):
		"""
		Elimina un tipo de item especificado.

		:param tdi_id: Id del tipo de item a eliminar
		"""
		# if self.servicio_items.get_items_con_tdi(self.get_tipo_de_item(tdi_id)).count() == 0: TODO: Validación para eliminar tipo de ítem
		TipoDeItem.objects.filter(id=tdi_id).first().delete()

	def cambiar_nombre_tdi(self, tdi_id, nombre):
		"""
		Funcion para cambiar el nombre de un tipo de item especificado.

		:param tdi_id: Id del tipo de item a modificar
		:param nombre: Nuevo nombre para el tipo de item
		"""
		tdi = self.get_tipo_de_item(tdi_id)
		tdi.nombre = nombre
		tdi.save()

	def eliminar_atributo_de_tdi(self, atributo):
		"""
		Elimina un atributo de tipo de item especificado.

		:param atributo: El atributo en cuestion
		"""
		AtributoDinamico.objects.filter(id=self.get_atributo_de_tdi(atributo).id).first().delete()

	def agregar_atributo_de_tdi(self, titulo, tdi_ref, tipo, es_obligatorio, min, max):
		"""
		Crea un nuevo atributo dinamico.

		:param nombre: Identificacion del atributo
		:param tipo: Tipo de dato del atributo (booleano, numerico, texto, url, archivo)
		:param es_obligatorio: Especifica si el atributo creado es obligatorio o no
		:param min: Valor minimo del atributo (para atributos numericos)
		:param max: Valor maximo del atributo (para atributos numericos)
		"""
		tdi = ServicioTipoDeItem().get_tipo_de_item(tdi_ref)
		AtributoDinamico.objects.create(titulo=titulo, tipo=tipo, tipo_de_item=tdi, es_obligatorio=es_obligatorio,
		                                min=min, max=max).save()

	def esta_en_proyecto(self, proyecto_ref, tdi_ref):
		"""
		Verifica si un tipo de item especifico es de un proyecto.

		:param proyecto_ref: Referencia al proyecto en cuestion
		:param tdi_ref: Referencia al tipo de item en cuestion
		:return: Returna True si el tipo de item es parte del proyecto, retorna False en caso contrario o si no existe el tipo de item
		"""
		tdis_proyecto = self.servicio_proyectos.get_tipos_de_item(proyecto_ref)
		tdi = self.get_tipo_de_item(tdi_ref)
		if tdi is not None:
			for tdi_proyecto in tdis_proyecto:
				if tdi.id == tdi_proyecto.id:
					return True
		return False


class ServicioFases:
	"""
	Clase que organiza los servicios para fases, simplificacion de api.
	"""
	servicio_proyectos = ServicioProyectos()
	servicio_tdi = ServicioTipoDeItem()

	def get_fases(self):
		"""
		Funcion que retorna todas las fases del sistema.

		:return: Retorna todas las fases del sistema, ordenadas numericamente
		"""
		return Fase.objects.all().order_by('numero_fase')

	def get_fase(self, fase):
		"""
		Funcion que obtiene una fase en especifico.

		:param fase: La referencia a fase en cuestion, puede ser id (string o int) o el objeto en sí
		:return: el objeto fase
		"""
		if isinstance(fase, str) and fase.isdigit():
			return Fase.objects.filter(id=int(fase)).first()
		if isinstance(fase, str):
			return Fase.objects.filter(nombre=fase).first()
		elif isinstance(fase, int):
			return Fase.objects.filter(id=fase).first()
		return fase

	def get_fase_por_numero_en_proyecto(self, proyecto_ref, numero):
		"""
		Funcion que obtiene una fase de un proyecto, especificada por el numero de la fase.

		:param proyecto_ref: Referencia al proyecto
		:param numero: Identificacion numerica de la fase deseada
		:return: El objeto Fase indicado, en caso de que numero de fase no exista en el proyecto, retorna None
		"""
		for fase in self.servicio_proyectos.get_fases(proyecto_ref):
			if fase.numero_fase == numero:
				return fase
		return None

	def get_fase_previa_a(self, fase_ref):
		"""
		Funcion que obtiene la fase previa a la fase especificada.

		:param fase_ref: Referencia a la fase
		:return: Retorna el objeto fase deseado, si no se encuentra, retorna None
		"""
		fase = self.get_fase(fase_ref)
		return self.get_fase_por_numero_en_proyecto(fase.proyecto, fase.numero_fase - 1)

	def get_fase_siguiente_a(self, fase_ref):
		"""
		Funcion que obtiene la fase siguiente a la fase especificada.

		:param fase_ref: Referencia a la fase
		:return: Retorna el objeto fase deseado, si no se encuentra, retorna None
		"""
		fase = self.get_fase(fase_ref)
		return self.get_fase_por_numero_en_proyecto(fase.proyecto, fase.numero_fase + 1)

	def get_tdis(self, fase_ref, mostrar_todos=True):
		"""
		Funcion para obtener todos los tipos de items de una fase especifica, segun tengan o no atributos.

		:param fase_ref: Referencia a la fase en cuestion
		:param mostrar_todos: Filtra por cantidad de atributos si es False
		:return: Una lista de tipos de items, o una lista vacia si no hay tipos de items en la fase o si todos los tipos de items no tienen atributos
		"""
		fase = self.get_fase(fase_ref)
		tdis = self.servicio_proyectos.get_tipos_de_item(fase.proyecto)
		tdis_id = [tdi.id for tdi in tdis if tdi.fase is not None and tdi.fase.id == fase.id]
		if not mostrar_todos:
			tdis_id = [tdi_id for tdi_id in tdis_id if
			           AtributoDinamico.objects.filter(tipo_de_item_id=tdi_id).count() > 0]
		return TipoDeItem.objects.filter(id__in=tdis_id).all()

	def asignar_fase_a_tdi(self, fase_ref, tdi_ref):
		"""
		Asigna un tipo de item a una fase.

		:param fase_ref: Referencia a la fase en cuestion
		:param tdi_ref: Referencia al tipo de item en cuestion
		"""
		tdi = self.servicio_tdi.get_tipo_de_item(tdi_ref)
		fase = self.get_fase(fase_ref)
		tdi.fase = fase
		tdi.save()

	def agregar_fase_a_proyecto(self, nombre, proyecto, numero, color, icono):
		"""
		Crea una fase y la asigna a un proyecto.

		:param nombre: Identificacion de la nueva fase
		:param proyecto: Proyecto que recibira la nueva fase
		"""
		nro_fase_sig = self.servicio_proyectos.get_fases(proyecto).count() + 1
		if numero != nro_fase_sig:
			# Mover las fases siguientes una posición para para acomodar la nueva fase
			fases_proyecto = self.servicio_proyectos.get_fases(proyecto)
			for fase_proyecto in fases_proyecto:
				if fase_proyecto.numero_fase >= numero:
					fase_proyecto.numero_fase += 1
					fase_proyecto.save()
		Fase.objects.create(nombre=nombre, numero_fase=numero,
		                    proyecto=self.servicio_proyectos.get_proyecto(proyecto),
		                    color=color, icono=icono).save()

	def eliminar_fase_de_proyecto(self, fase, proyecto):
		"""
		Elimina una fase especifcada de un proyecto especificado.

		:param fase: Fase a eliminar
		:param proyecto: Proyecto al cual pertenece la fase
		"""
		fases_proyecto = self.servicio_proyectos.get_fases(proyecto)
		nro_fase_eliminar = self.get_fase(fase).numero_fase
		for fase_proyecto in fases_proyecto:
			if fase_proyecto.numero_fase > nro_fase_eliminar:
				fase_proyecto.numero_fase -= 1
				fase_proyecto.save()
		self.get_fase(fase).delete()

	def agregar_rol_de_fase_a_proyecto(self, rol, proyecto):
		"""
		Crea un rol, y lo asigna a un proyecto.

		:param rol: Identificacion del rol
		:param proyecto: El proyecto donde se agrega el rol
		"""
		Rol.objects.create(nombre=rol, ambiente='fa').save()
		rol = Rol.objects.last()
		proyecto = self.servicio_proyectos.get_proyecto(proyecto)
		ProyectoRol.objects.create(proyecto=proyecto, rol=rol).save()

	def get_roles_de_usuario_en_fase(self, usuario_ref, fase_ref):
		"""
		Funcion que obtiene los roles de fase de un usuario.

		:param usuario_ref: Referencia al usuario en cuestion
		:param fase_ref: Referencia a la fase en cuestion
		:return: Una lista de roles, en caso de que el usuario no tenga ningun rol de fase, retorna una lista vacia
		"""
		fase = self.get_fase(fase_ref)
		usuario = servicio_usuario.get_usuario(usuario_ref)
		roles_id = [ruf.rol.id for ruf in RolUsuarioFase.objects.filter(usuario=usuario, fase=fase)]
		return Rol.objects.filter(id__in=roles_id)

	def agregar_rol_de_fase_a_miembro(self, miembro_ref, rol_ref, fase_ref):
		"""
		Relaciona un rol de fase con un usuario.

		:param miembro_ref: Referencia al usuario en cuestion
		:param rol_ref: Referencia al rol en cuestion
		:param fase_ref: Referencia a la fase en cuestion
		"""
		fase = self.get_fase(fase_ref)
		miembro = servicio_usuario.get_usuario(miembro_ref)
		rol = servicio_roles.get_rol(rol_ref)
		RolUsuarioFase.objects.create(rol=rol, fase=fase, usuario=miembro)

	def remover_rol_de_usuario_en_fase(self, miembro_ref, rol_ref, fase_ref):
		"""
		Elimina un rol de fase de un usuario.

		:param miembro_ref: Referencia al usuarioe en cuestion
		:param rol_ref: Referencia al rol en cuestion
		"""
		usuario = servicio_usuario.get_usuario(miembro_ref)
		rol = servicio_roles.get_rol(rol_ref)
		fase = self.get_fase(fase_ref)
		RolUsuarioFase.objects.get(rol=rol, usuario=usuario, fase=fase).delete()

	def tiene_tipo_de_item(self, fase_ref, tdi_ref):
		"""
		Verifica si una fase especifica tiene un tipo de item especifico.

		:param fase_ref: Referencia a la fase en cuestion
		:param tdi_ref: Referencia al tipo de item en cuestion
		:return: Si la fase dada tiene asiagnada el tipo de ítem especificado o no
		"""
		tdi = self.servicio_tdi.get_tipo_de_item(tdi_ref)
		fase = self.get_fase(fase_ref)
		return tdi.fase.id == fase.id

	def get_items(self, fase_ref):
		"""
		Funcion que obtiene todos los items de una fase especifica.

		:param fase_ref: Referencia a la fase en cuestion
		:return: Una lista de items, si la fase no tiene items, retorna una lista vacia
		"""
		fase = self.get_fase(fase_ref)
		return Item.objects.filter(fase=fase).order_by('codigo')

	def usuario_tiene_permiso_en_fase(self, usuario_ref, fase_ref, permiso_ref):
		"""
		Verifica si un usuario tiene un permiso especifico en una fase especifica.

		:param usuario_ref: Referencia al usuario en cuestion
		:param fase_ref: Referencia a la fase en cuestion
		:param permiso_ref:Referencia al permiso en cuestion
		:return: True si el usuario tiene el permiso dado en la fase dada, False en caso contrario
		"""
		usuario = servicio_usuario.get_usuario(usuario_ref)
		fase = self.get_fase(fase_ref)
		permiso = servicio_permisos.get_permiso(permiso_ref)
		roles = self.get_roles_de_usuario_en_fase(usuario, fase)
		for rol in roles:
			for rol_permiso in servicio_roles.get_permisos_rol(rol):
				if rol_permiso.id == permiso.id:
					return True
		return False

	def puede_finalizar_fase(self, fase_ref):
		"""
		Define si una fase dada cumple con todas las validaciones para ser finalizada.

		:params fase_ref: fase a considerar
		:return: si la fase puede ser finalizada o no
		"""
		from lineaBase.services import ServicioLineaBase
		servicio_lb = ServicioLineaBase()
		servicio_items = ServicioItems()
		fase = self.get_fase(fase_ref)
		# obtener hojas y raices
		hojas = []
		raices = []
		for item in self.get_items(fase):
			if servicio_items.es_hoja(item):
				hojas.append(item)
			if servicio_items.es_raiz(item):
				raices.append(item)
			# todos los ítems deben estar aprobados (o desactivados)
			if not (item.estado == 'ap' or item.estado == 'da'):
				return False
			# todos los ítems aprobados deben estar en una línea base
			if item.estado == 'ap' and not servicio_lb.item_tiene_linea_base(item):
				return False
		# verificar las hojas (si no es la última fase)
		if fase.numero_fase < self.servicio_proyectos.get_fases(fase.proyecto).count():
			for hoja in hojas:
				# todas las hojas deben tener al menos un sucesor
				sucesores = servicio_items.get_relaciones(hoja, 's')
				if sucesores.count() == 0:
					return False
		# en todas las fases, a excepción de la fase 1...
		if fase.numero_fase > 1:
			# todos los ítems raiz deben tener al menos un antecesor
			for raiz in raices:
				antecesores = servicio_items.get_relaciones(raiz, 'a')
				if antecesores.count() == 0:
					return False
			# la fase anterior debe estar finalizada
			fase_anterior = self.get_fase_previa_a(fase)
			if not fase_anterior.finalizado:
				return False

		# no debe haber líneas base rotas
		lbs = servicio_lb.get_lineas_base_de_fase(fase)
		for lb in lbs:
			if lb.estado != 'ac':
				return False
		# puede finalizar
		return True

	def finalizar_fase(self, fase_ref):
		fase = self.get_fase(fase_ref)
		if self.puede_finalizar_fase(fase):
			fase.finalizado = True
			fase.save()


class ServicioItems:
	"""
	Clase que organiza los servicios para items, simplificacion de api.
	"""
	servicio_tdi = ServicioTipoDeItem()
	servicio_fase = ServicioFases()
	servicio_proyecto = ServicioProyectos()

	def get_item(self, item):
		"""
		Convierte una refrencia a un Item (id, nombre de proyecto, id en texto) a el objeto proyecto que representa.

		:param proyecto: La identificacion referencial del proyecto
		:return: el objeto proyecto que representa
		"""
		if isinstance(item, str) and item.isdigit():
			return Item.objects.filter(id=int(item)).first()
		if isinstance(item, int):
			return Item.objects.filter(id=item).first()
		return item

	def get_items_proyecto(self, proyecto_ref):
		"""
		Obtiene todos los ítems pertenecientes a un proyecto específicio.

		:param proyecto_ref: La referencial del proyecto
		:return: una lista de ítems
		"""
		proyecto = self.servicio_proyecto.get_proyecto(proyecto_ref)
		fases = self.servicio_proyecto.get_fases(proyecto)
		items = []
		for fase in fases:
			fase_items = self.servicio_fase.get_items(fase)
			items.extend([fi for fi in fase_items])
		return items

	def get_historial(self, item_ref):
		"""
		Obtiene una lista con el historial de cambios de un ítem dado,

		:params item_ref: ítem a considerar
		:return: lista de historial de cambios del ítem
		"""
		item = self.get_item(item_ref)
		return ItemHistorial.objects.filter(item=item)

	def modificar_item(self, item_ref, nombre, descripcion, costo, estado, atributos_pair, usuario_ref):
		"""
		Mofica los atributos de un ítem dado.

		:params item_ref: ítem que será modificado
		:params nombre: nuevo nombre del ítem
		:params descripción: nueva descripción del ítem
		:params costo: nuevo costo del ítem
		:params estado: nuevo estado del ítem
		:params atrobutos_pair: lista de atributos dinámicos del ítem con sus respectivas modificaciones
		:params usuario_ref: usuario que realizó la modificación del ítem

		"""
		item = self.get_item(item_ref)
		usuario = servicio_usuario.get_usuario(usuario_ref)
		mod_descripcion = "El usuario " + usuario.username + " ha modificado el ítem " + str(
			item.prefijo).upper() + " " + str(item.codigo) + ":\n"
		desc_contador = 0
		modificado = False
		if nombre is not None and not item.nombre == nombre:
			desc_contador += 1
			mod_descripcion += str(desc_contador) + ". el nombre: de \"" + item.nombre + "\" a \"" + nombre + "\".\n"
			item.nombre = nombre
			modificado = True
		if descripcion is not None and not item.descripcion == descripcion:
			desc_contador += 1
			mod_descripcion += str(
				desc_contador) + ". la descripción: de \"" + item.descripcion + "\" a \"" + descripcion + "\".\n"
			item.descripcion = descripcion
			modificado = True
		if costo is not None and not str(item.costo) == str(costo):
			desc_contador += 1
			mod_descripcion += str(desc_contador) + ". el costo: de \"" + str(item.costo) + "\" a \"" + str(
				costo) + "\".\n"
			item.costo = costo
			modificado = True
		if estado is not None and not item.estado == estado:
			item.estado = estado
		item.save()
		# Modificar atributos
		if atributos_pair is not None:
			for pair in atributos_pair:
				atributo = self.servicio_tdi.get_atributo_de_tdi(pair[0])
				item_atributo = ItemAtributo.objects.filter(item=item, atributo=atributo).first()
				if not item_atributo.valor == pair[1]:
					desc_contador += 1
					mod_descripcion += str(desc_contador) + ". el atributo \"" + item_atributo.atributo.titulo + \
					                   "\": de \"" + item_atributo.valor + "\" a \"" + pair[1] + "\".\n"
					item_atributo.valor = pair[1]
					item_atributo.save()
					modificado = True
		# Cambiar estado a pendiente si estaba aprobado y se modificó
		if modificado:
			if item.estado == 'er' or item.estado == 'co':
				# Guardar estado antes de pasar al estado "Pendiente de versionar"
				version_actual = self.get_version_actual_item(item)
				version_actual.estado_anterior = item.estado
				version_actual.save()
			# Cambiar al estado "Pendiente de versionar"
			item.estado = 'pv'
			item.save()
			# Crear historial de ítem
			ItemHistorial.objects.create(item=item, descripcion=mod_descripcion)

	def get_item_por_codigo(self, codigo, proyecto_ref):
		"""
		Funcion que obtiene un item, a traves del codigo especificado.

		:param codigo: El codigo del item deseado
		:param proyecto_ref: Referencia al proyecto en cuestion
		:return: Un objeto item, en caso de que el item no exista, retorna None
		"""
		fases_querys = self.servicio_proyecto.get_fases(proyecto_ref)
		fases = [fase for fase in fases_querys]
		return Item.objects.filter(fase__in=fases, codigo=codigo)

	def get_atributos_dinamicos(self, item_ref):
		"""
		Obtiene todos los atributos dinamicos de un item.

		:param item_ref: Referencia al item en cuestion
		:return: Una lista de atributos
		"""
		item = self.get_item(item_ref)
		return ItemAtributo.objects.filter(item=item)

	def es_hoja(self, item_ref):
		"""
		Define si un ítem dado no tiene hijos.

		:params item_ref: ítem a considerar
		:return: true si no tiene hijos, false caso contrario
		"""
		item = self.get_item(item_ref)
		hijos = self.get_relaciones(item, 'h')
		return hijos.count() == 0

	def es_raiz(self, item_ref):
		"""
		Define si un ítem dado no tiene padres.

		:params item_ref: ítem a considerar
		:return: true si no tiene padres, false caso contrario
		"""
		item = self.get_item(item_ref)
		padres = self.get_relaciones(item, 'p')
		return padres.count() == 0

	def agregar_item(self, nombre, prefijo, codigo, tipo_de_item_ref, fase_ref, descripcion, costo, atributos_pair, version):
		"""
		Crea un nuevo item y le asigna todos sus atributos genericos e inicializa sus atributos dinamicos (del tipo de item)

		:param nombre: Identificacion del nuevo item
		:param prefijo: Prefijo del nuevo item
		:param codigo: Codigo del nuevo item
		:param tipo_de_item_ref: Referencia al tipo de item al cual pertenecera
		:param fase_ref: Referencia a la fase a la cual pertenecera
		:param descripcion: Descripcion del nuevo item
		:param costo: Costo del nuevo item
		"""
		fase = self.servicio_fase.get_fase(fase_ref)
		tipo_de_item = self.servicio_tdi.get_tipo_de_item(tipo_de_item_ref)
		items = self.get_item_por_codigo(codigo, fase.proyecto)
		if items.count() == 0:
			item = Item.objects.create(nombre=nombre, prefijo=prefijo, codigo=codigo, tipo_de_item=tipo_de_item,
			                           fase=fase, descripcion=descripcion, costo=costo, estado="pe")
			item.save()
			# inicializar atributos del item
			for pair in atributos_pair:
				atributo = self.servicio_tdi.get_atributo_de_tdi(pair[0])
				ItemAtributo.objects.create(item=Item.objects.last(), atributo=atributo, valor=pair[1])
			# Actualizar último rf o rnf de proyecto
			if prefijo == 'rf':
				tipo_de_item.proyecto.ultimo_rf += 1
				tipo_de_item.proyecto.save()
			elif prefijo == 'rnf':
				tipo_de_item.proyecto.ultimo_rnf += 1
				tipo_de_item.proyecto.save()
			# Crear versión
			self.crear_nueva_version_de_item(item, version)
			return True
		return False

	def agregar_valor_a_atributo(self, item_ref, atributo_ref, valor):
		"""
		Asigna un valor a un atributo dinamico de un item.

		:param item_ref: Referencia al item en cuestion
		:param atributo_ref: Referencia al atributo en cuestion
		:param valor: El nuevo valor del atributo
		"""
		item = self.get_item(item_ref)
		atributo = self.servicio_tdi.get_atributo_de_tdi(atributo_ref)
		item_atributo = ItemAtributo.objects.filter(item=item, atributo=atributo).first()
		item_atributo.valor = valor
		item_atributo.save()

	def agregar_relacion(self, item_ref, item_relacion_ref, relacion_tipo):
		"""
		Crea una nueva relacion entre items.

		:param item_ref: Referencia al item en cuestion
		:param item_relacion_ref: Referencia al item al cual se relacionara
		:param relacion_tipo: Especificacion del tipo de relacion entre los items
		"""
		item = self.get_item(item_ref)
		item_relacion = self.get_item(item_relacion_ref)
		if relacion_tipo == 'p' or relacion_tipo == 's':
			ItemRelacion.objects.create(item=item, item_relacion=item_relacion, relacion_tipo=relacion_tipo)
		elif relacion_tipo == 'h':
			ItemRelacion.objects.create(item=item_relacion, item_relacion=item, relacion_tipo='p')
		elif relacion_tipo == 'a':
			ItemRelacion.objects.create(item=item_relacion, item_relacion=item, relacion_tipo='s')
		# Cambiar estado de ítem
		self.cambiar_estado_item_a_pv(item)
		# Cambiar estado de ítem relación
		self.cambiar_estado_item_a_pv(item_relacion)

	def eliminar_relacion(self, item_ref, item_relacion_ref):
		"""
		Elimina una relacion entre items.

		:param item_ref: Referencia al item en cuestion
		:param item_relacion_ref: Referencia al item relacionado
		"""
		item = self.get_item(item_ref)
		item_relacion = self.get_item(item_relacion_ref)
		ItemRelacion.objects.filter(item=item, item_relacion=item_relacion).delete()
		ItemRelacion.objects.filter(item=item_relacion, item_relacion=item).delete()
		# Cambiar estado de ítem
		self.cambiar_estado_item_a_pv(item)
		# Cambiar estado de ítem relación
		self.cambiar_estado_item_a_pv(item_relacion)

	def get_relaciones(self, item_ref, relacion_tipo):
		"""
		Obtiene todas las relaciones de un item en especifico de un tipo especifico.

		:params item_ref: Referencia al item en cuestion
		:params relacion_tipo: El tipo de relacion deseada (h/p/s/a)
		:return: Una lista de items, o una lista vacia si no existen relaciones
		"""
		item = self.get_item(item_ref)
		if relacion_tipo == 'p' or relacion_tipo == 's':
			return ItemRelacion.objects.filter(item=item, relacion_tipo=relacion_tipo)
		elif relacion_tipo == 'h':
			return ItemRelacion.objects.filter(item_relacion=item, relacion_tipo='p')
		elif relacion_tipo == 'a':
			return ItemRelacion.objects.filter(item_relacion=item, relacion_tipo='s')

	def get_todas_las_relaciones(self, item_ref):
		"""
		Obtiene todas las relaciones de un item en especifico.

		:params item_ref: Referencia al item en cuestion
		:return: Una lista de items, o una lista vacia si no existen relaciones
		"""
		item = self.get_item(item_ref)
		relaciones = []
		relaciones += [e for e in self.get_relaciones(item, 'h')]
		relaciones += [e for e in self.get_relaciones(item, 'p')]
		relaciones += [e for e in self.get_relaciones(item, 'a')]
		relaciones += [e for e in self.get_relaciones(item, 's')]
		return relaciones

	def get_hijos_posibles(self, item_ref):
		"""
		Funcion para obtener todos los items que pueden ser hijos de un item en especifico, esto es, sin causar relaciones circulares.

		:param item_ref: Referencia al item en cuestion
		:return: Una lista de items, o una lista vacia si no hay hijos posibles
		"""
		from lineaBase.services import ServicioLineaBase
		servicio_lb = ServicioLineaBase()
		item = self.get_item(item_ref)
		items_fase = self.servicio_fase.get_items(item.fase)
		padres_rec = self.get_padres_rec(item)
		padres_rec_id = [pr.item_relacion.id for pr in padres_rec]
		hijos_id = [h.item_relacion.id if h.item == item else h.item.id for h in self.get_relaciones(item, 'h')]
		items_en_lb_id = [i.id for i in servicio_lb.get_items_en_lb_en_fase(item.fase)]
		return items_fase.exclude(id__in=padres_rec_id).exclude(id=item.id).exclude(id__in=hijos_id).exclude(
			id__in=items_en_lb_id)

	def get_sucesores_posibles(self, item_ref):
		"""
		Funcion para obtener todos los items que pueden ser sucesores de un item en especifico, esto es, sin causar relaciones circulares.

		:param item_ref: Referencia al item en cuestion
		:return: Una lista de items, o una lista vacia si no hay sucesores posibles
		"""
		item = self.get_item(item_ref)
		siguiente_fase = self.servicio_fase.get_fase_siguiente_a(item.fase)
		no_sucesores = [s.item_relacion if s.item == item else s.item for s in self.get_relaciones(item, 's')]
		sucesores = self.servicio_fase.get_items(siguiente_fase)
		sucesores_filter = [s for s in sucesores if (s.estado == 'pe' or s.estado == 'ap') and s not in no_sucesores]
		return sucesores_filter

	def get_padres_rec(self, item):
		"""
		Obtiene una lista con los padres sucesivos de un ítem.

		:params item: ítem a considerar
		:return: lista de ítems padres en todos los niveles
		"""
		return self.get_elementos_relacionados_rec(self.get_relaciones(item, 'p'), 'p')

	def get_hijos_rec(self, item):
		"""
		Obtiene una lista con los hijo sucesivos de un ítem.

		:params item: ítem a considerar
		:return: lista de ítems hijos en todos los niveles
		"""
		return self.get_elementos_relacionados_rec(self.get_relaciones(item, 'h'), 'h', True)

	def get_antecesores_rec(self, item):
		"""
		Obtiene una lista con los antecesores sucesivos de un ítem.

		:params item: ítem a considerar
		:return: lista de ítems antecesores en todos los niveles
		"""
		return self.get_elementos_relacionados_rec(self.get_relaciones(item, 'a'), 'a', True)

	def get_sucesores_rec(self, item):
		"""
		Obtiene una lista con los sucesores sucesivos de un ítem.

		:params item: ítem a considerar
		:return: lista de ítems sucesores en todos los niveles
		"""
		return self.get_elementos_relacionados_rec(self.get_relaciones(item, 's'), 's')

	def get_elementos_relacionados_rec(self, elementos_origen, relacion, invertir_ralacion=False):
		"""
		Funcion para obtener todos los elementos de todos los niveles en el arbol de un item en especifico según el tipo de relación.

		:param elementos_origen: Lista de elementos del primer nivel del item deseado
		:param relacion: Tipo de relación buscada: p: padres, h: hijos, a: antecesores, s: sucesores
		:param invertir_ralacion: Define si el ítem a relacionar es el relacionando (item) o la relación (item_relacion)
		:return: Una lista de items, o una lista vacia si no hay elementos relacionados
		"""
		elementos = [e for e in elementos_origen]
		for elemento_origen in elementos_origen:
			item_relacionado = elemento_origen.item_relacion if not invertir_ralacion else elemento_origen.item
			elementos += self.get_elementos_relacionados_rec(self.get_relaciones(item_relacionado, relacion), relacion,
			                                                 invertir_ralacion)
		return elementos

	def get_ultimo_prefijo(self, prefijo, proyecto_ref, incrementar=False):
		"""
		Funcion que administra la generacion automatica de codigos de items.

		:param prefijo: Prefijo del item
		:param proyecto_ref: Referencia al proyecto en cuestion
		:param incrementar: Indicador de aumento o decremento de codigo, por defecto False
		:return: El codigo generado, si se produjo un error en la generacion, retorna -1
		"""
		proyecto = self.servicio_proyecto.get_proyecto(proyecto_ref)
		if prefijo == "rf" or prefijo == "RF":
			if incrementar:
				proyecto.ultimo_rf += 1
				proyecto.save()
			return proyecto.ultimo_rf
		elif prefijo == "rnf" or prefijo == "RNF":
			if incrementar:
				proyecto.ultimo_rnf += 1
				proyecto.save()
			return proyecto.ultimo_rnf
		return -1

	def crear_nueva_version_de_item(self, item_ref, version):
		"""
		Funcion para versionar un item e implementacion de la navegacion entre versiones.

		:param item_ref: Referencia al item
		:param version: Nueva version del item
		:return:
		"""
		item = self.get_item(item_ref)
		version_anterior = self.get_version_actual_item(item)
		item.estado = 'pe'
		item.save()
		# Nueva versión de ítem
		item_version = ItemVersion.objects.create(item=item, nombre=item.nombre, descripcion=item.descripcion,
		                                          costo=item.costo, version=version)
		# Nueva versión de atributos de ítem
		for atributo in self.get_atributos_dinamicos(item):
			ItemAtributoVersion.objects.create(item_version=item_version, item_atributo=atributo, valor=atributo.valor)
		# Nueva versión de relaciones de ítem
		for relacion in self.get_todas_las_relaciones(item):
			ItemRelacionVersion.objects.create(item_version=item_version, item=relacion.item,
			                                   item_relacion=relacion.item_relacion,
			                                   relacion_tipo=relacion.relacion_tipo)
		# Volver a estado anterior
		if version_anterior is not None and version_anterior.estado_anterior is not None:
			item.estado = version_anterior.estado_anterior
			item.save()
		# Si tiene sucesores, debe estar, al menos, aprobado
		elif self.get_relaciones(item, 's').count() > 0:
			item.estado = 'ap'
			item.save()

	def get_version_actual_item(self, item_ref):
		"""
		Obtiene la version actual de un item.

		:param item_ref: Referencia al item
		:return: La version del item
		"""
		return self.get_item_versiones(item_ref).last()

	def get_item_versiones(self, item_ref):
		"""
		Obtiene todas las versiones de un item.

		:param item_ref: Referencia al item
		:return:
		"""
		item = self.get_item(item_ref)
		return ItemVersion.objects.filter(item=item)

	def get_item_version(self, item_version):
		"""
		Obtiene el objeto version solicitado.

		:param item_version: referencia a la versión del ítem (puede ser su id o el objeto en sí)
		:return: el objeto de la versión del ítem en sí
		"""
		if isinstance(item_version, str) and item_version.isdigit():
			return ItemVersion.objects.get(id=int(item_version))
		if isinstance(item_version, int):
			return ItemVersion.objects.get(id=item_version)
		return item_version

	def cambiar_a_version(self, item_ref, version_id, usuario_ref):
		"""
		Funcion para aplicar los cambios entre versiones de un item.

		:param item_ref: Referencia al item
		:param version_id: Id de la version objetivo
		:return:
		"""
		item = self.get_item(item_ref)
		item_version = ItemVersion.objects.get(id=version_id)
		usuario = servicio_usuario.get_usuario(usuario_ref)
		atributos_dinamicos_de_version = self.get_atributos_dinamicos_de_version(item_version)
		atributos = [[atr_ver.item_atributo.atributo.id, atr_ver.valor] for atr_ver in atributos_dinamicos_de_version]
		# Modificar datos base
		self.modificar_item(item, item_version.nombre, item_version.descripcion, item_version.costo, None, atributos,
		                    usuario)

		# Modificar atributos dinámicos
		# for atrib_dv in atributos_dinamicos_de_version:
		#    atrib_dv.item_atributo.valor = atrib_dv.valor
		#    atrib_dv.item_atributo.save()

		# Modificar relaciones
		# Eliminar relaciones actuales
		for relacion in self.get_todas_las_relaciones(item):
			self.eliminar_relacion(relacion.item, relacion.item_relacion)
		# Agregar las relaciones de la versión anterior
		for relacion_version in ItemRelacionVersion.objects.filter(item_version=item_version):
			self.agregar_relacion(relacion_version.item, relacion_version.item_relacion, relacion_version.relacion_tipo)

		# Nueva versión (si no es la última versión)
		if item_version.id != ItemVersion.objects.filter(item=item).last().id:
			self.crear_nueva_version_de_item(item, item_version.version)
		# Deshacer cambios
		elif item_version.estado_anterior is not None and not item_version.estado_anterior == 'pv':
			item.estado = item_version.estado_anterior
			item.save()
		else:
			item.estado = 'pe'
			item.save()

	def deshacer_cambios(self, item_ref, usuario_ref):
		"""
		Funcion para deshacer cambios reciente hechos a un item.

		:param item_ref: Referencia al item
		:params usuario_ref: Usuario que realizó la operación de deshacer cambios
		"""
		version_actual = self.get_version_actual_item(item_ref)
		self.cambiar_a_version(item_ref, version_actual.id, usuario_ref)

	def get_atributos_dinamicos_de_version(self, item_version_ref):
		"""
		Funcion para obtener los datos de atributos dinamicos de una version especifica de un item.

		:param item_version_ref: Referencia a la version de item
		:return: Una lista de atributos de version, si no hay atributos, una lista vacia.
		"""
		item_version = self.get_item_version(item_version_ref)
		return ItemAtributoVersion.objects.filter(item_version=item_version)

	def cambiar_estado_item_a_pv(self, item_ref):
		"""
		Cambia el estado de un item a Pendiente de version.

		:param item_ref: Referencia al item
		"""
		item = self.get_item(item_ref)
		item_version = self.get_version_actual_item(item)
		if item.estado == 'er' or item.estado == 'co':
			item_version.estado_anterior = item.estado
			item_version.save()
		item.estado = "pv"
		item.save()

	def get_trazo(self, item_ref, fase_ref):
		"""
		Calcula la trazabilidad y obtiene los items de la traza de un item especifico.

		:param item_ref: Referencia al item en especifico
		:param fase_ref: Referencia a la fase del item
		:return: Una lista de items que componen la traza del item, en caso de el item no tenga traza, retorna el item.
		"""
		fase = self.servicio_fase.get_fase(fase_ref)
		item = self.get_item(item_ref)

		hijos = [i.item if i.item_relacion.id == item.id else i.item_relacion for i in self.get_hijos_rec(item)]
		sucesores = [i.item if i.item_relacion.id == item.id else i.item_relacion for i in self.get_sucesores_rec(item)]
		# sucesores_de_fase = [i for i in self.servicio_fase.get_items(fase) if i in sucesores]
		hijos_de_sucesores_de_fase = []
		for sucesor_de_fase in sucesores:
			hijos_de_sucesores_de_fase += [i.item if i.item_relacion.id == sucesor_de_fase.id else i.item_relacion for i
			                               in self.get_hijos_rec(sucesor_de_fase)]
		return hijos + [item] + sucesores + hijos_de_sucesores_de_fase

	def get_trazo_costo_total(self, item_ref):
		"""
		Funcion para obtener el costo total de trazabilidad de un item en especifico.

		:param item_ref: Referencia al item en cuestion
		:return: retorna el costo total del item
		"""
		item = self.get_item(item_ref)
		hijos = [i.item if i.item_relacion.id == item.id else i.item_relacion for i in self.get_hijos_rec(item)]
		sucesores = [i.item if i.item_relacion.id == item.id else i.item_relacion for i in self.get_sucesores_rec(item)]
		hijos_de_sucesores = []
		for sucesor in sucesores:
			hijos_de_sucesores += [i.item if i.item_relacion.id == sucesor.id else i.item_relacion for i in
			                       self.get_hijos_rec(sucesor)]
		costo_total = sum(map(lambda i: i.costo, hijos + sucesores + hijos_de_sucesores + [item]))
		costo_proyecto = self.get_costo_de_proyecto(item.fase.proyecto)
		costo_total_porc = costo_total * 100 / costo_proyecto
		return str(costo_total) + '/' + str(costo_proyecto) + ' | ' + str(round(costo_total_porc, 2)) + '%'

	def get_trazo_costo_fase(self, item_ref, fase_ref):
		"""
		Calcula el costo de trazabilidad de un item especifico en una fase especifica.

		:param item_ref: Referencia al item a trazar
		:param fase_ref: La fase a visualizar en la traza
		:return: El costo de la trazabilidad del item en la fase
		"""
		item = self.get_item(item_ref)
		fase = self.servicio_fase.get_fase(fase_ref)
		if item.fase.numero_fase == fase.numero_fase:
			hijos = [i.item if i.item_relacion.id == item.id else i.item_relacion for i in self.get_hijos_rec(item)]
			return sum(map(lambda i: i.costo, hijos + [item])) / self.get_costo_de_proyecto(item.fase.proyecto) * 100
		elif fase.numero_fase > item.fase.numero_fase:
			sucesores = [i.item if i.item_relacion.id == item.id else i.item_relacion for i in
			             self.get_sucesores_rec(item)]
			sucesores_de_fase = [i for i in self.servicio_fase.get_items(fase) if i in sucesores]
			hijos_de_sucesores_de_fase = []
			for sucesor_de_fase in sucesores_de_fase:
				hijos_de_sucesores_de_fase += [i.item if i.item_relacion.id == sucesor_de_fase.id else i.item_relacion
				                               for i in self.get_hijos_rec(sucesor_de_fase)]
			return sum(
				map(lambda i: i.costo, sucesores_de_fase + hijos_de_sucesores_de_fase)) / self.get_costo_de_proyecto(
				item.fase.proyecto) * 100
		return 0

	def get_costo_de_proyecto(self, proyecto_ref):
		"""
		Calcula el costo del proyecto.

		:param proyecto_ref: Referencia al proyecto
		:return: El costo total del proyecto
		"""
		proyecto = self.servicio_proyecto.get_proyecto(proyecto_ref)
		fases = self.servicio_proyecto.get_fases(proyecto)
		costo = 0
		for fase in fases:
			items = self.servicio_fase.get_items(fase)
			costo += sum(map(lambda i: i.costo, items))
		return costo

	def subir_adjunto(self, nombre, item_ref):
		"""
		Funcion para subir el archivo adjunto de un item.

		:param nombre: Nombre del archivo adjunto
		:param item_ref: Referencia al item a recibir el archivo adjunto
		"""
		item = self.get_item(item_ref)
		item.nombre_adjunto = nombre
		item.save()

	def remover_adjunto(self, item_ref):
		"""
		Funcion para desadjuntar el archivo adjunto de un item.

		:param item_ref: Referencia al item en cuestion
		"""
		item = self.get_item(item_ref)
		item.nombre_adjunto = None
		item.save()
