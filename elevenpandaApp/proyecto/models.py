from datetime import datetime

from django.db import models
from Usuarios.models import Usuario
from roles.models import Rol, RolUsuario
import dropbox


def decima_to_hexadecimal(n):
    """
    Gerena un código hexadecimal a partir de un número decimal

    :param n: número decimal
    :return: número hexadecimal
    """
    if n < 0:
        return '0'
    if n < 10:
        return str(n)
    if n == 10:
        return 'A'
    if n == 11:
        return 'B'
    if n == 12:
        return 'C'
    if n == 13:
        return 'D'
    if n == 14:
        return 'E'
    if n == 15:
        return 'F'


class Proyecto(models.Model):
    """
    Clase que implementa el modelo de proyecto
    """
    nombre = models.CharField(max_length=50)
    estado = models.CharField(max_length=2, default='pe')
    descripcion = models.TextField(blank=True, default='Sin descripción')
    gerente = models.ForeignKey(Usuario, on_delete=models.CASCADE, related_name='%(class)s_gerente', null=True, blank=True)
    miembros = models.ManyToManyField(Usuario, through='ProyectoUsuario', related_name='miembros')
    roles = models.ManyToManyField(Rol, through="ProyectoRol")
    # TODO: analizar si este atributo seria automatico
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_finalizacion = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    color_icono = models.CharField(max_length=10, null=True, blank=True)
    ultimo_rf = models.IntegerField(default=1)
    ultimo_rnf = models.IntegerField(default=1)

    def save(self, *args, **kwargs):
        if self.color_icono is None or not self.color_icono:
            self.color_icono = self.get_random_color()
        super(Proyecto, self).save(*args, **kwargs)

    def get_random_color(self):
        import random
        r_color = random.randint(4, 12)
        g_color = random.randint(4, 12)
        b_color = random.randint(4, 12)
        return '#' + decima_to_hexadecimal(r_color) + decima_to_hexadecimal(g_color) + decima_to_hexadecimal(b_color)


class Fase(models.Model):
    """
    Clase que implementa el modelo de fase

    """
    nombre = models.CharField(max_length=50)
    proyecto = models.ForeignKey(Proyecto, on_delete=models.CASCADE)
    usuarios = models.ManyToManyField(Usuario, through='RolUsuarioFase')
    numero_fase = models.IntegerField()
    color = models.CharField(max_length=10, default='#D9AB62')
    icono = models.CharField(max_length=60, default='fas fa-folder')
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    finalizado = models.BooleanField(default=False)
    fecha_finalizacion = models.DateTimeField(null=True)


class UsuarioProyecto(models.Model):
    """
    Clase intermedia que relaciona Usuarios con Proyectos

    """
    usuario = models.ForeignKey(Usuario, on_delete=models.CASCADE)
    proyecto = models.ForeignKey(Proyecto, on_delete=models.CASCADE)
    fecha_admision = models.DateTimeField(auto_now_add=True)


class TipoDeItem(models.Model):
    """
    Clase que implementa el modelo de Tipo de Item

    """
    nombre = models.CharField(max_length=50)
    proyecto = models.ForeignKey(Proyecto, on_delete=models.CASCADE)
    fase = models.ForeignKey(Fase, on_delete=models.CASCADE, null=True)


class AtributoDinamico(models.Model):
    """
    Clase que implementa el modelo de atributo dinamico personalizado de cada tipo de item

    """
    tipo_de_item = models.ForeignKey(TipoDeItem, on_delete=models.CASCADE)
    titulo = models.CharField(max_length=50)
    es_obligatorio = models.BooleanField(default=False)
    tipo = models.CharField(max_length=4)
    min = models.IntegerField()
    max = models.IntegerField()
    defecto = models.CharField(max_length=10, blank=True)


class Item(models.Model):
    """
        Clase que implementa el modelo de Item
    """
    
    ESTADOS_DE_ITEM = [
        ('pe', 'Pendiente'),
        #('pa', 'Pendiente de Aprobacion'),
        ('ap', 'Aprobado'),
        ('da', 'Desactivado'),
        ('er', 'En Revision'),
        ('co', 'Comprometido'),
        {'pv', 'Pendiente de version'},
    ]
    nombre = models.TextField(default="sin nombre")
    prefijo = models.CharField(max_length=3, default="RF")
    codigo = models.IntegerField(default=-1)
    tipo_de_item = models.ForeignKey(TipoDeItem, on_delete=models.CASCADE)
    fase = models.ForeignKey(Fase, on_delete=models.CASCADE)
    estado = models.CharField(max_length=2)
    descripcion = models.TextField(blank=True)
    costo = models.IntegerField(default=0)
    nombre_adjunto = models.TextField(null=True)
    #archivo_adjunto = models.FileField(default=None, upload_to='Apps/ElevenpandaBox', storage=dropbox)


class ItemHistorial(models.Model):
    item = models.ForeignKey(Item, on_delete=models.CASCADE)
    descripcion = models.TextField(blank=True)
    fecha_historia = models.DateTimeField(auto_now_add=True, null=True)


class ItemVersion(models.Model):
    item = models.ForeignKey(Item, on_delete=models.CASCADE)
    nombre = models.TextField(default="sin nombre")
    descripcion = models.TextField(blank=True)
    costo = models.IntegerField(default=0)
    version = models.TextField(default="1.0")
    fecha_version = models.DateTimeField(auto_now_add=True, null=True)
    estado_anterior = models.CharField(max_length=2, null=True, blank=True)


class ItemAtributo(models.Model):
    """

    """
    item = models.ForeignKey(Item, on_delete=models.CASCADE)
    atributo = models.ForeignKey(AtributoDinamico, on_delete=models.CASCADE)
    valor = models.TextField(null=True, blank=True)


class ItemAtributoVersion(models.Model):
    item_version = models.ForeignKey(ItemVersion, on_delete=models.CASCADE, null=True)
    item_atributo = models.ForeignKey(ItemAtributo, on_delete=models.CASCADE)
    valor = models.TextField(null=True, blank=True)


class ItemRelacion(models.Model):
    """

    """
    item = models.ForeignKey(Item, on_delete=models.CASCADE, related_name="item")
    item_relacion = models.ForeignKey(Item, on_delete=models.CASCADE, related_name="item_relacion")
    relacion_tipo = models.CharField(max_length=1)


class ItemRelacionVersion(models.Model):
    item_version = models.ForeignKey(ItemVersion, on_delete=models.CASCADE, related_name="irv_item_version", null=True)
    # item_relacion_original = models.ForeignKey(ItemRelacion, on_delete=models.CASCADE, related_name="irv_item_relacion_original", null=True)
    item = models.ForeignKey(Item, on_delete=models.CASCADE, related_name="irv_item", null=True)
    item_relacion = models.ForeignKey(Item, on_delete=models.CASCADE, related_name="irv_item_relacion", null=True)
    relacion_tipo = models.CharField(max_length=1, default='')


class ProyectoRol(models.Model):
    """
    Clase Intermedia que relaciona proyectos con roles
    """
    rol = models.ForeignKey(Rol, on_delete=models.CASCADE)
    proyecto = models.ForeignKey(Proyecto, on_delete=models.CASCADE)
    usuarios = models.ManyToManyField(Usuario, through='UsuarioRolProyecto')

    def save(self, *args, **kwargs):
        no_existe = ProyectoRol.objects.filter(proyecto=self.proyecto, rol=self.rol).first() is None
        if no_existe:
            super(ProyectoRol, self).save(*args, **kwargs)


class UsuarioRolProyecto(models.Model):
    """
    Clase Intermedia que relaciona Usuarios con roles y proyectos
    """
    usuario = models.ForeignKey(Usuario, on_delete=models.CASCADE)
    rol_proyecto = models.ForeignKey(ProyectoRol, on_delete=models.CASCADE)

    def save(self, *args, **kwargs):
        no_existe = UsuarioRolProyecto.objects.filter(usuario=self.usuario, rol_proyecto=self.rol_proyecto).first() is None
        if no_existe:
            super(UsuarioRolProyecto, self).save(*args, **kwargs)


class ProyectoUsuario(models.Model):
    """
    Clase Intermedia que relaciona proyectos con usuarios
    """
    usuario = models.ForeignKey(Usuario, on_delete=models.CASCADE)
    proyecto = models.ForeignKey(Proyecto, on_delete=models.CASCADE)
    es_valido = models.BooleanField(default=True)
    fecha_union = models.DateTimeField(auto_now_add=True)


class RolUsuarioFase(models.Model):
    """
    Clase intermedia que relaciona rol, usuario y fase
    """
    rol = models.ForeignKey(Rol, on_delete=models.CASCADE)
    fase = models.ForeignKey(Fase, on_delete=models.CASCADE)
    usuario = models.ForeignKey(Usuario, on_delete=models.CASCADE)


class UsuarioDeComiteDeCambios(models.Model):
    usuario = models.ForeignKey(Usuario, on_delete=models.CASCADE)
    proyecto = models.ForeignKey(Proyecto, on_delete=models.CASCADE)
