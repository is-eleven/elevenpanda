from .services import ServicioProyectos, ServicioFases

servicio_proyectos = ServicioProyectos()
servicio_fases = ServicioFases()


def get_url_prameter_int(request, param):
    """
    Convierte un parámetro pasado por POST a numérico
    :param request: request
    :param param: nombre del paramentro
    :return: parametro en numérico
    """
    paramStr = request.GET.get(param)
    if paramStr != None and paramStr.isdigit():
        return int(request.GET.get(param))
    return None


def tdis_context(request, origen, tipo_origen='pr'):
    ordenar_por = request.GET.get('ordenarPor')
    mostrar_todos = request.GET.get('todos') == 'True'
    agregar_atributo_a_tdi_con_id = get_url_prameter_int(request, 'agregar_atributo')
    if tipo_origen == 'pr':
        todos_los_tdis = servicio_proyectos.get_tipos_de_item(origen)
        tdis_con_atribs = servicio_proyectos.get_tipos_de_item(origen, False)
        agregar_atributo = request.GET.get('agregar_atributo') is not None and \
                           servicio_proyectos.tiene_tipo_de_item_con_nombre(origen, agregar_atributo_a_tdi_con_id)
    else:
        todos_los_tdis = servicio_fases.get_tdis(origen)
        tdis_con_atribs = servicio_fases.get_tdis(origen, False)
        agregar_atributo = request.GET.get('agregar_atributo') is not None and \
                           servicio_fases.tiene_tipo_de_item(origen, agregar_atributo_a_tdi_con_id)
    tdis = todos_los_tdis if mostrar_todos else tdis_con_atribs

    base_context = {
        'tdis': tdis,
        'cantidad_tdi_con_atibutos': len(tdis_con_atribs),
        'cantidad_tdi_totales': len(todos_los_tdis),
    }
    if agregar_atributo:
        extra_context = {
            'agregar_atributoa_a_tdi': True,
            'tdi_agregando_atribu': agregar_atributo_a_tdi_con_id
        }
        base_context = {**base_context, **extra_context}
    return base_context
