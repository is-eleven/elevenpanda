from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.template.defaultfilters import register
import datetime
import pytz

from Usuarios.decorators import cuenta_aprobada_requerida
from Usuarios.services import ServicioUsuario
from roles.decorators import permiso_requerido
from roles.services import ServicioPermisos, ServicioRoles
from .models import Proyecto
from .services import ServicioProyectos, ServicioTipoDeItem, ServicioFases, ServicioItems
from .contexts import tdis_context
from lineaBase.services import ServicioLineaBase

servicio_proyectos = ServicioProyectos()
servicio_tdi = ServicioTipoDeItem()
servicio_fases = ServicioFases()
servicio_usuarios = ServicioUsuario()
servicio_permisos = ServicioPermisos()
servicio_roles = ServicioRoles()
servicio_items = ServicioItems()
servicio_linea_base = ServicioLineaBase()

def get_url_prameter_int(request, param):
    """
    Convierte un parámetro pasado por POST a numérico
    :param request: request
    :param param: nombre del paramentro
    :return: parametro en numérico
    """
    paramStr = request.GET.get(param)
    if paramStr != None and paramStr.isdigit():
        return int(request.GET.get(param))
    return None

def agregar_parametros_a_url(url, params):
    import urllib.parse as urlparse
    from urllib.parse import urlencode
    url_parts = list(urlparse.urlparse(url))
    query = dict(urlparse.parse_qsl(url_parts[4]))
    query.update(params)
    url_parts[4] = urlencode(query)
    return urlparse.urlunparse(url_parts)

@login_required
@cuenta_aprobada_requerida
def proyectos_lista_view(request):
    """
    :param request:
    :return: view con la lista de proyectos del sistema
    """
    if request.GET.get('ordenarPor'):
        ordenar_param = request.GET.get('ordenarPor')
        if ordenar_param == 'fecha_creacion':
            ordenar_param = '-' + ordenar_param
        lista_proyectos = Proyecto.objects.order_by(ordenar_param).all()
    else:
        lista_proyectos = Proyecto.objects.all()
    extra_context = {
        'listaProyectos': lista_proyectos,
    }
    return render(request, 'proyecto/listaProyectos.html', extra_context)


@login_required
@cuenta_aprobada_requerida
def proyecto_view(request, proyecto_id):
    """
    :param proyecto_id:
    :param request:
    :return: formulario con la información de un proyecto específico
    """
    proyecto = servicio_proyectos.get_proyecto(proyecto_id)
    puede_ver_proyecto = servicio_proyectos.puede_ver_proyecto(request.user, proyecto)

    if proyecto is not None and puede_ver_proyecto:
        es_gerente = proyecto.gerente is not None and proyecto.gerente.id == request.user.id
        miembros = servicio_proyectos.get_miembros(proyecto)
        tipos_de_items = servicio_tdi.get_tipos_de_item_de_proyecto(proyecto)
        proyecto_roles = servicio_proyectos.get_roles_proyecto(proyecto)
        fases = servicio_proyectos.get_fases(proyecto)
        modificable = es_gerente and proyecto.estado != 'ca' and proyecto.estado != 'fi'
        ordenar_por = request.GET.get('ordenarPor')

        base_context = {
            'tiene_gerente': proyecto.gerente is not None,
            'sin_gerente': proyecto.gerente is None,
            'lista_fases': servicio_proyectos.get_fases(proyecto, ordenar_por),
            'lista_fases_solo_ver': True,
            'proyecto': proyecto,
            'ocultar_conf_fases': True,
            'modificable': modificable,
            'showLateral': True,
            'vistaDeProyecto': True,
            'resumenDeProyecto': True,
            'cantidadMiembros': len(miembros),
            'cantidadTdis': len(tipos_de_items),
            'cantidadRoles': len(proyecto_roles),
            'cantidadFases': len(fases),
            'detallesDeProyecto': True,
            'mostrar_reporte': True,
            'miembrosFaltantes': servicio_usuarios.get_lista_usuarios('nombreDeUsuario'),
            'directorios': [
                {'nombre': 'Proyectos', 'href': '/'},
                {'nombre': proyecto.nombre, 'href': '/proyecto/' + str(proyecto.id) + '/'},
                {'nombre': 'Detalles', 'href': '/proyecto/' + str(proyecto.id) + '/'}]
        }

        return render(request, 'proyecto/infoProyecto.html', base_context)
    return HttpResponse("La página no existe o no tiene permiso para verla.")

#@permiso_requerido('ver_configuracion_de_proyecto')
def proyecto_configuracion_general(request, proyecto_id):
    proyecto = servicio_proyectos.get_proyecto(proyecto_id)
    puede_ver_proyecto = servicio_proyectos.puede_ver_proyecto(request.user, proyecto)
    pueder_ver_config_proyecto = request.user.id == proyecto.gerente.id or \
        servicio_proyectos.usuario_tiene_permiso_proyecto(request.user, proyecto, 'ver_configuracion_de_proyecto')

    if proyecto is not None and puede_ver_proyecto and pueder_ver_config_proyecto:
        puede_iniciar = servicio_proyectos.get_fases(proyecto).count() > 0
        es_gerente = proyecto.gerente is not None and proyecto.gerente.id == request.user.id
        miembros_comite = servicio_proyectos.get_miembros_del_comite(proyecto)
        base_context = {
            'proyecto': proyecto,
            'modificable': es_gerente and proyecto.estado != 'ca' and proyecto.estado != 'fi',
            'showLateral': True,
            'gerente': proyecto.gerente,
            'vistaDeProyecto': True,
            'configuracionDeProyecto': True,
            'configuracionesGenerales': True,
            'puedeIniciarProyecto': puede_iniciar,
            'listaUsuarios': miembros_comite,
            'soloMostrarMiembrosActivos': True,
            'listaDeUsuariosTitulo': 'Integrantes',
            'mostrar_comite': True,
            'cantidadUsuariosActivos': len(miembros_comite),
            'miembrosFaltantes': servicio_usuarios.get_lista_usuarios('nombreDeUsuario').exclude(id__in=[m.id for m in miembros_comite]),
            'directorios': [
                {'nombre': 'Proyectos', 'href': '/'},
                {'nombre': proyecto.nombre, 'href': '/proyecto/' + str(proyecto.id) + '/'},
                {'nombre': 'Configuración', 'href': '/proyecto/' + str(proyecto.id) + '/config'},
                {'nombre': 'General', 'href': '/proyecto/' + str(proyecto.id) + '/config'}]
        }
        return render(request, 'proyecto/configuracionGeneral.html', base_context)
    return HttpResponse("La página no existe o no tiene permiso para verla.")

#@permiso_requerido('ver_configuracion_de_proyecto')
def proyecto_miembros_view(request, proyecto_id):
    proyecto = servicio_proyectos.get_proyecto(proyecto_id)
    puede_ver_proyecto = servicio_proyectos.puede_ver_proyecto(request.user, proyecto)
    pueder_ver_config_proyecto = request.user.id == proyecto.gerente.id or \
        servicio_proyectos.usuario_tiene_permiso_proyecto(request.user, proyecto, 'ver_configuracion_de_proyecto')

    if proyecto is not None and puede_ver_proyecto and pueder_ver_config_proyecto:
        mostrar_miembros_inactivos = request.GET.get('todos') == 'True'
        ordenar_por = request.GET.get('ordenarPor')
        filtro = request.GET.get('filtro')
        miembros = servicio_proyectos.get_miembros(proyecto, mostrar_miembros_inactivos, ordenar_por, filtro)
        es_gerente = proyecto.gerente is not None and proyecto.gerente.id == request.user.id
        miembros_faltantes = servicio_proyectos.get_miembros_faltantes(proyecto)
        cantidad_proyecto_roles = servicio_proyectos.get_roles_proyecto(proyecto).count()
        base_context = {
            'proyecto': proyecto,
            'modificable': es_gerente and proyecto.estado != 'ca',
            'showLateral': True,
            'cantidadRolesDeProyecto': cantidad_proyecto_roles,
            'gerente': proyecto.gerente,
            'vistaDeProyecto': True,
            'configuracionDeProyecto': True,
            'configuración_de_miembros_de_proyecto': True,
            'listaUsuarios': miembros,
            'miembrosFaltantes': miembros_faltantes,
            'mostrarMiembros': True,
            'cantidadUsuariosTotales': len(servicio_proyectos.get_miembros(proyecto, True)),
            'cantidadUsuariosActivos': len(servicio_proyectos.get_miembros(proyecto)),
            'directorios': [
                {'nombre': 'Proyectos', 'href': '/'},
                {'nombre': proyecto.nombre, 'href': '/proyecto/' + str(proyecto.id) + '/'},
                {'nombre': 'Configuración', 'href': '/proyecto/' + str(proyecto.id) + '/config'},
                {'nombre': 'Miembros', 'href': '/proyecto/' + str(proyecto.id) + '/miembros'}]
        }
        # Mostrar miembros inactivos
        if mostrar_miembros_inactivos:
            miembros_inactivos = servicio_proyectos.get_miembros_inactivos(proyecto) \
                if mostrar_miembros_inactivos else []
            cantidad_miembros_inactivos = len(miembros_inactivos)
            extra_context = {
                'miembros_inactivos': miembros_inactivos,
                'mostrarMiembrosInactivos': mostrar_miembros_inactivos,
                'cantidadMiembrosInactivos': cantidad_miembros_inactivos,
            }
            base_context = {**base_context, **extra_context}
        # Modal para modificar miembro específico
        mod_miembro_id = get_url_prameter_int(request, 'modMiembro')
        if mod_miembro_id is not None:
            usuario = servicio_usuarios.get_usuario(mod_miembro_id)
            tiene_miembro = servicio_proyectos.tiene_miembro(proyecto, usuario)
            if tiene_miembro:
                usuario_roles = servicio_proyectos.get_roles_proyecto_usuario(proyecto, usuario)
                roles_de_usuario_faltantes = servicio_proyectos.get_roles_faltantes(usuario_roles, proyecto)
                extra_context = {
                    'usuarioMod': usuario,
                    # 'usuarioModRoles': usuario_roles,
                    'usuarioModRolesFaltantes': roles_de_usuario_faltantes,
                    #'proyectoRoles': proyecto_roles,
                    # 'usuarioAgregarRol': True,
                }
                base_context = {**base_context, **extra_context}
        return render(request, 'proyecto/usuariosDeProyecto.html', base_context)
    return HttpResponse("La página no existe o no tiene permiso para verla.")

#@permiso_requerido('ver_configuracion_de_proyecto')
def proyecto_configuracion_roles(request, proyecto_id):
    proyecto = servicio_proyectos.get_proyecto(proyecto_id)
    puede_ver_proyecto = servicio_proyectos.puede_ver_proyecto(request.user, proyecto)
    pueder_ver_config_proyecto = request.user.id == proyecto.gerente.id or \
        servicio_proyectos.usuario_tiene_permiso_proyecto(request.user, proyecto, 'ver_configuracion_de_proyecto')

    if proyecto is not None and puede_ver_proyecto and pueder_ver_config_proyecto:
        mod_rol = get_url_prameter_int(request, 'modRol')
        todos = request.GET.get('todos') == 'True'
        todos_los_roles = servicio_proyectos.get_roles_proyecto(proyecto)
        roles_no_vacios = servicio_proyectos.get_roles_proyecto(proyecto, True)
        lista_roles = todos_los_roles if todos else roles_no_vacios
        es_gerente = proyecto.gerente is not None and proyecto.gerente.id == request.user.id
        #### Filtrar
        lista_roles = servicio_roles.filtrar_lista_de_roles(lista_roles, request.GET.get('filtro'), todos)
        #### Ordenar por
        lista_roles = servicio_roles.ordenar_lista_de_roles_por(lista_roles, request.GET.get('ordenarPor'))
        base_context = {
            'proyecto': proyecto,
            'modificable': es_gerente and proyecto.estado != 'ca',
            'showLateral': True,
            'gerente': proyecto.gerente,
            'vistaDeProyecto': True,
            'configuracionDeProyecto': True,
            'configuracionesDeRolesDeProyecto': True,
            'lista_roles': lista_roles,
            'cantidad_roles_no_vacios': len(roles_no_vacios),
            'cantidad_roles_totales': len(todos_los_roles),
            'directorios': [
                {'nombre': 'Proyectos', 'href': '/'},
                {'nombre': proyecto.nombre, 'href': '/proyecto/' + str(proyecto.id) + '/'},
                {'nombre': 'Configuración', 'href': '/proyecto/' + str(proyecto.id) + '/config'},
                {'nombre': 'Roles', 'href': '/proyecto/' + str(proyecto.id) + '/config'}]
        }
        if mod_rol is not None:
            rol_modificando = servicio_roles.get_rol(mod_rol)
            extra_context = {
                'rolModificando': rol_modificando,
                'permisosFaltantes': servicio_permisos.get_permisos_faltantes_de_ambito(
                    servicio_roles.get_permisos_rol(rol_modificando), 'pr'),
            }
            base_context = {**base_context, **extra_context}
        return render(request, 'proyecto/rolesDeProyecto.html', base_context)
    return HttpResponse("La página no existe o no tiene permiso para verla.")

#@permiso_requerido('ver_configuracion_de_proyecto')
def proyecto_configuracion_fases(request, proyecto_id):
    proyecto = servicio_proyectos.get_proyecto(proyecto_id)
    puede_ver_proyecto = servicio_proyectos.puede_ver_proyecto(request.user, proyecto)
    pueder_ver_config_proyecto = request.user.id == proyecto.gerente.id or \
        servicio_proyectos.usuario_tiene_permiso_proyecto(request.user, proyecto, 'ver_configuracion_de_proyecto')

    if proyecto is not None and puede_ver_proyecto and pueder_ver_config_proyecto:
        es_gerente = proyecto.gerente is not None and proyecto.gerente.id == request.user.id
        ordenar_por = request.GET.get('ordenarPor')
        mostrar = request.GET.get('mostrar')
        agregar_tdi = request.GET.get('agregar_tdi') is not None
        base_context = {
            'proyecto': proyecto,
            'cantidad_tdis_disponibles': servicio_proyectos.get_tipos_de_item_disponibles(proyecto).count(),
            'lista_fases': servicio_proyectos.get_fases(proyecto, ordenar_por),
            'modificable': es_gerente and proyecto.estado != 'ca',
            'showLateral': True,
            'gerente': proyecto.gerente,
            'vistaDeProyecto': True,
            'configuracionDeProyecto': True,
            'tdis': servicio_proyectos.get_tipos_de_item(proyecto),
            'configuracionesDeFasesDeProyecto': True,
            'directorios': [
                {'nombre': 'Proyectos', 'href': '/'},
                {'nombre': proyecto.nombre, 'href': '/proyecto/' + str(proyecto.id) + '/'},
                {'nombre': 'Configuración', 'href': '/proyecto/' + str(proyecto.id) + '/config'},
                {'nombre': 'Fases', 'href': '/proyecto/' + str(proyecto.id) + '/fases'}]
        }
        # Mostrar
        if mostrar == 'roles':
            mod_rol = get_url_prameter_int(request, 'modRol')
            mostrar_todos = request.GET.get('todos') == 'True'
            roles_no_vacios = servicio_proyectos.get_roles_de_fases(proyecto, True)
            roles_totales = servicio_proyectos.get_roles_de_fases(proyecto)
            lista_roles = roles_totales if mostrar_todos else roles_no_vacios
            extra_context = {
                'lista_roles': lista_roles,
                'cantidad_roles_no_vacios': len(roles_no_vacios),
                'cantidad_roles_totales': len(roles_totales)
            }
            base_context = {**base_context, **extra_context}
            if mod_rol is not None:
                permisos_existentes = servicio_roles.get_permisos_rol(mod_rol)
                extra_context = {
                    'modRol': mod_rol,
                    'permisosFaltantes': servicio_permisos.get_permisos_faltantes_de_ambito(permisos_existentes, 'fa')
                }
                base_context = {**base_context, **extra_context}
        elif mostrar == 'miembros':
            pass
        # Asiganar tdi a fase
        if agregar_tdi:
            fase_id = get_url_prameter_int(request, 'agregar_tdi')
            extra_context = {
                'mod_fase': servicio_fases.get_fase(fase_id),
                'agregar_tdi': True,
                'tdis_disponibles': servicio_proyectos.get_tipos_de_item_disponibles(proyecto)
            }
            base_context = {**base_context, **extra_context}
        # Renderizar página
        return render(request, 'fases/fasesDeProyecto.html', base_context)
    return HttpResponse("La página no existe o no tiene permiso para verla.")

#@permiso_requerido('ver_configuracion_de_proyecto')
def proyecto_configuracion_tdis(request, proyecto_id):
    proyecto = servicio_proyectos.get_proyecto(proyecto_id)
    puede_ver_proyecto = servicio_proyectos.puede_ver_proyecto(request.user, proyecto)
    pueder_ver_config_proyecto = request.user.id == proyecto.gerente.id or \
        servicio_proyectos.usuario_tiene_permiso_proyecto(request.user, proyecto, 'ver_configuracion_de_proyecto')

    if proyecto is not None and puede_ver_proyecto and pueder_ver_config_proyecto:
        tdi_importar = request.GET.get('importarTdi')
        es_gerente = proyecto.gerente is not None and proyecto.gerente.id == request.user.id
        base_context = {
            'proyecto': proyecto,
            'modificable': es_gerente and proyecto.estado != 'ca',
            'showLateral': True,
            'gerente': proyecto.gerente,
            'vistaDeProyecto': True,
            'configuracionDeProyecto': True,
            'configuracionesDeTdisDeProyecto': True,
            'directorios': [
                {'nombre': 'Proyectos', 'href': '/'},
                {'nombre': proyecto.nombre, 'href': '/proyecto/' + str(proyecto.id) + '/'},
                {'nombre': 'Configuración', 'href': '/proyecto/' + str(proyecto.id) + '/config'},
                {'nombre': 'Tipos de ítem', 'href': '/proyecto/' + str(proyecto.id) + '/tdis'}]
        }
        base_context = {**base_context, **tdis_context(request, proyecto)}
        if tdi_importar:
            proyecto_para_importar_tdi = servicio_proyectos.get_proyecto(get_url_prameter_int(request, 'importarTdi'))
            extra_context = {
                'importarTdi': True,
                'proyectosParaImportarTdi': servicio_proyectos.get_proyectos(),
                'proyectoParaImportarTdiSeleccionado': proyecto_para_importar_tdi,
                'tdisDeProyectoSeleccionado': servicio_proyectos.get_tipos_de_item(proyecto_para_importar_tdi),
            }
            base_context = {**base_context, **extra_context}
        return render(request, 'tdis/tdisDeProyecto.html', base_context)
    return HttpResponse("La página no existe o no tiene permiso para verla.")

def reporte_de_items_de_proyecto(request, proyecto_id):
    proyecto = servicio_proyectos.get_proyecto(proyecto_id)
    base_context = {
        'proyecto': proyecto,
        'items': servicio_items.get_items_proyecto(proyecto),
    }
    return render(request, 'informes/informeItemsProyecto.html', base_context)

def reporte_de_cambios_de_proyecto(request, proyecto_id):
    fecha_inicio = request.POST.get('fechaInicio')
    fecha_fin = request.POST.get('fechaFin')
    proyecto = servicio_proyectos.get_proyecto(proyecto_id)
    base_context = {
        'proyecto': proyecto,
        'items': servicio_items.get_items_proyecto(proyecto),
        'inicial': fecha_inicio,
        'final': fecha_fin,
        'inicial_obj': datetime.datetime.strptime(fecha_inicio, '%Y-%m-%d'),
        'final_obj': datetime.datetime.strptime(fecha_fin, '%Y-%m-%d'),
    }
    return render(request, 'informes/reporteCambiosProyecto.html', base_context)

def agregar_comite_usuario(request, proyecto_id):
    usuarios = request.POST.get('usuarios')
    servicio_proyectos.agregar_miembro_comite(usuarios, proyecto_id)
    href = request.POST.get('href')
    return redirect(href)

def remover_comite_usuario(request, proyecto_id):
    usuario = request.POST.get('usuario')
    servicio_proyectos.remover_miembro_comite(usuario, proyecto_id)
    href = request.POST.get('href')
    return redirect(href)

def solicitar_romper_lineabase(request):
    usuario = request.POST.get('usuario')
    linea_base = request.POST.get('lineaBase')
    motivo = request.POST.get('motivo')
    servicio_linea_base.solicitar_romper(linea_base, usuario, motivo)
    href = request.POST.get('href')
    return redirect(href)

def confirmar_romper_lineabase(request):
    usuario = request.POST.get('usuario')
    solicitud = request.POST.get('solicitud')
    voto = request.POST.get('voto')
    servicio_linea_base.votar_romper_linea_base(usuario, solicitud, voto)
    href = request.POST.get('href')
    return redirect(href)

def activar_proyecto(request):
    """
    Solicitud para aprobar un proyecto
    :param request:
    :return:
    """
    proyecto_ref = request.POST.get('proyectoId')
    href = request.POST.get('href')
    proyecto = servicio_proyectos.get_proyecto(proyecto_ref)
    proyecto.estado = 'ac'
    proyecto.save()
    return redirect(href)

def importar_tdi(request):
    """
    SOlicitud para importar un tipo de ítem un proyecto
    :param request:
    :return:
    """
    proyecto_ref = request.POST.get('proyecto')
    tdi_ref = request.POST.get('tdi')
    href = request.POST.get('href')
    resutl = servicio_proyectos.importar_tdi(proyecto_ref, tdi_ref)
    if resutl:
        return redirect(agregar_parametros_a_url(href, {'estadoImportacion': 'correcto'}))
    else:
        return redirect(agregar_parametros_a_url(href, {'estadoImportacion': 'error'}))

def agregar_gerente_a_proyecto(request):
    """
    Solicitud para agregar un gerente a un proyecto
    :param request:
    :return:
    """
    usuario = request.POST.get('usuario')
    proyecto = request.POST.get('proyecto')
    href = request.POST.get('href')
    servicio_proyectos.asignar_gerente(proyecto, usuario)
    return redirect(href)

def agregar_proyecto(request):
    """
    Solicitud para agregar proyecto al sistema
    :param request:
    :return:
    """
    nombre = request.POST.get('projectName')
    descripcion = request.POST.get('projectDesc')
    href = request.POST.get('href')
    servicio_proyectos.agregar_proyecto(nombre, descripcion)
    return redirect(href)

def cambiar_nombre_descripcion_proyecto(request):
    """

    :param request: parametros: nombre, desc, proyecto_id, href
    :return:
    """
    nombre = request.POST.get('nombre')
    descripcion = request.POST.get('descripcion')
    proyecto = request.POST.get('proyecto')
    href = request.POST.get('href')
    servicio_proyectos.cambiar_nombre(nombre, proyecto)
    servicio_proyectos.cambiar_descripcion(descripcion, proyecto)
    return redirect(href)

def cancelar_proyecto(request):
    proyecto_id = request.POST.get('proyectoId')
    href = request.POST.get('href')
    servicio_proyectos.cancelar_proyecto(proyecto_id)
    return redirect(href)

def finalizar_proyecto(request):
    proyecto = request.POST.get('proyectoId')
    href = request.POST.get('href')
    servicio_proyectos.finalizar_proyecto(proyecto)
    return redirect(href)

def agregar_rol_a_proyecto(request):
    """
    Solicitud para agregar rol a un proyecto
    :param request:
    :return:
    """
    nombre = request.POST.get('nombre')
    proyecto = request.POST.get('proyecto')
    href = request.POST.get('href')
    servicio_proyectos.agregar_rol_a_proyecto(proyecto, nombre)
    return redirect(href)

def remover_rol_de_proyecto(request):
    rolId = request.POST.get('rol')
    #proyecto = request.POST.get('proyecto')
    href = request.POST.get('href')
    servicio_proyectos.remover_rol_de_proyecto(rolId)
    return redirect(href)

def agregar_permiso_a_rol(request):
    rol = request.POST.get('rol')
    permiso = request.POST.get('permiso')
    href = request.POST.get('href')
    servicio_roles.agregar_permiso_a_rol(rol, permiso)
    return redirect(href)

def remover_permiso_de_rol(request):
    rol = request.POST.get('rol')
    permiso = request.POST.get('permiso')
    href = request.POST.get('href')
    servicio_roles.eliminar_permiso_a_rol(rol, permiso)
    return redirect(href)

def agregar_tdi(request):
    """

    :param request: nombre, proyecto_id, href
    :return:
    """
    tdi = request.POST.get('tdi')
    proyecto = request.POST.get('proyecto')
    href = request.POST.get('href')
    servicio_tdi.agregar_tipo_de_item(tdi, proyecto)
    return redirect(href)

def agregar_tdi_a_fase(request):
    """

    :param request: tdi, fase, href
    :return:
    """
    tdi = request.POST.get('tdi')
    fase = request.POST.get('fase')
    href = request.POST.get('href')
    servicio_fases.asignar_fase_a_tdi(fase, tdi)
    return redirect(href)

def eliminar_tdi(request):
    """

    :param request:
    :return:
    """
    tdi = request.POST.get('tdi')
    href = request.POST.get('href')
    servicio_tdi.eliminar_tipo_de_item(tdi)
    return redirect(href)

def cambiar_nombre_tdi(request):
    """

    :param request: tdiNombreCambiado, tdiId, href
    :return:
    """
    nombre = request.POST.get('tdiNombreCambiado')
    tdi_id = request.POST.get('tdiId')
    href = request.POST.get('href')
    servicio_tdi.cambiar_nombre_tdi(tdi_id, nombre)
    return redirect(href)

def eliminar_atributo_de_tdi(request):
    """

    :param request: atributoId, href
    :return:
    """
    atributo_id = request.POST.get('atributo')
    href = request.POST.get('href')
    servicio_tdi.eliminar_atributo_de_tdi(atributo_id)
    return redirect(href)

def agregar_atributo_de_tdi(request):
    """

    :param request: href, atributoNombre, atributoTipo, atributoObligatorio, atributoMin, atributoMax
    :return:
    """
    nombre = request.POST.get('nombre')
    tipo = request.POST.get('tipo')
    es_obligatorio = request.POST.get('es_obligatorio') == 'on'
    min = request.POST.get('min')
    max = request.POST.get('max')
    href = request.POST.get('href')
    tdi = request.POST.get('tdi')
    servicio_tdi.agregar_atributo_de_tdi(nombre, tdi, tipo, es_obligatorio, min, max)
    return redirect(href)

def agregar_miembro_a_proyecto(request):
    """

    :param request: href ,usuarios, proyecto
    :return:
    """
    usuarios = request.POST.get('usuarios').split(',')
    proyecto = request.POST.get('proyecto')
    href = request.POST.get('href')
    for usuario in usuarios:
        servicio_proyectos.agregar_miembro(usuario, proyecto)
    return redirect(href)

def deshabilitar_miembro_de_proyecto(request):
    """

    :param request: href ,username, proyecto
    :return:
    """
    usuario = request.POST.get('usuario')
    proyecto = request.POST.get('proyecto')
    href = request.POST.get('href')
    servicio_proyectos.deshabilitar_miembro(usuario, proyecto)
    return redirect(href)

def agregar_rol_de_proyecto_a_miembro(request):
    """

    :param request: usuario, proyecto, href, rolId
    :return:
    """
    usuario = request.POST.get('usuario')
    rol = request.POST.get('rol')
    proyecto = request.POST.get('proyecto')
    href = request.POST.get('href')
    servicio_proyectos.agregar_rol_proyecto_a_miembro(usuario, proyecto, rol)
    return redirect(href)

def eliminar_rol_de_proyecto_a_miembro(request):
    """

    :param request: usuario, proyecto, href, rolId
    :return:
    """
    usuario = request.POST.get('usuario')
    rol = request.POST.get('rol')
    proyecto = request.POST.get('proyecto')
    href = request.POST.get('href')
    servicio_proyectos.eliminar_rol_proyecto_a_miembro(usuario, proyecto, rol)
    return redirect(href)

def agregar_fase_a_proyecto(request):
    """

    :param request: proyecto, fase
    :return:
    """
    fase_nombre = request.POST.get('fase')
    proyecto = request.POST.get('proyecto')
    color = request.POST.get('color')
    icono = request.POST.get('icono')
    href = request.POST.get('href')
    numero = request.POST.get('numero')
    servicio_fases.agregar_fase_a_proyecto(fase_nombre, proyecto, int(numero), color, icono)
    return redirect(href)

def eliminar_fase_de_proyecto(request):
    """
    Solicitud para eliminar una fase de un proyecto
    :param request:
    :return:
    """
    nombre = request.POST.get('fase')
    proyecto = request.POST.get('proyecto')
    href = request.POST.get('href')
    servicio_fases.eliminar_fase_de_proyecto(nombre, proyecto)
    return redirect(href)

def agregar_rol_de_fases(request):
    rol = request.POST.get('rol')
    proyecto = request.POST.get('proyecto')
    href = request.POST.get('href')
    servicio_fases.agregar_rol_de_fase_a_proyecto(rol, proyecto)
    return redirect(href)

def agregar_fase_rol_permiso(request):
    """
    Solicitud para agregar permisos a roles de fase
    :param request: rol, permiso
    :return:
    """
    rol = request.POST.get('rol')
    permiso = request.POST.get('permiso')
    href = request.POST.get('href')
    servicio_roles.agregar_permiso_a_rol(rol, permiso)
    return redirect(href)

def adjuntar_archivo_item(request):
    """
    Solicitud para asociar un archivo adjunto a un item y subirlo a la nube
    :param request: item, nombre
    :return:
    """
    item = request.POST.get('item')
    nombre = request.POST.get('nombre')
    href = request.POST.get('href')
    servicio_items.subir_adjunto(nombre, item)
    return redirect(href)

def remover_archivo_item(request):
    """
    Solicitud para remover el archivo adjunto de un item.
    :param request: item
    :return:
    """
    item = request.POST.get('item')
    href = request.POST.get('href')
    servicio_items.remover_adjunto(item)
    return redirect(href)

@register.filter
def tipo_descripcion(tipo):
    return 'numerico' if tipo == 'num' else 'booleando' if tipo == 'bool' else 'url' if tipo == 'url' else 'texto'

@register.filter
def es_admin(usuario_ref):
    usuario = servicio_usuarios.get_usuario(usuario_ref)
    if usuario is not None:
        return servicio_usuarios.get_usuario(usuario).is_superuser
    return False

@register.filter
def no_es_admin(usuario):
    return not servicio_usuarios.get_usuario(usuario).is_superuser

@register.filter
def descripcion_estado(estado):
    return 'activo' if estado == 'ac' else \
           'cancelado' if estado == 'ca' else \
           'finalizado' if estado == 'fi' else ''

@register.filter
def descripcion_estado_item(estado):
    return 'en revision' if estado == 'er' else \
           'desactivado' if estado == 'da' else \
           'comprometido' if estado == 'co' else \
           'pendiente de versionado' if estado == 'pv' else \
           'pendiente' if estado == 'pe' else \
           'aprobado' if estado == 'ap' else 'desconocido'

@register.filter
def roles_de_usuario_en_proyecto(usuario, proyecto):
    return servicio_proyectos.get_roles_proyecto_usuario(proyecto, usuario)

@register.filter
def cantidad_de_roles_de_usuario_en_proyecto_faltantes(usuario, proyecto):
    roles_existentes = servicio_proyectos.get_roles_proyecto_usuario(proyecto, usuario)
    return servicio_proyectos.get_roles_faltantes(roles_existentes, proyecto).count()

@register.filter
def usuario_es_activo_en_proyecto(usuario, proyecto):
    return servicio_proyectos.es_miembro_habilitado(proyecto, usuario)

@register.filter
def permisos_de_rol(rol):
    return servicio_roles.get_permisos_rol(rol)

@register.filter
def atributos_de_tdi(tdi):
    return servicio_tdi.get_atributos_de_tdi(tdi)

@register.filter
def cantidad_de_tdis_de_proyecto(proyecto):
    return len(servicio_proyectos.get_tipos_de_item(proyecto))

@register.filter
def usuario_proyecto(_usuario, _proyecto):
    return _usuario, _proyecto

@register.filter
def tiene_permiso_proyecto(_usuario_proyecto, permiso):
    _usuario, _proyecto = _usuario_proyecto
    usuario = servicio_usuarios.get_usuario(_usuario)
    proyecto = servicio_proyectos.get_proyecto(_proyecto)
    gerente = servicio_proyectos.get_gerente(proyecto)
    es_gerente = gerente is not None and gerente.id == usuario.id
    return es_gerente or servicio_proyectos.usuario_tiene_permiso_proyecto(usuario, proyecto, permiso)

@register.filter
def get_version_actual_item(item):
    return servicio_items.get_version_actual_item(item).version

@register.filter
def get_item_versiones(item):
    return servicio_items.get_item_versiones(item).order_by('-fecha_version')

@register.filter
def puede_finalizar_proyecto(proyecto):
    return servicio_proyectos.puede_finalizar_proyecto(proyecto)

@register.filter
def item_tiene_arch_adj(item_ref):
    item = servicio_items.get_item(item_ref)
    return item.nombre_adjunto is not None

@register.filter
def item_arch_adj(item_ref):
    item = servicio_items.get_item(item_ref)
    return item.nombre_adjunto

utc = pytz.UTC

@register.filter
def fecha_mayor_que(fecha1, fecha2):
    fecha1 = fecha1.replace(tzinfo=utc)
    fecha2 = fecha2.replace(tzinfo=utc)
    return fecha1 >= fecha2

@register.filter
def fecha_menor_que(fecha1, fecha2):
    fecha1 = fecha1.replace(tzinfo=utc)
    fecha2 = fecha2.replace(tzinfo=utc)
    return fecha1 <= fecha2
