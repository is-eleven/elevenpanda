# Generated by Django 3.0.4 on 2020-09-15 19:26

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('proyecto', '0012_itemversion_version'),
    ]

    operations = [
        migrations.AddField(
            model_name='itemrelacionversion',
            name='item',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='irv_item', to='proyecto.Item'),
        ),
        migrations.AddField(
            model_name='itemrelacionversion',
            name='item_relacion_original',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='irv_item_relacion_original', to='proyecto.ItemRelacion'),
        ),
        migrations.AlterField(
            model_name='itemrelacionversion',
            name='item_relacion',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='irv_item_relacion', to='proyecto.Item'),
        ),
        migrations.AlterField(
            model_name='itemrelacionversion',
            name='item_version',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='irv_item_version', to='proyecto.ItemVersion'),
        ),
        migrations.AlterField(
            model_name='itemrelacionversion',
            name='relacion_tipo',
            field=models.CharField(default='', max_length=1),
        ),
    ]
