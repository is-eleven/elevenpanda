# Generated by Django 3.0.4 on 2020-09-15 19:27

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('proyecto', '0013_auto_20200915_1526'),
    ]

    operations = [
        migrations.AddField(
            model_name='itematributoversion',
            name='item_version',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='proyecto.ItemVersion'),
        ),
    ]
