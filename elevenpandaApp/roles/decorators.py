from urllib.parse import urlparse
from django.http import HttpResponseRedirect, HttpResponse
from six import wraps

from Usuarios.models import Usuario
from .services import ServicioUsuarioRolSistema
from Usuarios.services import ServicioUsuario

servicio_usuario_rol_sistema = ServicioUsuarioRolSistema()
servicio_usuario = ServicioUsuario()


def permiso_requerido(permiso):
    """

    :param permiso:
    :return:
    """
    def permiso_requerido_true(function):
        """

        :param function:
        :return:
        """

        """
        Esta funcion implementa un decorator personalizado para que los usuarios esperen a ser aprobados para ingresar
        al sistema.
        :param function: es la funcion detallada abajo
        :return: Resultado de la funcion
        """

        def _function(request, *args, **kwargs):
            """
            Esta funcion realiza una validacion sobre una variable de usuario
            :param request: Es un query que contiene el html de una vista
            :param args: valores
            :param kwargs: claves
            :return: Retorna una redireccion Http si el usuario no esta validado
            """

            if servicio_usuario_rol_sistema.usuario_tiene_permiso(request.user, permiso) or request.user.is_superuser:
                return function(request, *args, **kwargs)
            return HttpResponse(
                'Esta pagina no existe o no tiene permiso para verla, contacte con un administrador del sistema.')


        return _function
    return permiso_requerido_true


def permiso_ver_perfil_requerido(function):
    """
        Asegra que un usuario (dependiento de sus permisos) pueda ver todos los perfiles, solo el suyo, o ninguno.
    """
    def _function(request, *args, **kwargs):
        username = request.path.split('/')[2]
        # Valida el nombre del usuario cuyo perfil será mostrado
        es_perfil_propio = servicio_usuario.es_usuario_propio(request.user, username)
        username = request.user.username if es_perfil_propio else username
        #
        # Definir visibilidad
        #
        # Si es admin, True
        es_admin = False
        if request.user.is_superuser:
            es_admin = True
        # Tiene permiso de ver todos los perfiles
        visible = servicio_usuario_rol_sistema.usuario_tiene_permiso(request.user.username, 'ver_todos_los_perfiles')
        if not visible and es_perfil_propio:
            # Tiene permiso de ver su propio perfil
            visible = servicio_usuario_rol_sistema.usuario_tiene_permiso(request.user.username, 'ver_perfil_propio')

        if visible or es_admin:
            return function(request, *args, **kwargs)
        return HttpResponseRedirect('/')
    return _function
