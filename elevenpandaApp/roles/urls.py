from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect
from django.urls import path
from .views import listar_roles_view, agregar_rol_view, eliminar_rol_view, agregar_permiso_rol_view, eliminar_permiso_rol_view


urlpatterns = [
    path('listar/', listar_roles_view),
    path('agregar/', agregar_rol_view),
    path('remover/', eliminar_rol_view),
    path('permiso/agregar/', agregar_permiso_rol_view),
    path('permiso/remover/', eliminar_permiso_rol_view),
]