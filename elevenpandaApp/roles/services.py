# coding=utf-8
#from proyecto.models import UsuarioRolProyecto, ProyectoRol
#from proyecto.services import ServicioProyectos
from .models import Rol, RolPermiso, Permiso, RolUsuario

from Usuarios.models import Usuario
from Usuarios.services import ServicioUsuario

class ServicioPermisos:
    """
    Definición de funciones para interactuar con los permisos del sistema
    """
    def get_permiso(self, permiso):
        """
        Obiene un permiso dado una referencia del mismo.

        :param permiso: referencia
        :return: objeto permiso
        """
        if isinstance(permiso, str) and permiso.isdigit():
            return Permiso.objects.filter(id=int(permiso)).first()
        if isinstance(permiso, str):
            return Permiso.objects.filter(nombre=permiso).first()
        elif isinstance(permiso, int):
            return Permiso.objects.filter(id=permiso).first()
        return permiso

    def get_permisos_sistema(self):
        """
        Obtiene todos los permisos del sistema.

        :return: lista de permisos en el ámbito de sistema
        """
        return self.get_permisos_de_ambito('si')

    def get_permisos_de_ambito(self, ambito):
        """
        Obtiene todos los permisos en un ámbito dado.

        :param ambito: ámbito a considerar
        :return: lista de permisos en un ámbito específico
        """
        return Permiso.objects.filter(ambito=ambito).all()

    def get_permisos_faltantes_de_ambito(self, permisos_existentes, ambito):
        """
        :param permisos_existentes: lista de permisos a considerar.

        :param ambito: ámbito a considerar
        :return: lista de permisos en el ámbito dado que no incluyen los permisos ya existentes
        """
        todos_los_permisos = self.get_permisos_de_ambito(ambito)
        permisos_faltantes = []
        for permiso in todos_los_permisos:
            falta = True
            for existente in permisos_existentes:
                if permiso.id == existente.id:
                    falta = False
                    break
            if falta:
                permisos_faltantes.append(permiso)
        permisos_id = [perm.id for perm in permisos_faltantes]
        return Permiso.objects.filter(id__in=permisos_id)

    def agregar_si_no_existe(self, codigo, descripcion, ambito):
        """
        Agregar un nuevo permiso a la base de datos si todavía no existe.

        :param codigo: definición del permiso
        :param descripcion: descripción simple del permiso
        :param ambito: área de efecto (sistema, proyecto o fase)
        """
        permiso = Permiso.objects.filter(nombre=codigo, ambito=ambito).first()
        if permiso is None:
            Permiso.objects.create(nombre=codigo, descripcion=descripcion, ambito=ambito)

    def crear_permisos(self):
        """
        Agrega la lista por defecto de permisos del sistema.
        """
        # Crear permisos de sistema
        self.agregar_si_no_existe('ver_lista_usuarios', 'Ver lista de usuarios', 'si')
        self.agregar_si_no_existe('ver_todos_los_perfiles', 'Ver todos los perfiles', 'si')
        self.agregar_si_no_existe('ver_perfil_propio', 'Ver perfil propio', 'si')
        self.agregar_si_no_existe('modificar_todos_los_perfiles', 'Modificar todos los perfiles', 'si')
        self.agregar_si_no_existe('modificar_perfil_propio', 'Modificar perfil propio', 'si')
        self.agregar_si_no_existe('deshabilitar_usuario', 'Dehabilitar usuarios', 'si')
        self.agregar_si_no_existe('aprobar_usuario', 'Aprobar usuarios', 'si')
        self.agregar_si_no_existe('ver_lista_roles_de_sistema', 'Ver lista de roles del sistema', 'si')
        self.agregar_si_no_existe('modificar_roles_sistema', 'Modificar roles del sistema', 'si')
        self.agregar_si_no_existe('crear_proyecto', 'Crear proyecto', 'si')
        self.agregar_si_no_existe('ver_todos_los_proyectos', 'Ver todos los proyectos', 'si')
        self.agregar_si_no_existe('ver_proyectos_propios', 'Ver proyectos propios', 'si')
        self.agregar_si_no_existe('ver_lista_proyectos', 'Ver lista de proyectos', 'si')
        self.agregar_si_no_existe('modificar_roles_de_usuarios', 'Modificar roles de usuarios', 'si')
        # Crear permisos de proyectos
        self.agregar_si_no_existe('ver_configuracion_de_proyecto', 'Ver configuraciṕn de proyecto', 'pr')
        self.agregar_si_no_existe('ver_fases_de_proyecto', 'Ver fases de proyecto', 'pr')
        # Crear permisos de fases
        self.agregar_si_no_existe('ver_fase', 'Ver fase', 'fa')
        self.agregar_si_no_existe('crear_nuevo_item', 'Crear nuevo item', 'fa')
        self.agregar_si_no_existe('modificar_item', 'Modificar item', 'fa')
        self.agregar_si_no_existe('modificar_relaciones', 'Modificar relaciones', 'fa')
        self.agregar_si_no_existe('versionar_item', 'Versionar ítem', 'fa')
        self.agregar_si_no_existe('ver_trazo', 'Ver trazo de ítem', 'fa')
        self.agregar_si_no_existe('aprobar_pendiente', 'Aprobar ítem pendiente', 'fa')
        self.agregar_si_no_existe('aprobar_en_revision', 'Aprobar ítem en revisión', 'fa')
        self.agregar_si_no_existe('aprobar_comprometido', 'Aprobar ítem comprometido', 'fa')
        self.agregar_si_no_existe('crear_linea_base', 'Crear línea base', 'fa')
        self.agregar_si_no_existe('ver_historial_items', 'Ver historial de ítems', 'fa')


class ServicioRoles:
    """
    Clase que organiza los servicios para roles, simplificacion de api.
    """

    servicio_usuario = ServicioUsuario()
    servicio_permisos = ServicioPermisos()

    def get_rol(self, rol):
        """
        Obtiene el objeto rol dado su refencia.

        :param rol: referencia de rol
        :return: objeto rol
        """
        if isinstance(rol, str) and rol.isdigit():
            return Rol.objects.filter(id=int(rol)).first()
        elif isinstance(rol, int):
            return Rol.objects.filter(id=rol).first()
        return rol

    def get_roles(self, ambiente='si'):
        """
        Obtiene todos los roles dado un ambiente.

        :param: ambiente: pueden ser: sistema ('si'), proyecto ('pr'), fase ('fa')
        :return: Retorna todos los roles que existen en el sistema
        """
        from roles.services import ServicioPermisos
        ServicioPermisos().crear_permisos()
        return Rol.objects.filter(ambiente=ambiente).all()

    def get_roles_no_vacios(self, ambiente='si'):
        """
        Obtiene todos los roles que tengan al menos un permiso asignado, en un ambiente dado

        :param: ambiente: pueden ser: sistema ('si'), proyecto ('pr'), fase ('fa')
        :return: Retorna todos los roles que existen en el sistema
        """
        todos_los_roles = self.get_roles(ambiente=ambiente)
        roles_con_permisos = []
        for rol in todos_los_roles:
            if rol.permisos.count() > 0:
                roles_con_permisos.append(rol)
        roles_id = [roles_con_permiso.id for roles_con_permiso in roles_con_permisos]
        return Rol.objects.filter(id__in=roles_id).all()

    def roles_a_diccionario(self, roles):
        """
        Convierte una lista de roles a json

        :param roles: lista de roles a convertir
        :return: cadena de roles convertida
        """
        diccionario_roles = {}
        for rol in roles:
            diccionario_roles[rol.id] = rol.nombre
        return diccionario_roles

    def permisos_a_diccionario(self, permisos):
        """
        Convierte una lista de permisos a json

        :param roles: lista de permisos a convertir
        :return: cadena de permisos convertida
        """
        diccionario = {}
        for perm in permisos:
            diccionario[perm.id] = perm.descripcion
        return diccionario

    def get_roles_faltantes_en_ambiente(self, roles_existentes, ambiente='si'):
        """
        Obtiene todos los roles en un ámbito dado y que no se encuentren en la lisa de roles existentes dados.

        :param roles_existentes: lista de roles a considerar
        :param ambiente: ambiente a considerar
        :return: lista de roles en el ambiente dado que no incluyen los roles ya existentes
        """
        todos_los_roles = self.get_roles(ambiente)
        roles_faltantes = []
        for rol in todos_los_roles:
            falta = True
            for existente in roles_existentes:
                if rol.id == existente.id:
                    falta = False
                    break
            if falta:
                roles_faltantes.append(rol)
        roles_id = [rol.id for rol in roles_faltantes]
        return Rol.objects.filter(id__in=roles_id).all()

    def agregar_rol(self, descripcion):
        """
        Permite crear un nuevo rol.

        :param descripcion: Es el nombre del rol
        :return: el rol creado
        """
        rol = Rol.objects.create(nombre=descripcion)
        rol.save()
        return rol

    def get_permisos_rol(self, rol):
        """
        Funcion para obtener los permisos de un rol.

        :param rol: El rol en cuestion
        :return: La lista de permisos del rol
        """
        if rol is None:
            return None
        return self.get_rol(rol).permisos.all()

    def filtrar_lista_de_roles(self, lista_original, filtro, mostrar_todos):
        if filtro is not None:
            if filtro == 'sinPermisos':
                if mostrar_todos:
                    roles = []
                    for original in lista_original:
                        if self.get_permisos_rol(original).count() == 0:
                            roles.append(original)
                    roles_id = [rol.id for rol in roles]
                    return Rol.objects.filter(id__in=roles_id)
                else:
                    return Rol.objects.none()
        return lista_original

    def ordenar_lista_de_roles_por(self, lista_original, ordenar_por):
        """
        Ordena una lista de roles dada.

        :param lista_original: lista de roles a ser ordenada
        :param ordenar_por: nombre del atributo en función al cual se ordenará
        :return: lista de roles ordenada
        """
        if ordenar_por is not None:
            if ordenar_por == 'nombre':
                return lista_original.order_by('nombre')
            # Ordenar por cantidad de permisos
            elif ordenar_por == 'cantidadPermisos' or ordenar_por == 'cantidadPermisosInv':
                class RolCantidad:
                    def __init__(self, rol, cantidad):
                        self.rol = rol
                        self.cantidad = cantidad

                lista_rol_cantidad = []
                for rol in lista_original:
                    lista_rol_cantidad.append(RolCantidad(rol, self.get_permisos_rol(rol).count()))
                lista_rol_cantidad.sort(key=lambda x: x.cantidad, reverse=ordenar_por != 'cantidadPermisosInv')
                lista_roles_ordenado = []
                for rol_cantidad in lista_rol_cantidad:
                    lista_roles_ordenado.append(rol_cantidad.rol)
                lista_roles_id = [rol.id for rol in lista_roles_ordenado]
                from .models import Rol
                lista_roles_objeto = Rol.objects.filter(id__in=lista_roles_id)
                lista_roles_objeto = dict([(obj.id, obj) for obj in lista_roles_objeto])
                return [lista_roles_objeto[rol_id] for rol_id in lista_roles_id]
        return lista_original

    def tiene_permiso(self, rol, permiso_ref):
        """
        Funcion para chequear si un rol tiene asignado un permiso.

        :param rol: Un rol especifico
        :param permiso: Un permiso especifico
        :return: retorna True si el rol tiene el permiso, False en caso contrario
        """
        permiso = self.servicio_permisos.get_permiso(permiso_ref)
        if permiso:
            for permiso_de_rol in self.get_permisos_rol(rol):
                if permiso_de_rol is not None and permiso.id == permiso_de_rol.id:
                    return True
        return False

    def get_roles_vacios(self, ambiente='si'):
        """
        Obtiene una lista de roles en un ambiente dado que no tengan permisos asignados.

        :return: lista de roles del sistema que no tienen asignados ningún permiso
        """
        roles_no_vacios = self.get_roles_no_vacios(ambiente)
        return self.get_roles_faltantes_en_ambiente(roles_no_vacios, ambiente)

    def agregar_permiso_a_rol(self, rol_ref, permiso_ref):
        """
        Asigna un conjunto de permisos a un rol.

        :param rol: El rol creado
        :param permisos: Es una lista de permisos relacionados al rol
        :return:
        """
        rol = self.get_rol(rol_ref)
        permiso = self.servicio_permisos.get_permiso(permiso_ref)
        if rol.ambiente == permiso.ambito:
            RolPermiso.objects.create(rol=rol, permiso=permiso).save()

    def eliminar_permiso_a_rol(self, rol, permiso):
        """
        Desasocia un permiso de un rol.

        :param rol: rol del cual se removerá el permiso
        :param permiso: permiso a remover
        """
        rol = self.get_rol(rol)
        permiso = self.servicio_permisos.get_permiso(permiso)
        RolPermiso.objects.filter(rol=rol, permiso=permiso).delete()


class ServicioUsuarioRolSistema:
    """
    Servicio para relaciones de Usuario con Roles y Permisos, simplificacion de api.
    Revisar la definicion correcta de este servicio.
    """
    servicios_roles = ServicioRoles()
    servicio_usuario = ServicioUsuario()
    #servicio_proyectos = ServicioProyectos()

    def get_roles_usuario(self, usuario):
        """
        Funcion para obtener los roles de un usuario.

        :param usuario: El usuario en cuestion
        :return: Los roles del usuario
        """
        return self.servicio_usuario.get_usuario(usuario).roles.all()

    def usuario_tiene_rol(self, usuario, rol):
        """
        Define si un usuario dado posee un permiso específico

        :param usuario: usuario a considerar
        :param rol: rol a considerar
        :return: si un usuario dado tiene asignado un rol específico
        """
        return rol in self.get_roles_usuario(usuario)

    def usuario_tiene_permiso(self, usuario_ref, permiso):
        """
        Define si un usuario dado posee un permiso dado en cualquiera de sus roles

        :param usuario: usuario a considerar
        :param permiso: permiso a buscar
        :return: si un usuario dado tiene asignado un permiso específico
        """
        usuario = self.servicio_usuario.get_usuario(usuario_ref)
        if usuario.is_superuser:
            return True
        roles = self.get_roles_usuario(usuario)
        for rol in roles:
            if self.servicios_roles.tiene_permiso(rol, permiso):
                return True
        return False

    def agregar_rol_de_sistema(self, usuario, rol):
        """
        Agregar un nuevo rol a un usuario.

        :param usuario: usuario al cual se asignará el rol
        :param rol: rol que será asignado
        """
        RolUsuario.objects.create(
            usuario=self.servicio_usuario.get_usuario(usuario),
            rol=self.servicios_roles.get_rol(rol)).save()

    def remover_rol_de_ambiente(self, usuario_ref, rol_ref, ambiente='si'):
        """
        Remueve un rol de un usuario.

        :param usuario_ref: usuario al cual se le removerá un rol
        :param rol_ref: rol que será removido
        :param ambiente: tipo de rol a considerar
        """
        usuario = self.servicio_usuario.get_usuario(usuario_ref)
        rol = self.servicios_roles.get_rol(rol_ref)
        if ambiente == 'si':
            RolUsuario.objects.filter(usuario=usuario, rol=rol).delete()
        elif ambiente == 'pr':
            from proyecto.models import UsuarioRolProyecto, ProyectoRol
            rol_proyecto = ProyectoRol.objects.filter(rol=rol).first()
            UsuarioRolProyecto.objects.filter(usuario=usuario, rol_proyecto=rol_proyecto).delete()
        elif ambiente == 'fa':
            pass
            # TODO: ambiente de fase

    def eliminar_rol_y_de_usuarios(self, rol, ambiente='si'):
        """
        Permite eliminar un rol del sistema y al mismo tiempo remueve dicho rol de todos los uaurios que lo tengan.

        :param ambiente: pueden ser: 'si' (sistema), 'pr' (proyecto), 'fa' (fase)
        :param rol: Rol a eliminar
        """
        rol_eliminar = self.servicios_roles.get_rol(rol)
        if rol_eliminar:
            usuarios = self.servicio_usuario.get_lista_usuarios()
            for usuario in usuarios:
                if ambiente == 'si' and self.usuario_tiene_rol(usuario, rol_eliminar):
                    self.remover_rol_de_ambiente(usuario, rol_eliminar)
                elif ambiente == 'pr':
                    from proyecto.services import ServicioProyectos
                    if ServicioProyectos().miembro_tiene_rol_de_proyecto(usuario, rol_eliminar):
                        self.remover_rol_de_ambiente(usuario, rol_eliminar)
                else:
                    pass
                    # TODO: ambiente de fase
            rol_eliminar.delete()

    def get_roles_de_usuario_no_vacios(self, usuario_ref):
        """
        Obtiene una lista de todos los roles pertenecientes a un usuario dado que tengan al menos un permiso asignado.

        :param usuario_ref: usuario a considerar
        :return: lista de roles con al menos un permiso asignado
        """
        roles_usuario = self.get_roles_usuario(usuario_ref)
        roles = []
        for rol in roles_usuario:
            if Permiso.objects.filter(rol=rol).count() > 0:
                roles.append(rol)
        roles_id = [rol.id for rol in roles]
        return Rol.objects.filter(id__in=roles_id).all()
