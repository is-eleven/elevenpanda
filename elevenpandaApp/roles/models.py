from django.db import models

AMBIENTES = [
    ('si', 'Sistema'),
    ('pr', 'Proyecto'),
    ('fa', 'Fase'),
]


class Permiso(models.Model):
    """
    Clase que modela un permiso
    """
    nombre = models.CharField(max_length=50)
    descripcion = models.CharField(max_length=100, default='None')
    ambito = models.CharField(max_length=2, choices=AMBIENTES, default="si")

    def __str__(self):
        return self.nombre+':'+self.ambito

class Rol(models.Model):
    """
    Clase que modela un Rol dentro del sistema.
    """
    nombre = models.CharField(max_length=50)
    permisos = models.ManyToManyField(Permiso, through="RolPermiso")
    ambiente = models.CharField(max_length=2, choices=AMBIENTES, default='si')

    def __str__(self):
        return self.nombre+':'+self.ambiente


"""
    class Meta: #agregar/eliminar segun necesidad y/o si ya existe en django.
        permissions = (
            ('eliminar_rol', 'Eliminar Rol'),
            ('agregar_rol', 'Agregar Rol'),
            ('modificar_rol', 'Modificar Rol'),
            ('listar_roles_sistema', 'Listar Roles del Sistema'),
            ('agregar_permiso', 'Agregar Permiso'),
            ('listar_permisos', 'Listar Permisos'),
        )
"""


class RolPermiso(models.Model):
    """
    Clase intermedia que relaciona un rol con un permiso
    """
    rol = models.ForeignKey(Rol, on_delete=models.CASCADE)
    permiso = models.ForeignKey(Permiso, on_delete=models.CASCADE)

class RolUsuario(models.Model):
    """
    Clase intermedia que relaciona un rol con un usuario
    """
    usuario = models.ForeignKey('Usuarios.Usuario', on_delete=models.CASCADE)   #revisar
    rol = models.ForeignKey(Rol, on_delete=models.CASCADE)
