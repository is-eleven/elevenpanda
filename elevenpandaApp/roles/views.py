from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.template.defaulttags import register

from Usuarios.decorators import cuenta_aprobada_requerida
from .decorators import permiso_requerido
from .services import ServicioRoles, ServicioUsuarioRolSistema, ServicioPermisos
from Usuarios.views import get_contexto_basico, tiene_permiso_sistema

servicio_roles = ServicioRoles()
servicios_usuario_rol_sistema = ServicioUsuarioRolSistema()
servicio_permisos = ServicioPermisos()

@login_required
@cuenta_aprobada_requerida
@permiso_requerido('ver_lista_roles_de_sistema')
def listar_roles_view(request):
    """
    Vista de visualizacion de roles
    :param request: Todos los datos de la vista
    :return: Pagina html renderizada
    """
    #### mostrar todos? o solo con permisos?
    mostar_todos = request.GET.get('todos') == 'True'
    roles_no_vacios = servicio_roles.get_roles_no_vacios()
    lista_roles = servicio_roles.get_roles()
    cantidad_roles_totales = lista_roles.count()
    if not mostar_todos:
        lista_roles = roles_no_vacios
    #### Modificar permisos de rol
    mod_rol = False
    rol_a_modificar = None
    id_mod_rol = request.GET.get('modRol')
    if id_mod_rol is not None:
        rol_a_modificar = servicio_roles.get_rol(id_mod_rol)
        if rol_a_modificar is not None:
            mod_rol = True
    #### Filtrar
    lista_roles = servicio_roles.filtrar_lista_de_roles(lista_roles, request.GET.get('filtro'), mostar_todos)
    #### Ordenar por
    lista_roles = servicio_roles.ordenar_lista_de_roles_por(lista_roles, request.GET.get('ordenarPor'))
    #### Contexto
    context = {
        'cantidad_roles_totales': cantidad_roles_totales,
        'configuracionesDeRolesDeSistema': True,
        'cantidad_roles_no_vacios': len(roles_no_vacios),
        "lista_roles": lista_roles,
        'modificarRol': mod_rol,
    }
    if mod_rol:
        permisos_de_rol = servicio_roles.get_permisos_rol(rol_a_modificar)
        extra_context = {
            'rolModificando': rol_a_modificar,
            'rolPermisos': permisos_de_rol,
            'permisosFaltantes': servicio_permisos.get_permisos_faltantes_de_ambito(permisos_de_rol, 'si'),
        }
        context = {**context, **extra_context}
    return render(request, "roles/listaRoles.html", {**context, **get_contexto_basico(request.user)})


@permiso_requerido('modificar_roles_sistema')
def agregar_rol_view(request):
    """
    Vista de creacion de un nuevo rol
    :param request: Todos los datos de la vista
    :return: Crea un nuevo rol y redirecciona a la pagina de listado de roles actualizada con el rol nuevo.
    """
    if request.method == "POST":
        rol = request.POST.get("rol")
        href = request.POST.get("href")
        servicio_roles.agregar_rol(rol)
        return redirect(href)

@permiso_requerido('modificar_roles_sistema')
def eliminar_rol_view(request):
    """
    Vista de eliminacion de un rol
    :param request: Todos los datos de la vista
    :return:
    """
    if request.method == "POST":
        servicios_usuario_rol_sistema.eliminar_rol_y_de_usuarios(request.POST.get("rol"))
        return redirect(request.POST.get("href"))

@permiso_requerido('modificar_roles_sistema')
def agregar_permiso_rol_view(request):
    """
    Vista donde se relaciona un conjunto de permisos a un rol
    :param request: Todos los datos de la vista
    :return:
    """
    if request.method == "POST":
        servicio_roles.agregar_permiso_a_rol(request.POST.get("rol"), request.POST.get("permiso"))
        return redirect(request.POST.get('href'))

@permiso_requerido('modificar_roles_sistema')
def eliminar_permiso_rol_view(request):
    """
    Vista donde se desasocia un permiso de un rol
    :param request: rol, permiso, href
    :return:
    """
    rol = request.POST.get("rol")
    permiso = request.POST.get('permiso')
    href = request.POST.get('href')
    servicio_roles.eliminar_permiso_a_rol(rol, permiso)
    return redirect(href)


@register.filter
def get_rol_permisos(rol):
    return servicio_roles.get_permisos_rol(rol)

@register.filter
def get_rol_permisos_diccionario(rol):
    return servicio_roles.permisos_a_diccionario(servicio_roles.get_permisos_rol(rol))

@register.filter
def get_rol_cantidad_permisos(rol):
    return len(servicio_roles.get_permisos_rol(rol))

@register.filter
def tiene_todos_los_permisos(rol_ref):
    rol = servicio_roles.get_rol(rol_ref)
    ambiente = rol.ambiente
    faltantes = servicio_permisos.get_permisos_faltantes_de_ambito(servicio_roles.get_permisos_rol(rol), ambiente)
    return faltantes.count() == 0
