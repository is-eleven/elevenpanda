from django.test import TestCase
from .models import Rol, Permiso
from .services import ServicioRoles, ServicioPermisos


class RolesTest(TestCase):
    servicio_roles = ServicioRoles()
    servicio_permisos = ServicioPermisos()

    def setUp(self):
        """ Inicaliza la base de datos de prueba y crear elementos de prueba """
        print('#####################################')
        print('##################################### Iniciando test de roles')
        print('#####################################')
        print('Creando roles de prueba en los ambientes de sistema, proyecto y fase.')
        for i in range(5):
            nombre = 'Rol de sistema test ' + str(i)
            Rol.objects.create(nombre=nombre, ambiente='si')
        for i in range(5):
            nombre = 'Rol de proyecto test ' + str(i)
            Rol.objects.create(nombre=nombre, ambiente='pr')
        for i in range(5):
            nombre = 'Rol de fase test ' + str(i)
            Rol.objects.create(nombre=nombre, ambiente='fa')
        print('Roles de prueba creados.')
        print('Creando permisos de prueba en los ambientes de sistema, proyecto y fase.')
        for i in range(5):
            nombre = 'Permiso de sistema test ' + str(i)
            Permiso.objects.create(nombre=nombre, ambito='si')
        for i in range(5):
            nombre = 'Permiso de proyecto test ' + str(i)
            Permiso.objects.create(nombre=nombre, ambito='pr')
        for i in range(5):
            nombre = 'Permiso de fase test ' + str(i)
            Permiso.objects.create(nombre=nombre, ambito='fa')
        print('Permisos de prueba creados.')

    def test_permisos(self):
        """ Realiza test para probar los servicios de roles y permisos """
        def test_agregar_en_ambiente(self, ambienteCodigo, ambienteNombre):
            print('#####################################')
            print('##################################### Test de agregar permisos de roles de ' + ambienteNombre)
            print('#####################################')
            print('Probando que el servicio para extraer roles filtra correctamente roles del .' + ambienteNombre)
            roles_ambiente = self.servicio_roles.get_roles(ambiente=ambienteCodigo)
            for rol_ambiente in roles_ambiente:
                self.assertEqual(rol_ambiente.ambiente, ambienteCodigo, 'No se filtraron (o agregaron) correctamente los roles de ' + ambienteNombre)
            print('Probando que el servicio para extraer permisos filtra correctamente roles del .' + ambienteNombre)
            permisos_ambito = self.servicio_permisos.get_permisos_de_ambito(ambito=ambienteCodigo)
            for permiso_ambito in permisos_ambito:
                self.assertEqual(permiso_ambito.ambito, ambienteCodigo, 'No se filtraron (o agregaron) correctamente los permisos de ' + ambienteNombre)
            print('Agregando permisos a los roles de ' + ambienteNombre)
            for rol_ambito in roles_ambiente:
                for permiso_ambito in permisos_ambito:
                    self.servicio_roles.agregar_permiso_a_rol(rol_ambito, permiso_ambito)
            print('Probando que el servicio para agregar permisos a rol funciona.')
            for rol_ambito in roles_ambiente:
                permisos = self.servicio_roles.get_permisos_rol(rol_ambito)
                self.assertEqual(permisos.count(), permisos_ambito.count(),
                'Los permisos no se agregaron correctamente al rol. Se esperaba que hubieran ' 
                + str(permisos_ambito.count()) +
                ' permisos agregados, pero se encontraron ' + str(permisos.count()) + '.')

        test_agregar_en_ambiente(self, 'si', 'sistema')
        test_agregar_en_ambiente(self, 'pr', 'proyecto')
        test_agregar_en_ambiente(self, 'fa', 'fase')

        def test_eliminar_en_ambiente(self, ambienteCodigo, ambienteNombre):
            print('#####################################')
            print('##################################### Test de eliminar permisos de roles de ' + ambienteNombre)
            print('#####################################')
            roles_ambiente = self.servicio_roles.get_roles(ambiente=ambienteCodigo)
            for rol_ambiente in roles_ambiente:
                self.assertEqual(rol_ambiente.ambiente, ambienteCodigo, 'No se filtraron (o agregaron) correctamente los roles de ' + ambienteNombre)
            permisos_ambito = self.servicio_permisos.get_permisos_de_ambito(ambito=ambienteCodigo)
            for permiso_ambito in permisos_ambito:
                self.assertEqual(permiso_ambito.ambito, ambienteCodigo, 'No se filtraron (o agregaron) correctamente los permisos de ' + ambienteNombre)
            print('Eliminando permisos a los roles de ' + ambienteNombre)
            for rol_ambito in roles_ambiente:
                for permiso_ambito in permisos_ambito:
                    self.servicio_roles.eliminar_permiso_a_rol(rol_ambito, permiso_ambito)
            print('Probando que los roles de ' + ambienteNombre + ' hayan quedado sin permisos.')
            for rol_ambito in roles_ambiente:
                permisos = self.servicio_roles.get_permisos_rol(rol_ambito)
                self.assertEqual(permisos.count(), 0,
                'Los permisos no se eliminaron correctamente al rol. Se esperaba que hubieran 0 '
                'permisos, pero se encontraron ' + str(permisos.count()) + '.')

        test_eliminar_en_ambiente(self, 'si', 'sistema')
        test_eliminar_en_ambiente(self, 'pr', 'proyecto')
        test_eliminar_en_ambiente(self, 'fa', 'fase')

