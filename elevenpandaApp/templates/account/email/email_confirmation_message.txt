{% load account %}{% user_display user as user_display %}{% load i18n %}{% autoescape off %}{% blocktrans with site_name=current_site.name site_domain=current_site.domain %}

El usuario '{{ user_display }}' ha enviado una solicitud de registro.

Para aprobar este registro, vaya a {{ activate_url }}

{% endblocktrans %}
{% blocktrans with site_name=current_site.name site_domain=current_site.domain %}
{% endblocktrans %}
{% endautoescape %}
